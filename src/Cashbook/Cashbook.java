package Cashbook;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Cashbook extends javax.swing.JFrame {

    public Cashbook() {
        initComponents();
        setOpaque();
        changeBackgroud();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        DesignTable();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAddCashbook = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCashbook = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        dateChooserFrom = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        dateChooserTo = new com.toedter.calendar.JDateChooser();
        dateSearch = new javax.swing.JLabel();
        lbRefesh = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtIncome = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        txtExpence = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtBalance = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Cashbook");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddCashbook.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddCashbook.setForeground(new java.awt.Color(255, 255, 255));
        btnAddCashbook.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddCashbook.setText("+");
        btnAddCashbook.setToolTipText("Add user");
        btnAddCashbook.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddCashbook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddCashbookMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddCashbookMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddCashbookMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCashbook, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCashbook, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 30, 30));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblCashbook.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblCashbook.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "DATE", "INCOME", "EXPENCE", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCashbook.setFocusable(false);
        tblCashbook.setRowHeight(35);
        tblCashbook.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblCashbook.setShowVerticalLines(false);
        tblCashbook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblCashbookMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblCashbook);
        if (tblCashbook.getColumnModel().getColumnCount() > 0) {
            tblCashbook.getColumnModel().getColumn(0).setMinWidth(0);
            tblCashbook.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblCashbook.getColumnModel().getColumn(0).setMaxWidth(0);
            tblCashbook.getColumnModel().getColumn(4).setPreferredWidth(325);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 700, 440));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("From :");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 70, 40, -1));

        dateChooserFrom.setDateFormatString("yyyy-MM-dd");
        dateChooserFrom.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserFrom, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 110, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("To :");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 70, 30, -1));

        dateChooserTo.setDateFormatString("yyyy-MM-dd");
        dateChooserTo.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserTo, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 70, 110, 20));

        dateSearch.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        dateSearch.setForeground(new java.awt.Color(0, 153, 153));
        dateSearch.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        dateSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dateSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateSearchMouseClicked(evt);
            }
        });
        jPanel3.add(dateSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 70, 40, 20));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel3.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 70, 30, 20));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, -1, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 200, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 200, 10));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("All income :");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        txtIncome.setEditable(false);
        txtIncome.setBackground(new java.awt.Color(255, 255, 255));
        txtIncome.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtIncome.setText("0.00");
        txtIncome.setBorder(null);
        txtIncome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIncomeActionPerformed(evt);
            }
        });
        jPanel4.add(txtIncome, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 130, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 130, 10));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("All expence :");
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, -1, -1));

        txtExpence.setEditable(false);
        txtExpence.setBackground(new java.awt.Color(255, 255, 255));
        txtExpence.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtExpence.setText("0.00");
        txtExpence.setBorder(null);
        txtExpence.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtExpenceActionPerformed(evt);
            }
        });
        jPanel4.add(txtExpence, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 130, 20));
        jPanel4.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 40, 130, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Balance :");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 20, -1, -1));

        txtBalance.setEditable(false);
        txtBalance.setBackground(new java.awt.Color(255, 255, 255));
        txtBalance.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtBalance.setText("0.00");
        txtBalance.setBorder(null);
        txtBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceActionPerformed(evt);
            }
        });
        jPanel4.add(txtBalance, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 20, 130, 20));
        jPanel4.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 130, 10));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 580, 700, 60));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 660));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void btnAddCashbookMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCashbookMouseEntered
        btnAddCashbook.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddCashbookMouseEntered

    private void btnAddCashbookMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCashbookMouseExited
        btnAddCashbook.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddCashbookMouseExited

    private void btnAddCashbookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCashbookMouseClicked
        //if (UserPrivilage.equals("admin")) {

        new AddCashbook().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddCashbookMouseClicked

    private void tblCashbookMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCashbookMouseReleased

    }//GEN-LAST:event_tblCashbookMouseReleased

    private void dateSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateSearchMouseClicked
        search();
    }//GEN-LAST:event_dateSearchMouseClicked

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        txtSearch.setText(null);
        dateChooserFrom.setDate(null);
        dateChooserTo.setDate(null);
        DefaultTableModel dtm = (DefaultTableModel) tblCashbook.getModel();
        dtm.setRowCount(0);
        loadDetails();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tblCashbook.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(txtSearch.getText().toUpperCase()));
    }//GEN-LAST:event_lbRefeshMouseClicked

    private void txtBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceActionPerformed

    private void txtExpenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtExpenceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtExpenceActionPerformed

    private void txtIncomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIncomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIncomeActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            dateChooserFrom.setDate(null);
            dateChooserTo.setDate(null);

            ResultSet rs = DB.search("SELECT * FROM cashbook WHERE description LIKE '%' '" + txtSearch.getText() + "' '%' OR income LIKE '%' '" + txtSearch.getText() + "' '%' OR expence LIKE '%' '" + txtSearch.getText() + "' '%' ");

            DefaultTableModel dtm = (DefaultTableModel) tblCashbook.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idcashbook"),
                    rs.getString("date"),
                    Decimal_Formats.Price(rs.getDouble("income")),
                    Decimal_Formats.Price(rs.getDouble("expence")),
                    rs.getString("description")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cashbook().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddCashbook;
    private com.toedter.calendar.JDateChooser dateChooserFrom;
    private com.toedter.calendar.JDateChooser dateChooserTo;
    private javax.swing.JLabel dateSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbRefesh;
    public static javax.swing.JTable tblCashbook;
    public static javax.swing.JTextField txtBalance;
    public static javax.swing.JTextField txtExpence;
    public static javax.swing.JTextField txtIncome;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnAddCashbook.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnAddCashbook.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblCashbook.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblCashbook.getTableHeader().setOpaque(false);
        tblCashbook.getTableHeader().setBackground(Color.white);
        tblCashbook.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblCashbook.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
        tblCashbook.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblCashbook.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
    }

    public static void loadDetails() {
        try {
            ResultSet rs = DB.search("SELECT * FROM cashbook WHERE isActive = '1' ORDER BY date DESC");

            DefaultTableModel dtm = (DefaultTableModel) tblCashbook.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idcashbook"),
                    rs.getString("date"),
                    Decimal_Formats.Price(rs.getDouble("income")),
                    Decimal_Formats.Price(rs.getDouble("expence")),
                    rs.getString("description")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void search() {
        try {

            if (dateChooserFrom.getDate() != null || dateChooserTo.getDate() != null) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DefaultTableModel dtm = (DefaultTableModel) tblCashbook.getModel();
                dtm.setRowCount(0);

                ResultSet rs = null;

                if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() != null) {

                    rs = DB.search("SELECT * FROM cashbook WHERE date BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' AND isActive = 1 ORDER BY date DESC ");

                } else if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() == null) {

                    rs = DB.search("SELECT * FROM cashbook WHERE date = '" + df.format(dateChooserFrom.getDate()) + "' AND isActive = 1 ORDER BY date DESC ");

                } else if (dateChooserTo.getDate() != null && dateChooserFrom.getDate() == null) {

                    rs = DB.search("SELECT * FROM cashbook WHERE date = '" + df.format(dateChooserTo.getDate()) + "' AND isActive = 1 ORDER BY date DESC ");

                }

                while (rs.next()) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idcashbook"),
                        rs.getString("date"),
                        Decimal_Formats.Price(rs.getDouble("income")),
                        Decimal_Formats.Price(rs.getDouble("expence")),
                        rs.getString("description")
                    });
                }

                calculate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculate() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblCashbook.getModel();

            Double income = 0.0;
            Double expence = 0.0;
            Double total = 0.0;

            for (int i = 0; i < dtm.getRowCount(); i++) {
                income += Double.parseDouble(dtm.getValueAt(i, 2).toString());
                expence += Double.parseDouble(dtm.getValueAt(i, 3).toString());
            }

            total = income - expence;

            txtIncome.setText("" + Decimal_Formats.Price(income));
            txtExpence.setText("" + Decimal_Formats.Price(expence));
            txtBalance.setText("" + Decimal_Formats.Price(total));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
