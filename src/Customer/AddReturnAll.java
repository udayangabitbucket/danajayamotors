package Customer;

import static Customer.ViewAllInvoice.tblAllInvoice;
import static Customer.InvReturnAll.getAllInvReturn;
import static Customer.InvReturnAll.invNo;
import static Customer.InvReturnAll.loadItems;
import static Customer.InvReturnAll.tblInvItemAll;
import static Customer.ViewAllInvoice.loadAllInv;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserName;
import static User.User.loadUsers;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

public class AddReturnAll extends javax.swing.JFrame {

    boolean TXT_VALIDATE = false;
    String invID = null;
    String itemID = null;
    Double stockQty = null;

    public AddReturnAll() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        loadReturn();
        txtReturnPrice.grabFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnSave = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txtReturnPrice = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        txtReturnQty = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        lbPartNo = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lbInvQty = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lbStock = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbInvPrice = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lbDescription = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtReason = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Add Return");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 120, 26));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("SAVE");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 450, 120, 40));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtReturnPrice.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtReturnPrice.setBorder(null);
        txtReturnPrice.setNextFocusableComponent(txtReturnQty);
        txtReturnPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReturnPriceActionPerformed(evt);
            }
        });
        txtReturnPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtReturnPriceKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtReturnPriceKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtReturnPriceKeyTyped(evt);
            }
        });
        jPanel2.add(txtReturnPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 330, 20));
        jPanel2.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 330, 20));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 153, 153));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Return Qty : *");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 90, -1));

        txtReturnQty.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtReturnQty.setBorder(null);
        txtReturnQty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReturnQtyActionPerformed(evt);
            }
        });
        txtReturnQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtReturnQtyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtReturnQtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtReturnQtyKeyTyped(evt);
            }
        });
        jPanel2.add(txtReturnQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 60, 330, 20));
        jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 330, 20));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 3, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("(Single item)");
        jPanel2.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 80, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Return price : *");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 90, -1));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 500, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date : ");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 60, 40, -1));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 60, 140, 20));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setText("Part no :");
        jPanel4.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, 50, -1));

        lbPartNo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbPartNo.setForeground(new java.awt.Color(51, 51, 51));
        lbPartNo.setText("0000000");
        jPanel4.add(lbPartNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 150, -1));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setText("Invoice Qty :");
        jPanel4.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, -1, -1));

        lbInvQty.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbInvQty.setForeground(new java.awt.Color(51, 51, 51));
        lbInvQty.setText("00");
        jPanel4.add(lbInvQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 60, -1));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Stock qty :");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 70, -1));

        lbStock.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbStock.setText("00");
        jPanel4.add(lbStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 70, -1));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 153, 153));
        jLabel10.setText("Invoice price :");
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 20, -1, -1));

        lbInvPrice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbInvPrice.setText("0.00");
        jPanel4.add(lbInvPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 20, 80, -1));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 153, 153));
        jLabel9.setText("Description :");
        jPanel4.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, -1));

        lbDescription.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbDescription.setForeground(new java.awt.Color(51, 51, 51));
        lbDescription.setText("Description ");
        jPanel4.add(lbDescription, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 340, -1));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 500, 150));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Reason :  ");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 390, 80, -1));

        txtReason.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtReason.setBorder(null);
        txtReason.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReasonActionPerformed(evt);
            }
        });
        txtReason.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtReasonKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtReasonKeyReleased(evt);
            }
        });
        jPanel3.add(txtReason, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 390, 330, 20));
        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 410, 330, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 570, 530));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void txtReturnQtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReturnQtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReturnQtyActionPerformed

    private void txtReturnQtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnQtyKeyPressed

    }//GEN-LAST:event_txtReturnQtyKeyPressed

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        saveReturn();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void txtReturnPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReturnPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReturnPriceActionPerformed

    private void txtReturnPriceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnPriceKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReturnPriceKeyPressed

    private void txtReturnPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnPriceKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtReturnPriceKeyTyped

    private void txtReturnPriceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnPriceKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveReturn();
        }
    }//GEN-LAST:event_txtReturnPriceKeyReleased

    private void txtReturnQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnQtyKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveReturn();
        }
    }//GEN-LAST:event_txtReturnQtyKeyReleased

    private void txtReturnQtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReturnQtyKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtReturnQtyKeyTyped

    private void txtReasonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReasonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReasonActionPerformed

    private void txtReasonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReasonKeyPressed

    }//GEN-LAST:event_txtReasonKeyPressed

    private void txtReasonKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReasonKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveReturn();
        }
    }//GEN-LAST:event_txtReasonKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddReturnAll().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbDescription;
    private javax.swing.JLabel lbInvPrice;
    private javax.swing.JLabel lbInvQty;
    private javax.swing.JLabel lbPartNo;
    private javax.swing.JLabel lbStock;
    private javax.swing.JTextField txtReason;
    private javax.swing.JTextField txtReturnPrice;
    private javax.swing.JTextField txtReturnQty;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);

    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);

    }

    private void validateTXT() {

        java.awt.Component[] cp = jPanel2.getComponents();
        for (java.awt.Component c : cp) {

            if (c instanceof JTextField) {

                if ((((JTextField) c).getText().isEmpty())) {

                    TXT_VALIDATE = true;

                    break;

                } else {

                    TXT_VALIDATE = false;

                }

            }

        }
    }

    private void loadReturn() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblInvItemAll.getModel();
            int selectedRow = tblInvItemAll.getSelectedRow();

            invID = dtm.getValueAt(selectedRow, 0).toString();
            itemID = dtm.getValueAt(selectedRow, 1).toString();

            ResultSet rs = DB.search("SELECT s.*, i.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems WHERE s.iditems = '" + itemID + "' ");

            if (rs.next()) {
                stockQty = rs.getDouble("qty");
            }
            lbStock.setText("" + stockQty);
            dateChooser.setDate(new Date());
            lbPartNo.setText(dtm.getValueAt(selectedRow, 2).toString());
            lbInvQty.setText(dtm.getValueAt(selectedRow, 5).toString());
            lbDescription.setText(dtm.getValueAt(selectedRow, 3).toString());
            lbInvPrice.setText(Decimal_Formats.Price(Double.parseDouble((String) dtm.getValueAt(selectedRow, 7))));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveReturn() {
        try {

            if (dateChooser.getDate() != null) {
                validateTXT();
                if (TXT_VALIDATE == false) {

                    if (Double.parseDouble(lbInvQty.getText()) >= Double.parseDouble(txtReturnQty.getText())) {

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

                        Double total = Double.parseDouble(txtReturnPrice.getText()) * Double.parseDouble(txtReturnQty.getText());

                        if (Double.parseDouble(txtReturnPrice.getText()) != 0) {
                            ResultSet invoice = DB.search("SELECT * FROM invoice WHERE idinvoice = '" + invID + "' ");

                            if (invoice.next()) {

                                String invoiceTotal = "UPDATE invoice SET invoiceTotal = '" + (invoice.getDouble("invoiceTotal") - total) + "' WHERE idinvoice = '" + invID + "' ";
                                DB.idu(invoiceTotal);
                            }

                            ResultSet invoiceItem = DB.search("SELECT * FROM invoiceitems WHERE idinvoice = '" + invID + "' AND iditems = '" + itemID + "' ");

                            if (invoiceItem.next()) {

                                String invItemTotal = "UPDATE invoiceitems SET totalAmount = '" + (invoiceItem.getDouble("totalAmount") - total) + "' WHERE idinvoice = '" + invID + "' AND iditems = '" + itemID + "' ";
                                DB.idu(invItemTotal);
                            }

                            ResultSet invoicePayment = DB.search("SELECT * FROM invoicepayment WHERE idinvoice = '" + invID + "' ");

                            if (invoicePayment.next()) {

                                Double invoicePaid = invoicePayment.getDouble("paid");

                                if (invoicePaid > total) {
                                    String paymentTotal = "UPDATE invoicepayment SET total = '" + (invoicePayment.getDouble("total") - total) + "', paid = '" + (invoicePayment.getDouble("paid") - total) + "' WHERE idinvoice = '" + invID + "' ";
                                    DB.idu(paymentTotal);
                                } else {

                                    String paymentTotal = "UPDATE invoicepayment SET total = '" + (invoicePayment.getDouble("total") - total) + "' WHERE idinvoice = '" + invID + "' ";
                                    DB.idu(paymentTotal);
                                }

                            }

                            String insertOutstand = "INSERT INTO invoiceoutsatnding(outstandDate, deduction, status, idinvoice)"
                                    + "VALUES ('" + date + "', '" + total + "', '(" + lbPartNo.getText() + ") ' 'return item', '" + invID + "')";

                            DB.idu(insertOutstand);

                            String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + 0.0 + "', '" + total + "', '" + lbPartNo.getText() + "('  '" + invNo + ")' ' customer return amount' ) ";
                            DB.idu(insertCashbook);

                        }

                        if (Double.parseDouble(txtReturnQty.getText()) != 0) {
                            String insertReturn = "INSERT INTO returninvoice (returnPrice, qty, returnDate, returnTotal, returnstatus, reason, idinvoice, iditems) VALUES ('" + Double.parseDouble(txtReturnPrice.getText()) + "', '" + Double.parseDouble(txtReturnQty.getText()) + "', '" + date + "', '" + total + "', 'return_inv', '" + txtReason.getText().toLowerCase() + "', '" + invID + "', '" + itemID + "' ) ";
                            DB.idu(insertReturn);

                            ResultSet rs = DB.search("SELECT * FROM invoiceitems WHERE idinvoice = '" + invID + "' AND iditems = '" + itemID + "' ");

                            Double invoiceQty = null;

                            if (rs.next()) {
                                invoiceQty = rs.getDouble("qty");
                            }

                            String updateInvItem = "UPDATE invoiceitems SET qty = '" + (invoiceQty - Double.parseDouble(txtReturnQty.getText())) + "' WHERE idinvoice = '" + invID + "' AND iditems = '" + itemID + "' ";
                            DB.idu(updateInvItem);

                            Double qty = stockQty + Double.parseDouble(txtReturnQty.getText());

                            String updateStock = "UPDATE stock SET qty = '" + qty + "' WHERE iditems = '" + itemID + "' ";
                            DB.idu(updateStock);
                        }

                        loadItems();
                        getAllInvReturn();
                        loadAllInv();

                        this.dispose();
                        NotificationPopup.save();

                    } else {
                        NotificationPopup.errorQty();
                    }

                } else {
                    NotificationPopup.fillFeilds();
                }
            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
