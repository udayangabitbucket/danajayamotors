package Customer;

import ExClasses.DB;
import ExClasses.dateForm;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class Customer extends javax.swing.JFrame {

    public Customer() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        tableASC();
        loadCustomer();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        addInvoice = new javax.swing.JMenuItem();
        invoiceHistory = new javax.swing.JMenuItem();
        editPopup = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAddCustomer = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCustomer = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        btnOldInvoice = new javax.swing.JLabel();
        btnAllInvoice = new javax.swing.JLabel();
        btnOutsatnding = new javax.swing.JLabel();

        jPopupMenu1.setBackground(new java.awt.Color(102, 102, 102));
        jPopupMenu1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPopupMenu1.setPreferredSize(new java.awt.Dimension(100, 75));

        addInvoice.setBackground(new java.awt.Color(102, 102, 102));
        addInvoice.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        addInvoice.setForeground(new java.awt.Color(255, 255, 255));
        addInvoice.setText("Add Invoice");
        addInvoice.setBorder(null);
        addInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addInvoiceActionPerformed(evt);
            }
        });
        jPopupMenu1.add(addInvoice);

        invoiceHistory.setBackground(new java.awt.Color(102, 102, 102));
        invoiceHistory.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        invoiceHistory.setForeground(new java.awt.Color(255, 255, 255));
        invoiceHistory.setText("Invoice History");
        invoiceHistory.setBorder(null);
        invoiceHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceHistoryActionPerformed(evt);
            }
        });
        jPopupMenu1.add(invoiceHistory);

        editPopup.setBackground(new java.awt.Color(102, 102, 102));
        editPopup.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        editPopup.setForeground(new java.awt.Color(255, 255, 255));
        editPopup.setText("Edit");
        editPopup.setBorder(null);
        editPopup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editPopupActionPerformed(evt);
            }
        });
        jPopupMenu1.add(editPopup);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Customers");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddCustomer.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddCustomer.setForeground(new java.awt.Color(255, 255, 255));
        btnAddCustomer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddCustomer.setText("+");
        btnAddCustomer.setToolTipText("Add user");
        btnAddCustomer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddCustomerMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddCustomerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddCustomerMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 30, 30));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NAME", "CONTACT NO", "ADDRESS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCustomer.setFocusable(false);
        tblCustomer.setRowHeight(30);
        tblCustomer.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblCustomer.setShowVerticalLines(false);
        tblCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCustomerMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblCustomerMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblCustomer);
        if (tblCustomer.getColumnModel().getColumnCount() > 0) {
            tblCustomer.getColumnModel().getColumn(0).setMinWidth(0);
            tblCustomer.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblCustomer.getColumnModel().getColumn(0).setMaxWidth(0);
            tblCustomer.getColumnModel().getColumn(1).setPreferredWidth(150);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 700, 400));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 70, 260, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 90, 260, 20));

        btnOldInvoice.setBackground(new java.awt.Color(255, 255, 255));
        btnOldInvoice.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnOldInvoice.setForeground(new java.awt.Color(0, 153, 153));
        btnOldInvoice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnOldInvoice.setText("Old Invoice");
        btnOldInvoice.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnOldInvoice.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOldInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnOldInvoiceMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnOldInvoiceMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnOldInvoiceMouseExited(evt);
            }
        });
        jPanel3.add(btnOldInvoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 540, 130, 25));

        btnAllInvoice.setBackground(new java.awt.Color(255, 255, 255));
        btnAllInvoice.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnAllInvoice.setForeground(new java.awt.Color(0, 153, 153));
        btnAllInvoice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAllInvoice.setText("All Invoice History");
        btnAllInvoice.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnAllInvoice.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnAllInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAllInvoiceMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAllInvoiceMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAllInvoiceMouseExited(evt);
            }
        });
        jPanel3.add(btnAllInvoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 540, 130, 25));

        btnOutsatnding.setBackground(new java.awt.Color(255, 255, 255));
        btnOutsatnding.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnOutsatnding.setForeground(new java.awt.Color(0, 153, 153));
        btnOutsatnding.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnOutsatnding.setText("Outstanding List");
        btnOutsatnding.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnOutsatnding.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOutsatnding.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnOutsatndingMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnOutsatndingMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnOutsatndingMouseExited(evt);
            }
        });
        jPanel3.add(btnOutsatnding, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 540, 130, 25));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void btnAddCustomerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCustomerMouseEntered
        btnAddCustomer.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddCustomerMouseEntered

    private void btnAddCustomerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCustomerMouseExited
        btnAddCustomer.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddCustomerMouseExited

    private void btnAddCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCustomerMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new AddCustomer().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddCustomerMouseClicked

    private void tblCustomerMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomerMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblCustomer.getSelectedRowCount() != 0) {
                jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}

        }
    }//GEN-LAST:event_tblCustomerMouseReleased

    private void addInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addInvoiceActionPerformed
        new AddInvoice().setVisible(true);
    }//GEN-LAST:event_addInvoiceActionPerformed

    private void invoiceHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceHistoryActionPerformed
        DefaultTableModel dtm = (DefaultTableModel) tblCustomer.getModel();
        int selectedRow = tblCustomer.getSelectedRow();

        if (dtm.getValueAt(selectedRow, 1).equals("SALES CUSTOMER")) {
            new ViewAllInvSalse().setVisible(true);
        } else {
            new ViewInvCustomer().setVisible(true);
        }


    }//GEN-LAST:event_invoiceHistoryActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            ResultSet rs = DB.search("SELECT * FROM customer WHERE (customerName LIKE '%' '" + txtSearch.getText() + "' '%' OR contact_1 LIKE '%' '" + txtSearch.getText() + "' '%' OR contact_2 LIKE '%' '" + txtSearch.getText() + "' '%') AND status = 'permenent' ");

            DefaultTableModel dtm = (DefaultTableModel) tblCustomer.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                dtm.addRow(new Object[]{
                    rs.getInt("idcustomer"),
                    rs.getString("customerName"),
                    rs.getString("contact_1") + "  " + rs.getString("contact_2"),
                    rs.getString("address")
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void editPopupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editPopupActionPerformed
        DefaultTableModel dtm = (DefaultTableModel) tblCustomer.getModel();
        int selectedRow = tblCustomer.getSelectedRow();
        if (!(dtm.getValueAt(selectedRow, 1)).equals("SALES CUSTOMER")) {
            new EditCustomer().setVisible(true);
        }
    }//GEN-LAST:event_editPopupActionPerformed

    private void tblCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomerMouseClicked
        txtSearch.setText(null);
    }//GEN-LAST:event_tblCustomerMouseClicked

    private void btnOldInvoiceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOldInvoiceMouseClicked
        new OldInvoice.OldInvoice().setVisible(true);
    }//GEN-LAST:event_btnOldInvoiceMouseClicked

    private void btnOldInvoiceMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOldInvoiceMouseEntered
        btnOldInvoice.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnOldInvoiceMouseEntered

    private void btnOldInvoiceMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOldInvoiceMouseExited
        btnOldInvoice.setBackground(Color.white);
    }//GEN-LAST:event_btnOldInvoiceMouseExited

    private void btnAllInvoiceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAllInvoiceMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new ViewAllInvoice().setVisible(true);
        // }
    }//GEN-LAST:event_btnAllInvoiceMouseClicked

    private void btnAllInvoiceMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAllInvoiceMouseEntered
        btnAllInvoice.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnAllInvoiceMouseEntered

    private void btnAllInvoiceMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAllInvoiceMouseExited
        btnAllInvoice.setBackground(Color.white);
    }//GEN-LAST:event_btnAllInvoiceMouseExited

    private void btnOutsatndingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOutsatndingMouseClicked
        new OutstandingList().setVisible(true);
    }//GEN-LAST:event_btnOutsatndingMouseClicked

    private void btnOutsatndingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOutsatndingMouseEntered
        btnOutsatnding.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnOutsatndingMouseEntered

    private void btnOutsatndingMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOutsatndingMouseExited
        btnOutsatnding.setBackground(Color.white);
    }//GEN-LAST:event_btnOutsatndingMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Customer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addInvoice;
    private javax.swing.JLabel btnAddCustomer;
    private javax.swing.JLabel btnAllInvoice;
    private javax.swing.JLabel btnOldInvoice;
    private javax.swing.JLabel btnOutsatnding;
    private javax.swing.JMenuItem editPopup;
    private javax.swing.JMenuItem invoiceHistory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblCustomer;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnAddCustomer.setOpaque(true);
        btnAllInvoice.setOpaque(true);
        btnOldInvoice.setOpaque(true);
        btnOutsatnding.setOpaque(true);
    }

    private void changeBackgroud() {
        btnAllInvoice.setBackground(Color.white);
        btnOldInvoice.setBackground(Color.white);
        lbClose.setBackground(Color.white);
        btnAddCustomer.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblCustomer.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblCustomer.getTableHeader().setOpaque(false);
        tblCustomer.getTableHeader().setBackground(Color.white);
        tblCustomer.getTableHeader().setForeground(new Color(0, 153, 153));

//        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
//        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
//        tblCustomer.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
//        tblCustomer.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
//        tblCustomer.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
    }
    
    private void tableASC() {

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tblCustomer.getModel());
        tblCustomer.setRowSorter(sorter);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();

    }

    public static void loadCustomer() {
        try {
            ResultSet rs = DB.search("SELECT * FROM customer WHERE status = 'permenent' OR customerName = 'sales customer' ");

            DefaultTableModel dtm = (DefaultTableModel) tblCustomer.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                dtm.addRow(new Object[]{
                    rs.getInt("idcustomer"),
                    rs.getString("customerName"),
                    rs.getString("contact_1") + "  " + rs.getString("contact_2"),
                    rs.getString("address")
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
