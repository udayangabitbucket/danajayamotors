package Customer;

import static Customer.ViewAllInvoice.loadAllInv;
import static Customer.ViewAllInvoice.tblAllInvoice;
import Items.*;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import ExClasses.dateForm;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class EditInvCustomer extends javax.swing.JFrame {

    Integer invoidId;

    public EditInvCustomer() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        invoidId = Integer.parseInt(tblAllInvoice.getModel().getValueAt(tblAllInvoice.getSelectedRow(), 0).toString());
        lbCustomer.setText(tblAllInvoice.getModel().getValueAt(tblAllInvoice.getSelectedRow(), 1).toString());
        lbInvoice.setText(tblAllInvoice.getModel().getValueAt(tblAllInvoice.getSelectedRow(), 2).toString());
        tblChangeCustomer.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        rbPermenent = new javax.swing.JRadioButton();
        rbSales = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblChangeCustomer = new javax.swing.JTable();
        btnUpdate = new javax.swing.JLabel();
        lbCustomer = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lbInvoice = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(760, 630));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Change Customer");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 140, 26));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 100, 30, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 100, 280, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 120, 280, 20));

        rbPermenent.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbPermenent);
        rbPermenent.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rbPermenent.setForeground(new java.awt.Color(0, 153, 153));
        rbPermenent.setSelected(true);
        rbPermenent.setText("PERMENENT CUSTOMER");
        rbPermenent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPermenentActionPerformed(evt);
            }
        });
        jPanel3.add(rbPermenent, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, -1, -1));

        rbSales.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbSales);
        rbSales.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rbSales.setForeground(new java.awt.Color(0, 153, 153));
        rbSales.setText("SALES CUSTOMER");
        rbSales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSalesActionPerformed(evt);
            }
        });
        jPanel3.add(rbSales, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 110, -1, -1));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblChangeCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblChangeCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CUSTOMER ID", "CUSTOMER "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblChangeCustomer.setColumnSelectionAllowed(true);
        tblChangeCustomer.setFocusable(false);
        tblChangeCustomer.setRequestFocusEnabled(false);
        tblChangeCustomer.setRowHeight(30);
        tblChangeCustomer.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblChangeCustomer.setShowVerticalLines(false);
        tblChangeCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblChangeCustomerMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblChangeCustomerMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblChangeCustomer);
        tblChangeCustomer.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (tblChangeCustomer.getColumnModel().getColumnCount() > 0) {
            tblChangeCustomer.getColumnModel().getColumn(0).setMinWidth(0);
            tblChangeCustomer.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblChangeCustomer.getColumnModel().getColumn(0).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, 680, 400));

        btnUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnUpdate.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(0, 153, 153));
        btnUpdate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnUpdate.setText("UPDATE");
        btnUpdate.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnUpdate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUpdateMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdateMouseExited(evt);
            }
        });
        jPanel3.add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 570, 110, 30));

        lbCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbCustomer.setText("SET CUTOMER NAME HERE");
        jPanel3.add(lbCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 450, -1));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setText("Current Customer :");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 120, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setText("          Invoice No :");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 120, -1));

        lbInvoice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbInvoice.setText("SET INVOICE NO HERE");
        jPanel3.add(lbInvoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, 230, -1));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblChangeCustomer.getModel();
            dtm.setRowCount(0);

            ResultSet rs = null;

            if (rbPermenent.isSelected()) {
                try {

                    rs = DB.search("SELECT * FROM customer WHERE (customerName LIKE '%' '" + txtSearch.getText() + "' '%' OR address LIKE '%' '" + txtSearch.getText() + "' '%' OR contact_1 LIKE '%' '" + txtSearch.getText() + "' '%') AND status = 'permenent' ORDER BY customerName ASC");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {

                    rs = DB.search("SELECT * FROM customer WHERE (customerName LIKE '%' '" + txtSearch.getText() + "' '%' OR address LIKE '%' '" + txtSearch.getText() + "' '%' OR contact_1 LIKE '%' '" + txtSearch.getText() + "' '%') AND status = 'temporory' ORDER BY customerName ASC");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            while (rs.next()) {

                if (!(rs.getString("customerName").equals("SALES CUSTOMER"))) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idcustomer"),
                        rs.getString("customerName") + ",   " + rs.getString("address").toLowerCase() + ",   " + rs.getString("contact_1")
                    });
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void rbPermenentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPermenentActionPerformed
        txtSearch.setText("");
        loadDetails();
    }//GEN-LAST:event_rbPermenentActionPerformed

    private void tblChangeCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblChangeCustomerMouseClicked

    }//GEN-LAST:event_tblChangeCustomerMouseClicked

    private void tblChangeCustomerMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblChangeCustomerMouseReleased

    }//GEN-LAST:event_tblChangeCustomerMouseReleased

    private void btnUpdateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseClicked
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblChangeCustomer.getModel();
            int selectedRow = tblChangeCustomer.getSelectedRow();

            if (selectedRow != -1) {

                int response = JOptionPane.showConfirmDialog(this, "Do you want change customer?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (response == JOptionPane.YES_OPTION) {

                    String updateInv = "UPDATE invoice SET idcustomer = '" + dtm.getValueAt(selectedRow, 0) + "' WHERE idinvoice = '" + invoidId + "' ";
                    DB.idu(updateInv);

                    String updateInvPay = "UPDATE invoicepayment SET idcustomer = '" + dtm.getValueAt(selectedRow, 0) + "' WHERE idinvoice = '" + invoidId + "' ";
                    DB.idu(updateInvPay);
                    loadAllInv();
                    this.dispose();
                    NotificationPopup.update();
                    

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnUpdateMouseClicked

    private void btnUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseEntered
        btnUpdate.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnUpdateMouseEntered

    private void btnUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseExited
        btnUpdate.setBackground(Color.white);
    }//GEN-LAST:event_btnUpdateMouseExited

    private void rbSalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSalesActionPerformed
        txtSearch.setText("");
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblChangeCustomer.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT * FROM customer WHERE status = 'temporory' ORDER BY customerName ASC");

            while (rs.next()) {

                if (!(rs.getString("customerName").equals("SALES CUSTOMER"))) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idcustomer"),
                        rs.getString("customerName") + ",   " + rs.getString("address").toLowerCase() + ",   " + rs.getString("contact_1")
                    });
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_rbSalesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditInvCustomer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbCustomer;
    private javax.swing.JLabel lbInvoice;
    private javax.swing.JRadioButton rbPermenent;
    private javax.swing.JRadioButton rbSales;
    public static javax.swing.JTable tblChangeCustomer;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnUpdate.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnUpdate.setBackground(Color.white);
    }

    private void DesignTable() {
        tblChangeCustomer.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblChangeCustomer.getTableHeader().setOpaque(false);
        tblChangeCustomer.getTableHeader().setBackground(Color.white);
        tblChangeCustomer.getTableHeader().setForeground(new Color(0, 153, 153));
    }

    private void loadDetails() {

        try {

            DefaultTableModel dtm = (DefaultTableModel) tblChangeCustomer.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT * FROM customer WHERE status = 'permenent' ORDER BY customerName ASC");

            while (rs.next()) {

                if (!(rs.getString("customerName").equals("SALES CUSTOMER"))) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idcustomer"),
                        rs.getString("customerName") + ",   " + rs.getString("address").toLowerCase() + ",   " + rs.getString("contact_1")

                    });
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
