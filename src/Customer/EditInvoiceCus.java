package Customer;

import static Customer.ViewInvCustomer.loadInvoice;
import static Customer.ViewInvCustomer.tblInvCustomer;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import ExClasses.dateForm;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class EditInvoiceCus extends javax.swing.JFrame {

    boolean TXT_VALIDATE_1 = false;
    boolean TXT_VALIDATE_2 = false;
    boolean comboActive = false;

    Integer invID;
    String invNo;
    String cusName;
    Double prePaid;

    public EditInvoiceCus() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        invID = Integer.parseInt(tblInvCustomer.getModel().getValueAt(tblInvCustomer.getSelectedRow(), 0).toString());
        invNo = tblInvCustomer.getModel().getValueAt(tblInvCustomer.getSelectedRow(), 3).toString();
        cusName = tblInvCustomer.getModel().getValueAt(tblInvCustomer.getSelectedRow(), 2).toString();
        prePaid = Double.parseDouble(tblInvCustomer.getModel().getValueAt(tblInvCustomer.getSelectedRow(), 6).toString());
        lbCustomer.setText(cusName);
        lbChequeNo.setVisible(false);
        loadReFCus();
        loadDetails();
        loadSearchTable();
        searchMenu.add(searchPanel);
        txtPartNo.setFont(new Font("Times New Roman", Font.PLAIN, 15));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        searchtbl = new javax.swing.JTable();
        searchMenu = new javax.swing.JPopupMenu();
        grnPopup = new javax.swing.JPopupMenu();
        ediit = new javax.swing.JMenuItem();
        remove = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEditInvoice = new javax.swing.JTable();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txtPartNo = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtUnitPrice = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        txtQty = new javax.swing.JTextField();
        jSeparator13 = new javax.swing.JSeparator();
        txtDiscount = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lbRefesh = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lbQty = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        txtPaid = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        comboType = new javax.swing.JComboBox<>();
        lbChequeNo = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        comboRef = new javax.swing.JComboBox<>();
        btnAddRef = new javax.swing.JLabel();
        txtInvNo = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        lbCustomer = new javax.swing.JLabel();
        btnSave = new javax.swing.JLabel();

        searchPanel.setBackground(new java.awt.Color(255, 255, 255));
        searchPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        searchtbl.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        searchtbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        searchtbl.setFocusable(false);
        searchtbl.setOpaque(false);
        searchtbl.setRowHeight(25);
        searchtbl.setSelectionBackground(new java.awt.Color(204, 204, 204));
        searchtbl.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        searchtbl.setShowVerticalLines(false);
        searchtbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchtblMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                searchtblMousePressed(evt);
            }
        });
        searchtbl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchtblKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchtblKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(searchtbl);
        if (searchtbl.getColumnModel().getColumnCount() > 0) {
            searchtbl.getColumnModel().getColumn(0).setMinWidth(0);
            searchtbl.getColumnModel().getColumn(0).setPreferredWidth(0);
            searchtbl.getColumnModel().getColumn(0).setMaxWidth(0);
            searchtbl.getColumnModel().getColumn(1).setMinWidth(100);
            searchtbl.getColumnModel().getColumn(1).setPreferredWidth(100);
            searchtbl.getColumnModel().getColumn(1).setMaxWidth(100);
            searchtbl.getColumnModel().getColumn(2).setMinWidth(300);
            searchtbl.getColumnModel().getColumn(2).setPreferredWidth(300);
            searchtbl.getColumnModel().getColumn(2).setMaxWidth(300);
        }

        searchPanel.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 402, 400));

        searchMenu.setFocusable(false);

        grnPopup.setBackground(new java.awt.Color(102, 102, 102));
        grnPopup.setForeground(new java.awt.Color(255, 255, 255));
        grnPopup.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        grnPopup.setPreferredSize(new java.awt.Dimension(97, 50));

        ediit.setBackground(new java.awt.Color(102, 102, 102));
        ediit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        ediit.setForeground(new java.awt.Color(255, 255, 255));
        ediit.setText("Edit");
        ediit.setBorder(null);
        ediit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ediitActionPerformed(evt);
            }
        });
        grnPopup.add(ediit);

        remove.setBackground(new java.awt.Color(102, 102, 102));
        remove.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        remove.setForeground(new java.awt.Color(255, 255, 255));
        remove.setText("Remove");
        remove.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeActionPerformed(evt);
            }
        });
        grnPopup.add(remove);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Change Invoice");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblEditInvoice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblEditInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM ID", "PART NO", "DESCRIPTION", "UNIT PRICE", "QTY", "DISCOUNT", "ITEM PRICE", "AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblEditInvoice.setFocusable(false);
        tblEditInvoice.setRequestFocusEnabled(false);
        tblEditInvoice.setRowHeight(25);
        tblEditInvoice.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblEditInvoice.setShowVerticalLines(false);
        tblEditInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEditInvoiceMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblEditInvoiceMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblEditInvoice);
        if (tblEditInvoice.getColumnModel().getColumnCount() > 0) {
            tblEditInvoice.getColumnModel().getColumn(0).setMinWidth(0);
            tblEditInvoice.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblEditInvoice.getColumnModel().getColumn(0).setMaxWidth(0);
            tblEditInvoice.getColumnModel().getColumn(2).setPreferredWidth(200);
            tblEditInvoice.getColumnModel().getColumn(6).setMinWidth(0);
            tblEditInvoice.getColumnModel().getColumn(6).setPreferredWidth(0);
            tblEditInvoice.getColumnModel().getColumn(6).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, 730, 220));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        dateChooser.setNextFocusableComponent(txtPartNo);
        jPanel3.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 40, 130, 20));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date :*");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 40, 40, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtPartNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPartNo.setBorder(null);
        txtPartNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartNoActionPerformed(evt);
            }
        });
        txtPartNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartNoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPartNoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPartNoKeyTyped(evt);
            }
        });
        jPanel2.add(txtPartNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 210, 20));
        jPanel2.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 210, 10));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setText("Part no : *");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setText("Unit price :*");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 20, -1, -1));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 153, 153));
        jLabel9.setText("Qty :*");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 60, -1, -1));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 153, 153));
        jLabel10.setText("Discount :");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 100, 60, -1));

        txtUnitPrice.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtUnitPrice.setBorder(null);
        txtUnitPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUnitPriceKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUnitPriceKeyTyped(evt);
            }
        });
        jPanel2.add(txtUnitPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 20, 190, 20));
        jPanel2.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 40, 190, 10));

        txtQty.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtQty.setBorder(null);
        txtQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQtyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtQtyKeyTyped(evt);
            }
        });
        jPanel2.add(txtQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 60, 190, 20));
        jPanel2.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 80, 190, 10));

        txtDiscount.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtDiscount.setBorder(null);
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDiscountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });
        jPanel2.add(txtDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 100, 190, 20));
        jPanel2.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 120, 190, 10));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 3, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("(Press Enter)");
        jPanel2.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, 70, 20));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 3, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setText("(Press Enter)");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 120, 70, 20));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel2.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 50, 30, 20));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 153, 153));
        jLabel17.setText("Available qty :");
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        lbQty.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        lbQty.setText("0");
        jPanel2.add(lbQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 170, -1));

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 153, 204));
        jLabel16.setText("View Price List");
        jLabel16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 50, -1, -1));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 730, 150));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Paid :*");
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 20, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        jPanel4.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, 110, 20));
        jPanel4.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 40, 110, 20));

        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setBorder(null);
        txtPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaidKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidKeyTyped(evt);
            }
        });
        jPanel4.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 110, 20));
        jPanel4.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, 10));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Type :");
        jPanel4.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 20, 40, -1));

        comboType.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboType.setForeground(new java.awt.Color(0, 153, 153));
        comboType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "Cheque" }));
        comboType.setBorder(null);
        comboType.setOpaque(false);
        comboType.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboTypePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTypeActionPerformed(evt);
            }
        });
        jPanel4.add(comboType, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 20, 100, 25));

        lbChequeNo.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        lbChequeNo.setForeground(new java.awt.Color(0, 153, 153));
        lbChequeNo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbChequeNo.setText("000");
        jPanel4.add(lbChequeNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, 140, -1));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 153));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("  Ref :");
        jPanel4.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 20, 40, 20));

        comboRef.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        comboRef.setForeground(new java.awt.Color(0, 153, 153));
        comboRef.setBorder(null);
        comboRef.setOpaque(false);
        comboRef.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboRefPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboRef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRefActionPerformed(evt);
            }
        });
        jPanel4.add(comboRef, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 20, 170, 25));

        btnAddRef.setBackground(new java.awt.Color(255, 255, 255));
        btnAddRef.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        btnAddRef.setForeground(new java.awt.Color(0, 153, 255));
        btnAddRef.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddRef.setText("Add...");
        btnAddRef.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 153, 255)));
        btnAddRef.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddRef.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddRefMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddRefMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddRefMouseExited(evt);
            }
        });
        jPanel4.add(btnAddRef, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 50, 50, 23));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 490, 730, 80));

        txtInvNo.setEditable(false);
        txtInvNo.setBackground(new java.awt.Color(255, 255, 255));
        txtInvNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtInvNo.setBorder(null);
        txtInvNo.setNextFocusableComponent(txtPartNo);
        jPanel3.add(txtInvNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 40, 170, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 60, 170, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Invoice no : ");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 40, -1, -1));

        lbCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbCustomer.setText("SET CUSTOMER HERE");
        jPanel3.add(lbCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 260, 20));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("UPDATE");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 580, 120, 30));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblEditInvoiceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEditInvoiceMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            if (evt.isPopupTrigger() && tblEditInvoice.getSelectedRowCount() != 0) {
                grnPopup.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        }
    }//GEN-LAST:event_tblEditInvoiceMouseReleased

    private void txtPartNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNoActionPerformed

    private void txtPartNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNoKeyPressed

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        try {

            if (!(Double.parseDouble(txtTotal.getText()) == 0)) {

                validateTXT_2();
                if (TXT_VALIDATE_2 == false) {

                    if (!(Double.parseDouble(txtTotal.getText()) < Double.parseDouble(txtPaid.getText()))) {
                        if (Double.parseDouble(txtTotal.getText()) > Double.parseDouble(txtPaid.getText())) {
                            int response = JOptionPane.showConfirmDialog(this, "The amount paid is less than the total amount. Do you want continue?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (response == JOptionPane.YES_OPTION) {
                                updateInv();
//                                printPdf();
                                this.dispose();
                                NotificationPopup.update();
                            }
                        } else {
                            updateInv();
//                            printPdf();
                            this.dispose();
                            NotificationPopup.update();
                        }
                    } else {
                        NotificationPopup.incorrectPayment();
                    }

                } else {
                    NotificationPopup.fillFeilds();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void txtPartNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyReleased
        if (!txtPartNo.equals("")) {
            searchMenu.show(txtPartNo, 0, txtPartNo.getHeight());

            searchJtext();

        } else {
            searchMenu.setVisible(false);
        }

        try {

            ResultSet rs_2 = DB.search("SELECT i.*,s.* FROM items i INNER JOIN stock s ON i.iditems = s.iditems WHERE itemsPartNo = '" + txtPartNo.getText() + "' AND i.isActive = 1 ");

            if (rs_2.next()) {
                lbQty.setText("" + rs_2.getInt("qty"));
            } else {
                lbQty.setText("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtUnitPrice.grabFocus();
            searchMenu.setVisible(false);
        }

    }//GEN-LAST:event_txtPartNoKeyReleased

    private void searchtblKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchtblKeyReleased

    }//GEN-LAST:event_searchtblKeyReleased

    private void searchtblMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchtblMousePressed

    }//GEN-LAST:event_searchtblMousePressed

    private void searchtblKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchtblKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            searchtbl.grabFocus();
        }
    }//GEN-LAST:event_searchtblKeyPressed

    private void txtUnitPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnitPriceKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtUnitPriceKeyTyped

    private void txtQtyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtQtyKeyTyped

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void txtQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyKeyReleased
        try {

            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (txtPartNo.isEditable() == false) {

                    if (TXT_VALIDATE_1 == false) {

                        Double availableQty = Double.parseDouble(lbQty.getText());
                        Double qty = Double.parseDouble(txtQty.getText());

                        if (availableQty >= qty) {
                            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();

                            Double discount = null;
                            if (txtDiscount.getText().equals("")) {
                                discount = 0.0;
                            } else {
                                discount = Double.parseDouble(txtDiscount.getText().trim());
                            }

                            Double itemPrice = (Double.parseDouble(txtUnitPrice.getText()) * 1.0) / 100 * (100 - discount);

                            Double amount = (Double.parseDouble(txtUnitPrice.getText()) * Double.parseDouble(txtQty.getText())) / 100 * (100 - discount);

                            for (int i = 0; i < dtm.getRowCount(); i++) {

                                if (dtm.getValueAt(i, 1).equals(txtPartNo.getText())) {
                                    dtm.setValueAt(Decimal_Formats.Price(Double.parseDouble(txtUnitPrice.getText())), i, 3);
                                    dtm.setValueAt(Double.parseDouble(txtQty.getText()), i, 4);
                                    dtm.setValueAt(discount + " %", i, 5);
                                    dtm.setValueAt(Decimal_Formats.Price(itemPrice), i, 6);
                                    dtm.setValueAt(Decimal_Formats.Price(amount), i, 7);

                                    break;
                                }
                            }
                            NotificationPopup.update();

                            Double total = 0.0;

                            for (int x = 0; x < dtm.getRowCount(); x++) {
                                total += Double.parseDouble(dtm.getValueAt(x, 7).toString());
                            }

                            txtTotal.setText("" + Decimal_Formats.Price(total));

                            lbQty.setText("0");
                            txtPartNo.setEditable(true);
                            txtPartNo.setText(null);
                            txtUnitPrice.setText(null);
                            txtQty.setText(null);
                            txtDiscount.setText(null);
                            comboActive = false;
                            dtm.fireTableDataChanged();

                        } else {
                            NotificationPopup.noFindQty();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }

                } else {
                    DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
                    dtm.fireTableDataChanged();
                    calculation();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtQtyKeyReleased

    private void txtDiscountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyReleased
        try {

            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (txtPartNo.isEditable() == false) {

                    if (TXT_VALIDATE_1 == false) {

                        Double availableQty = Double.parseDouble(lbQty.getText());
                        Double qty = Double.parseDouble(txtQty.getText());

                        if (availableQty >= qty) {
                            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();

                            Double discount = null;
                            if (txtDiscount.getText().equals("")) {
                                discount = 0.0;
                            } else {
                                discount = Double.parseDouble(txtDiscount.getText().trim());
                            }

                            Double itemPrice = (Double.parseDouble(txtUnitPrice.getText()) * 1.0) / 100 * (100 - discount);

                            Double amount = (Double.parseDouble(txtUnitPrice.getText()) * Double.parseDouble(txtQty.getText())) / 100 * (100 - discount);

                            for (int i = 0; i < dtm.getRowCount(); i++) {

                                if (dtm.getValueAt(i, 1).equals(txtPartNo.getText())) {
                                    dtm.setValueAt(Decimal_Formats.Price(Double.parseDouble(txtUnitPrice.getText())), i, 3);
                                    dtm.setValueAt(Double.parseDouble(txtQty.getText()), i, 4);
                                    dtm.setValueAt(discount + " %", i, 5);
                                    dtm.setValueAt(Decimal_Formats.Price(itemPrice), i, 6);
                                    dtm.setValueAt(Decimal_Formats.Price(amount), i, 7);

                                    break;
                                }
                            }
                            NotificationPopup.update();

                            Double total = 0.0;

                            for (int x = 0; x < dtm.getRowCount(); x++) {
                                total += Double.parseDouble(dtm.getValueAt(x, 7).toString());
                            }

                            txtTotal.setText("" + Decimal_Formats.Price(total));

                            lbQty.setText("0");
                            txtPartNo.setEditable(true);
                            txtPartNo.setText(null);
                            txtUnitPrice.setText(null);
                            txtQty.setText(null);
                            txtDiscount.setText(null);
                            comboActive = false;
                            dtm.fireTableDataChanged();

                        } else {
                            NotificationPopup.noFindQty();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }

                } else {
                    DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
                    dtm.fireTableDataChanged();
                    calculation();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtDiscountKeyReleased

    private void tblEditInvoiceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEditInvoiceMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblEditInvoiceMouseClicked

    private void searchtblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchtblMouseClicked
        DefaultTableModel dtm = (DefaultTableModel) searchtbl.getModel();
        int selectedRow = searchtbl.getSelectedRow();

        txtPartNo.setText(dtm.getValueAt(selectedRow, 1).toString());
        searchMenu.setVisible(false);
        txtUnitPrice.grabFocus();

        try {

            ResultSet rs_2 = DB.search("SELECT i.*,s.* FROM items i INNER JOIN stock s ON i.iditems = s.iditems WHERE itemsPartNo = '" + txtPartNo.getText() + "' AND i.isActive = 1 ");

            if (rs_2.next()) {
                lbQty.setText("" + rs_2.getInt("qty"));
            } else {
                lbQty.setText("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_searchtblMouseClicked

    private void removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeActionPerformed

        int response = JOptionPane.showConfirmDialog(this, "Are you sure remove this item ?", "Remove items", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (response == JOptionPane.YES_OPTION) {
            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
            int selectedRow = tblEditInvoice.getSelectedRow();
            dtm.removeRow(selectedRow);

            if (dtm.getRowCount() != 0) {

                Double total = 0.0;

                for (int i = 0; i < dtm.getRowCount(); i++) {
                    total += Double.parseDouble(dtm.getValueAt(i, 7).toString());
                }

                txtTotal.setText("" + Decimal_Formats.Price(total));
            } else {
                txtTotal.setText("0.00");
            }

            txtPartNo.setEditable(true);
            clearTXT_1();
            txtDiscount.setText("");
            comboActive = false;
            dtm.fireTableDataChanged();

        }


    }//GEN-LAST:event_removeActionPerformed

    private void txtPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtPaidKeyTyped

    private void txtPartNoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyTyped
        char keyChar = evt.getKeyChar();
        if (Character.isLowerCase(keyChar)) {
            evt.setKeyChar(Character.toUpperCase(keyChar));
        }
    }//GEN-LAST:event_txtPartNoKeyTyped

    private void txtPaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {

                if (!(Double.parseDouble(txtTotal.getText()) == 0)) {

                    validateTXT_2();
                    if (TXT_VALIDATE_2 == false) {
                        if (!(Double.parseDouble(txtTotal.getText()) < Double.parseDouble(txtPaid.getText()))) {
                            if (Double.parseDouble(txtTotal.getText()) > Double.parseDouble(txtPaid.getText())) {
                                int response = JOptionPane.showConfirmDialog(this, "The amount paid is less than the total amount. Do you want continue?", "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (response == JOptionPane.YES_OPTION) {
                                    updateInv();
//                                    printPdf();
                                    this.dispose();
                                    NotificationPopup.update();
                                }
                            } else {
                                updateInv();
//                                printPdf();
                                this.dispose();
                                NotificationPopup.update();
                            }
                        } else {
                            NotificationPopup.incorrectPayment();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_txtPaidKeyReleased

    private void comboTypePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboTypePopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboTypePopupMenuWillBecomeInvisible

    private void comboTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTypeActionPerformed
        try {
            if (comboActive == true) {
                if (comboType.getSelectedIndex() == 1) {
                    String chequeNo = JOptionPane.showInputDialog("Enter Cheque No");

                    if (chequeNo.equals("")) {
                        comboType.setSelectedIndex(0);
                    } else {
                        lbChequeNo.setText(chequeNo);
                        lbChequeNo.setVisible(true);
                    }

                } else {
                    lbChequeNo.setText("");
                    lbChequeNo.setVisible(false);
                }
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_comboTypeActionPerformed

    private void ediitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ediitActionPerformed
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
            int selectedRow = tblEditInvoice.getSelectedRow();

            txtPartNo.setText(dtm.getValueAt(selectedRow, 1).toString());
            txtUnitPrice.setText(Decimal_Formats.Price(Double.parseDouble(dtm.getValueAt(selectedRow, 3).toString())));
            txtQty.setText(dtm.getValueAt(selectedRow, 4).toString());
            String[] discount = dtm.getValueAt(selectedRow, 5).toString().split(" ");
            txtDiscount.setText("" + discount[0]);
            txtUnitPrice.grabFocus();
            txtPartNo.setEditable(false);

            try {

                ResultSet rs_2 = DB.search("SELECT i.*,s.* FROM items i INNER JOIN stock s ON i.iditems = s.iditems WHERE itemsPartNo = '" + txtPartNo.getText() + "' AND i.isActive = 1 ");

                if (rs_2.next()) {
                    lbQty.setText("" + rs_2.getInt("qty"));
                } else {
                    lbQty.setText("0");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_ediitActionPerformed

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        txtPartNo.setEditable(true);
        clearTXT_1();
        txtDiscount.setText("");
        comboActive = false;
        DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
        dtm.fireTableDataChanged();
    }//GEN-LAST:event_lbRefeshMouseClicked

    private void txtUnitPriceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUnitPriceKeyReleased
        try {

            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (txtPartNo.isEditable() == false) {

                    if (TXT_VALIDATE_1 == false) {

                        Double availableQty = Double.parseDouble(lbQty.getText());
                        Double qty = Double.parseDouble(txtQty.getText());

                        if (availableQty >= qty) {
                            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();

                            Double discount = null;
                            if (txtDiscount.getText().equals("")) {
                                discount = 0.0;
                            } else {
                                discount = Double.parseDouble(txtDiscount.getText().trim());
                            }

                            Double itemPrice = (Double.parseDouble(txtUnitPrice.getText()) * 1.0) / 100 * (100 - discount);

                            Double amount = (Double.parseDouble(txtUnitPrice.getText()) * Double.parseDouble(txtQty.getText())) / 100 * (100 - discount);

                            for (int i = 0; i < dtm.getRowCount(); i++) {

                                if (dtm.getValueAt(i, 1).equals(txtPartNo.getText())) {
                                    dtm.setValueAt(Decimal_Formats.Price(Double.parseDouble(txtUnitPrice.getText())), i, 3);
                                    dtm.setValueAt(Double.parseDouble(txtQty.getText()), i, 4);
                                    dtm.setValueAt(discount + " %", i, 5);
                                    dtm.setValueAt(Decimal_Formats.Price(itemPrice), i, 6);
                                    dtm.setValueAt(Decimal_Formats.Price(amount), i, 7);

                                    break;
                                }
                            }

                            NotificationPopup.update();
                            Double total = 0.0;

                            for (int x = 0; x < dtm.getRowCount(); x++) {
                                total += Double.parseDouble(dtm.getValueAt(x, 7).toString());
                            }

                            txtTotal.setText("" + Decimal_Formats.Price(total));

                            lbQty.setText("0");
                            txtPartNo.setEditable(true);
                            txtPartNo.setText(null);
                            txtUnitPrice.setText(null);
                            txtQty.setText(null);
                            txtDiscount.setText(null);
                            comboActive = false;
                            dtm.fireTableDataChanged();

                        } else {
                            NotificationPopup.noFindQty();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }

                } else {
                    DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
                    dtm.fireTableDataChanged();
                    calculation();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtUnitPriceKeyReleased

    private void comboRefPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboRefPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboRefPopupMenuWillBecomeInvisible

    private void comboRefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRefActionPerformed

    }//GEN-LAST:event_comboRefActionPerformed

    private void btnAddRefMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddRefMouseClicked
        new Refferee().setVisible(true);
    }//GEN-LAST:event_btnAddRefMouseClicked

    private void btnAddRefMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddRefMouseEntered
        btnAddRef.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnAddRefMouseEntered

    private void btnAddRefMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddRefMouseExited
        btnAddRef.setBackground(Color.white);
    }//GEN-LAST:event_btnAddRefMouseExited

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        new PriceList().setVisible(true);
    }//GEN-LAST:event_jLabel16MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditInvoiceCus().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddRef;
    private javax.swing.JLabel btnSave;
    public static javax.swing.JComboBox<String> comboRef;
    public static javax.swing.JComboBox<String> comboType;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JMenuItem ediit;
    private javax.swing.JPopupMenu grnPopup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel lbChequeNo;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbCustomer;
    private javax.swing.JLabel lbQty;
    private javax.swing.JLabel lbRefesh;
    private javax.swing.JMenuItem remove;
    private javax.swing.JPopupMenu searchMenu;
    private javax.swing.JPanel searchPanel;
    public static javax.swing.JTable searchtbl;
    public static javax.swing.JTable tblEditInvoice;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtInvNo;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtPartNo;
    private javax.swing.JTextField txtQty;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtUnitPrice;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
        comboType.setOpaque(true);
        comboRef.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
        comboType.setBackground(Color.white);
        comboRef.setBackground(Color.white);
    }

    private void DesignTable() {
        tblEditInvoice.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 12));
        tblEditInvoice.getTableHeader().setOpaque(false);
        tblEditInvoice.getTableHeader().setBackground(Color.white);
        tblEditInvoice.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblEditInvoice.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblEditInvoice.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblEditInvoice.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        tblEditInvoice.getColumnModel().getColumn(7).setCellRenderer(rightRenderer);

        searchtbl.setTableHeader(null);

        DefaultTableCellRenderer rightRenderer2 = new DefaultTableCellRenderer();
        rightRenderer2.setHorizontalAlignment(JLabel.CENTER);
        searchtbl.getColumnModel().getColumn(1).setCellRenderer(rightRenderer2);
    }

    private void loadDetails() {
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT inv.*, ii.*, ip.*, i.* FROM invoice inv INNER JOIN invoiceitems ii ON inv.idinvoice = ii.idinvoice INNER JOIN invoicepayment ip ON inv.idinvoice = ip.idinvoice INNER JOIN items i ON ii.iditems = i.iditems WHERE inv.idinvoice = '" + invID + "' ");

            while (rs.next()) {

                txtInvNo.setText(invNo);
                dateChooser.setDate(rs.getDate("date"));
                txtTotal.setText(Decimal_Formats.Price(rs.getDouble("total")));
                txtPaid.setText(Decimal_Formats.Price(rs.getDouble("paid")));
                comboRef.setSelectedItem(rs.getString("referre"));

                if (rs.getString("paymentType").equals("Cash")) {
                    comboType.setSelectedIndex(0);
                } else {
                    comboType.setSelectedIndex(1);
                    String[] chequeNo = rs.getString("paymentType").split(" ");
                    lbChequeNo.setText(chequeNo[1]);
                    lbChequeNo.setVisible(true);
                }

                dtm.addRow(new Object[]{
                    rs.getString("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("descrip"),
                    Decimal_Formats.Price(rs.getDouble("sellingPrice")),
                    rs.getString("qty"),
                    rs.getString("discount") + " %",
                    Decimal_Formats.Price(((rs.getDouble("sellingPrice") * 1.0) / 100 * (100 - rs.getDouble("discount")))),
                    Decimal_Formats.Price(rs.getDouble("totalAmount"))
                });
            }

            comboActive = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadSearchTable() {

        try {

            DefaultTableModel dtm = (DefaultTableModel) searchtbl.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE i.isActive = 1");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase()
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void searchJtext() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) searchtbl.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE i.itemsPartNo LIKE '%' '" + txtPartNo.getText().toUpperCase() + "' '%' AND i.isActive = 1 ");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase()
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadReFCus() {
        try {
            comboRef.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM referee WHERE isActive = '1'");

            comboRef.addItem("");

            while (rs.next()) {
                comboRef.addItem(rs.getString("fullName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculation() {

        try {

            if (TXT_VALIDATE_1 == false) {

                DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();

                boolean itemFind = false;
                Integer itemId = null;
                String description = null;

                ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE i.itemsPartNo = '" + txtPartNo.getText() + "' ");

                if (rs.next()) {
                    itemId = rs.getInt("iditems");
                    description = rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase();
                    itemFind = true;
                } else {
                    NotificationPopup.noFindItem();
                    itemFind = false;
                    txtPartNo.grabFocus();
                }

                if (itemFind == true) {

                    int availableQty = Integer.parseInt(lbQty.getText());
                    int qty = Integer.parseInt(txtQty.getText());

                    if (availableQty >= qty) {
                        Double discount = null;
                        boolean already = false;

                        if (dtm.getRowCount() != 0) {

                            for (int x = 0; x < dtm.getRowCount(); x++) {
                                if (itemId == Integer.parseInt(dtm.getValueAt(x, 0).toString())) {
                                    already = true;
                                    txtUnitPrice.setText(null);
                                    txtQty.setText(null);
                                    txtDiscount.setText(null);
                                    txtPartNo.grabFocus();
                                    NotificationPopup.already();
                                    break;
                                }
                            }

                            if (already == false) {
                                if (txtDiscount.getText().equals("")) {
                                    discount = 0.0;
                                } else {
                                    discount = Double.parseDouble(txtDiscount.getText().trim());
                                }

                                Double itemPrice = (Double.parseDouble(txtUnitPrice.getText()) * 1.0) / 100 * (100 - discount);

                                Double amount = (Double.parseDouble(txtUnitPrice.getText()) * Double.parseDouble(txtQty.getText())) / 100 * (100 - discount);

                                dtm.addRow(new Object[]{
                                    itemId,
                                    txtPartNo.getText(),
                                    description,
                                    Decimal_Formats.Price(Double.parseDouble(txtUnitPrice.getText())),
                                    Double.parseDouble(txtQty.getText()),
                                    discount + " %",
                                    Decimal_Formats.Price(itemPrice),
                                    Decimal_Formats.Price(amount)
                                });

                                Double total = 0.0;

                                for (int i = 0; i < dtm.getRowCount(); i++) {
                                    total += Double.parseDouble(dtm.getValueAt(i, 7).toString());
                                }

                                txtTotal.setText("" + Decimal_Formats.Price(total));

                                clearTXT_1();
                            }

                        } else {
                            if (txtDiscount.getText().equals("")) {
                                discount = 0.0;
                            } else {
                                discount = Double.parseDouble(txtDiscount.getText().trim());
                            }

                            Double itemPrice = (Double.parseDouble(txtUnitPrice.getText()) * 1.0) / 100 * (100 - discount);

                            Double amount = (Double.parseDouble(txtUnitPrice.getText()) * Double.parseDouble(txtQty.getText())) / 100 * (100 - discount);

                            dtm.addRow(new Object[]{
                                itemId,
                                txtPartNo.getText(),
                                description,
                                Decimal_Formats.Price(Double.parseDouble(txtUnitPrice.getText())),
                                Double.parseDouble(txtQty.getText()),
                                discount + " %",
                                Decimal_Formats.Price(itemPrice),
                                Decimal_Formats.Price(amount)
                            });

                            Double total = 0.0;

                            for (int i = 0; i < dtm.getRowCount(); i++) {
                                total += Double.parseDouble(dtm.getValueAt(i, 7).toString());
                            }

                            txtTotal.setText("" + Decimal_Formats.Price(total));

                            clearTXT_1();
                        }
                    } else {
                        NotificationPopup.noFindQty();
                    }

                } else {
                    NotificationPopup.noFindItem();
                }

            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateInv() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblEditInvoice.getModel();

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

            String[] getInvNo = invNo.split("-");

            String updateInv = "UPDATE invoice SET invoiceNo = '" + getInvNo[1] + "', invoiceDate = '" + date + "', invoiceTotal = '" + Double.parseDouble(txtTotal.getText()) + "', referre = '" + comboRef.getSelectedItem() + "' WHERE idinvoice = '" + invID + "' ";
            DB.idu(updateInv);

            ResultSet invItems = DB.search("SELECT * FROM invoiceitems WHERE idinvoice = '" + invID + "' ");

            while (invItems.next()) {
                ResultSet stock = DB.search("SELECT * FROM stock WHERE iditems = '" + invItems.getString("iditems") + "' ");

                if (stock.next()) {
                    String updateStock = "UPDATE stock SET qty = '" + (stock.getDouble("qty") + invItems.getDouble("qty")) + "' WHERE iditems = '" + invItems.getString("iditems") + "' ";
                    DB.idu(updateStock);
                }

            }

            String deleteItems = "DELETE FROM invoiceitems WHERE idinvoice = '" + invID + "' ";
            DB.idu(deleteItems);

            for (int i = 0; i < tblEditInvoice.getRowCount(); i++) {

                String[] getDiscount = dtm.getValueAt(i, 5).toString().split(" ");
                Double discount = Double.parseDouble(getDiscount[0]);

                ResultSet newStock = DB.search("SELECT * FROM stock WHERE iditems = '" + dtm.getValueAt(i, 0).toString() + "' ");

                if (newStock.next()) {
                    String updateStock = "UPDATE stock SET qty = '" + (newStock.getDouble("qty") - Double.parseDouble(dtm.getValueAt(i, 4).toString())) + "' WHERE iditems = '" + dtm.getValueAt(i, 0).toString() + "' ";
                    DB.idu(updateStock);
                }

                String insertInvItems = "INSERT INTO invoiceitems (sellingPrice, qty, discount, totalAmount,descrip, idinvoice, iditems) VALUES ('" + Double.parseDouble(dtm.getValueAt(i, 3).toString()) + "', '" + Double.parseDouble(dtm.getValueAt(i, 4).toString()) + "', '" + discount + "', '" + Double.parseDouble(dtm.getValueAt(i, 7).toString()) + "', '" + dtm.getValueAt(i, 2).toString().toUpperCase() + "', '" + invID + "', '" + dtm.getValueAt(i, 0).toString() + "' )";
                DB.idu(insertInvItems);

            }

            String payType = null;

            if (comboType.getSelectedIndex() == 1) {
                payType = comboType.getSelectedItem().toString() + " (" + lbChequeNo.getText().toLowerCase() + ")";
            } else {
                payType = comboType.getSelectedItem().toString();
            }

            String updateInvPay = "UPDATE invoicepayment SET total = '" + Double.parseDouble(txtTotal.getText()) + "', paid = '" + Double.parseDouble(txtPaid.getText()) + "', date = '" + date + "', paymentType = '" + payType + "' WHERE idinvoice = '" + invID + "' ";
            DB.idu(updateInvPay);

            if (Double.parseDouble(txtPaid.getText()) != 0) {
                if (prePaid > Double.parseDouble(txtPaid.getText())) {
                    
                    String insertOutstand = "INSERT INTO invoiceoutsatnding(outstandDate, addition, status, idinvoice)"
                            + "VALUES ('" + date + "', '" + (prePaid - Double.parseDouble(txtPaid.getText())) + "', 'Change paid amount', '" + invID + "')";

                    DB.idu(insertOutstand);
                    
                    String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + 0.0 + "', '" + (prePaid - Double.parseDouble(txtPaid.getText())) + "', 'INV-' '" + txtInvNo.getText() + "' ' Change customer paid amount' ) ";
                    DB.idu(insertCashbook);
                    
                } else if (prePaid < Double.parseDouble(txtPaid.getText())) {
                    
                    String insertOutstand = "INSERT INTO invoiceoutsatnding(outstandDate, deduction, status, idinvoice)"
                            + "VALUES ('" + date + "', '" + (Double.parseDouble(txtPaid.getText()) - prePaid) + "', 'Change paid amount', '" + invID + "')";

                    DB.idu(insertOutstand);
                    
                    String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + (Double.parseDouble(txtPaid.getText()) - prePaid) + "', '" + 0.0 + "', 'INV-' '" + txtInvNo.getText() + "' ' Change customer paid amount' ) ";
                    DB.idu(insertCashbook);
                }
            }

            txtPartNo.setEditable(true);
            clearTXT_1();
            txtDiscount.setText("");
            comboActive = false;
            dtm.fireTableDataChanged();
            loadInvoice();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void validateTXT_1() {

        Component[] cp = jPanel2.getComponents();
        for (Component c : cp) {

            if (c instanceof JTextField) {

                if ((((JTextField) c).getText().isEmpty())) {

                    TXT_VALIDATE_1 = true;

                    break;

                } else {

                    TXT_VALIDATE_1 = false;

                }

            }

        }
    }

    private void validateTXT_2() {
        if (txtInvNo.getText().equals("")) {
            txtInvNo.grabFocus();
            TXT_VALIDATE_2 = true;
        } else if (txtPaid.getText().equals("")) {
            txtPaid.grabFocus();
            TXT_VALIDATE_2 = true;
        } else if (dateChooser.getDate() == null) {
            dateChooser.grabFocus();
            TXT_VALIDATE_2 = true;
        } else {
            TXT_VALIDATE_2 = false;
        }
    }

    private void clearTXT_1() {
        txtPartNo.setText(null);
        txtUnitPrice.setText(null);
        txtQty.setText(null);
        txtPartNo.grabFocus();
        lbQty.setText("0");
    }

//    private void clearTXT_2() {
//        txtBillNo.setText(null);
//        txtPaid.setText(null);
//        txtDiscount.setText(null);
//        txtItems.setText("0");
//        txtTotal.setText("0.00");
//        DefaultTableModel dtm = (DefaultTableModel) tblGrn.getModel();
//        dtm.setRowCount(0);
//        txtBillNo.grabFocus();
//    }
    private void printPdf() {
        try {

            File dir = new File("C:/Reports/" + dateForm.currentdateFolder());

            if (!dir.exists()) {

                try {

                    boolean mkdir = dir.mkdirs();

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

            InputStream in = new FileInputStream(new File("D:\\DanajayaMotors\\grn.jrxml"));
            JasperDesign jd = JRXmlLoader.load(in);
            String sql = "SELECT s.*, i.*, g.*, gi.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnitems gi ON g.idgrn = gi.idGrn INNER JOIN grnpayment gp ON g.idgrn = gp.idGrn INNER JOIN items i ON i.iditems = gi.iditems WHERE g.billNo = '" + txtInvNo.getText() + "' ";
            JRDesignQuery newQuery = new JRDesignQuery();
            newQuery.setText(sql);
            jd.setQuery(newQuery);
            JasperReport jr = JasperCompileManager.compileReport(jd);
            HashMap para = new HashMap();
            JasperPrint j = JasperFillManager.fillReport(jr, para, DB.getCon());
            //JasperViewer.viewReport(j, false);

            String path = dir.toString() + "/" + "GR-" + txtInvNo.getText() + ".pdf";
            JasperExportManager.exportReportToPdfFile(j, path);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
