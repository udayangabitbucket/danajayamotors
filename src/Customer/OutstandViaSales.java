package Customer;

import static Customer.ViewAllInvSalse.loadInvoice;
import static Customer.ViewAllInvSalse.tblInvSales;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import Items.AddItems;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class OutstandViaSales extends javax.swing.JFrame {

    boolean TXT_VALIDATE_1 = false;
    boolean TXT_VALIDATE_2 = false;

    Integer invId;
    String invoiceNo;

    public OutstandViaSales() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOutstandSales = new javax.swing.JTable();
        lbInvoice = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnSave = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        txtPiadTotal = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        txtArrears = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        lbChequeNo = new javax.swing.JLabel();
        lbCustomer = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Payments");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblOutstandSales.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblOutstandSales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "INVOICE ID", "DATE", "ADDITIONS", "DEDUCTIONS", "PAYMENT TYPE", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOutstandSales.setFocusable(false);
        tblOutstandSales.setRequestFocusEnabled(false);
        tblOutstandSales.setRowHeight(30);
        tblOutstandSales.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblOutstandSales.setShowVerticalLines(false);
        tblOutstandSales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOutstandSalesMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblOutstandSalesMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblOutstandSales);
        if (tblOutstandSales.getColumnModel().getColumnCount() > 0) {
            tblOutstandSales.getColumnModel().getColumn(0).setMinWidth(0);
            tblOutstandSales.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblOutstandSales.getColumnModel().getColumn(0).setMaxWidth(0);
            tblOutstandSales.getColumnModel().getColumn(1).setPreferredWidth(50);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, 740, 230));

        lbInvoice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbInvoice.setText("SET INVOICE NO HERE");
        jPanel3.add(lbInvoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 50, 250, 20));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pay Outstanding", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("PAY");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel4.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 200, 200, 30));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Pay : *");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 90, -1, -1));

        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setBorder(null);
        txtPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaidKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidKeyTyped(evt);
            }
        });
        jPanel4.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 90, 200, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 110, 200, 10));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 40, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 30, 200, 20));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 153));
        jLabel15.setText("Paid :");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 40, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Arrears :");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));
        jPanel2.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 200, 20));

        txtPiadTotal.setEditable(false);
        txtPiadTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtPiadTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPiadTotal.setText("0.00");
        txtPiadTotal.setBorder(null);
        txtPiadTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPiadTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtPiadTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 200, 20));
        jPanel2.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, 200, 20));

        txtArrears.setEditable(false);
        txtArrears.setBackground(new java.awt.Color(255, 255, 255));
        txtArrears.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtArrears.setText("0.00");
        txtArrears.setBorder(null);
        txtArrears.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtArrearsKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtArrearsKeyTyped(evt);
            }
        });
        jPanel2.add(txtArrears, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, 200, 20));
        jPanel2.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 200, 10));

        jPanel4.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 310, 200));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date : *");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 50, -1, -1));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel4.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 50, 150, -1));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 153, 153));
        jLabel17.setText("Payment Type : *");
        jPanel4.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 140, -1, -1));

        jCheckBox2.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jCheckBox2);
        jCheckBox2.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jCheckBox2.setForeground(new java.awt.Color(0, 153, 153));
        jCheckBox2.setSelected(true);
        jCheckBox2.setText("Cash");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });
        jPanel4.add(jCheckBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 140, -1, 20));

        jCheckBox1.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jCheckBox1);
        jCheckBox1.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(0, 153, 153));
        jCheckBox1.setText("Cheque");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel4.add(jCheckBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 140, -1, 20));

        lbChequeNo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbChequeNo.setText("set cheque no here");
        jPanel4.add(lbChequeNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 170, 240, -1));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 740, 270));

        lbCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbCustomer.setText("SET CUSTOMER HERE");
        jPanel3.add(lbCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 250, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblOutstandSalesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandSalesMouseReleased

    }//GEN-LAST:event_tblOutstandSalesMouseReleased

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        savePaid();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void tblOutstandSalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandSalesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblOutstandSalesMouseClicked

    private void txtArrearsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyTyped
        char c = evt.getKeyChar();
        if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtArrearsKeyTyped

    private void txtArrearsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyReleased

    }//GEN-LAST:event_txtArrearsKeyReleased

    private void txtPaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            savePaid();
        }
    }//GEN-LAST:event_txtPaidKeyReleased

    private void txtPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtPaidKeyTyped

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPiadTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPiadTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPiadTotalActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OutstandViaSales().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbChequeNo;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbCustomer;
    private javax.swing.JLabel lbInvoice;
    public static javax.swing.JTable tblOutstandSales;
    private javax.swing.JTextField txtArrears;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtPiadTotal;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    private void DesignTable() {
        tblOutstandSales.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 12));
        tblOutstandSales.getTableHeader().setOpaque(false);
        tblOutstandSales.getTableHeader().setBackground(Color.white);
        tblOutstandSales.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblOutstandSales.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblOutstandSales.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
    }

    private void loadDetails() {
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            DefaultTableModel dtm_1 = (DefaultTableModel) tblInvSales.getModel();
            int selectedRow = tblInvSales.getSelectedRow();

            DefaultTableModel dtm_2 = (DefaultTableModel) tblOutstandSales.getModel();
            dtm_2.setRowCount(0);

            dateChooser.setDate(new Date());
            invId = Integer.parseInt(dtm_1.getValueAt(selectedRow, 0).toString());
            lbInvoice.setText(dtm_1.getValueAt(0, 2).toString());
            invoiceNo = dtm_1.getValueAt(selectedRow, 3).toString();
            txtTotal.setText(Decimal_Formats.Price(Double.parseDouble(dtm_1.getValueAt(selectedRow, 5).toString())));
            txtPiadTotal.setText(Decimal_Formats.Price(Double.parseDouble(dtm_1.getValueAt(selectedRow, 6).toString())));

            ResultSet rs = DB.search("SELECT * FROM invoiceoutsatnding WHERE idinvoice = '" + invId + "' ORDER BY outstandDate DESC ");

            while (rs.next()) {
                dtm_2.addRow(new Object[]{
                    rs.getInt("idinvoice"),
                    rs.getString("outstandDate"),
                    Decimal_Formats.Price(rs.getDouble("addition")),
                    Decimal_Formats.Price(rs.getDouble("deduction")),
                    rs.getString("status")
                });
            }

            txtArrears.setText(Decimal_Formats.Price(Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtPiadTotal.getText())));

            txtPaid.grabFocus();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void savePaid() {
        try {

            if (!txtPaid.getText().equals(null)) {
                if (!(Double.parseDouble(txtArrears.getText()) < Double.parseDouble(txtPaid.getText()))) {

                    if (Double.parseDouble(txtPaid.getText()) != 0) {
                        DefaultTableModel dtm = (DefaultTableModel) tblOutstandSales.getModel();

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

                        String insertOutstand = "INSERT INTO invoiceoutsatnding(outstandDate, deduction, status, idinvoice)"
                                + "VALUES ('" + date + "', '" + Double.parseDouble(txtPaid.getText()) + "', 'outstanding payment', '" + invId + "')";

                        DB.idu(insertOutstand);

                        Double paid = null;

                        ResultSet rs = DB.search("SELECT * FROM invoicepayment WHERE idinvoice = '" + invId + "' ");

                        if (rs.next()) {
                            paid = rs.getDouble("paid");
                        }

                        Double newPaid = paid + Double.parseDouble(txtPaid.getText());

                        String updatePayment = "UPDATE invoicepayment SET paid = '" + newPaid + "' WHERE idinvoice = '" + invId + "' ";
                        DB.idu(updatePayment);

                        String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + Double.parseDouble(txtPaid.getText()) + "', '" + 0.0 + "', '(" + invoiceNo + ") ' ' customer outstnding amount' ) ";
                        DB.idu(insertCashbook);

                        ResultSet rs1 = DB.search("SELECT ip.*, io.* FROM invoicepayment ip INNER JOIN invoiceoutsatnding io ON ip.idinvoice = io.idinvoice WHERE io.idinvoice = '" + invId + "' ORDER BY io.outstandDate DESC ");
                        dtm.setRowCount(0);
                        while (rs1.next()) {
                            dtm.addRow(new Object[]{
                                rs1.getInt("idinvoice"),
                                rs1.getString("outstandDate"),
                                Decimal_Formats.Price(rs1.getDouble("addition")),
                                Decimal_Formats.Price(rs1.getDouble("deduction")),
                                rs1.getString("status")
                            });

                            txtTotal.setText(Decimal_Formats.Price(rs1.getDouble("total")));
                            txtPiadTotal.setText(Decimal_Formats.Price(rs1.getDouble("paid")));

                            txtArrears.setText(Decimal_Formats.Price(rs1.getDouble("total") - rs1.getDouble("paid")));

                        }

                        txtPaid.setText("");
                        txtPaid.grabFocus();
                        loadInvoice();
                        NotificationPopup.save();
                    }

                } else {
                    NotificationPopup.incorrectPayment();
                }
            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
