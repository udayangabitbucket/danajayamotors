package Customer;

import ExClasses.DB;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.dateForm;
import static MainMenu.Login.UserPrivilage;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class OutstandingList extends javax.swing.JFrame {

    public static Map<String, Integer> customer = new HashMap();

    public OutstandingList() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadCustomer();
        loadReF();
        loadAllInv();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAllInvoice = new javax.swing.JTable();
        lbRefesh = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtBalance = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        comboCustomer = new javax.swing.JComboBox<>();
        btnPrint = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        comboRef = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Outstanding List");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 140, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblAllInvoice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblAllInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "INVOICE ID", "CUSTOMER ID", "CUSTOMER", "INVOICE NO", "DATE", "TOTAL", "PAID", "BALANCE", "REFEREE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblAllInvoice.setFocusable(false);
        tblAllInvoice.setRowHeight(35);
        tblAllInvoice.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblAllInvoice.setShowVerticalLines(false);
        tblAllInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblAllInvoiceMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblAllInvoice);
        if (tblAllInvoice.getColumnModel().getColumnCount() > 0) {
            tblAllInvoice.getColumnModel().getColumn(0).setMinWidth(0);
            tblAllInvoice.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblAllInvoice.getColumnModel().getColumn(0).setMaxWidth(0);
            tblAllInvoice.getColumnModel().getColumn(1).setMinWidth(0);
            tblAllInvoice.getColumnModel().getColumn(1).setPreferredWidth(0);
            tblAllInvoice.getColumnModel().getColumn(1).setMaxWidth(0);
            tblAllInvoice.getColumnModel().getColumn(2).setPreferredWidth(130);
            tblAllInvoice.getColumnModel().getColumn(7).setPreferredWidth(100);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 700, 410));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel3.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 80, 30, 20));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Total :");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 110, 20));
        jPanel2.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, 110, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Paid :");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, -1, -1));

        txtPaid.setEditable(false);
        txtPaid.setBackground(new java.awt.Color(255, 255, 255));
        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setText("0.00");
        txtPaid.setBorder(null);
        txtPaid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPaidActionPerformed(evt);
            }
        });
        jPanel2.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, 110, 20));
        jPanel2.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 40, 110, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Balance :");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, -1, -1));

        txtBalance.setEditable(false);
        txtBalance.setBackground(new java.awt.Color(255, 255, 255));
        txtBalance.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtBalance.setText("0.00");
        txtBalance.setBorder(null);
        txtBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceActionPerformed(evt);
            }
        });
        jPanel2.add(txtBalance, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 110, 20));
        jPanel2.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, 110, 10));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 550, 580, 60));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 153, 153));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Customer :");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 90, 20));

        comboCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        comboCustomer.setForeground(new java.awt.Color(0, 153, 153));
        comboCustomer.setBorder(null);
        comboCustomer.setOpaque(false);
        comboCustomer.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboCustomerPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCustomerActionPerformed(evt);
            }
        });
        jPanel3.add(comboCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 73, 360, 30));

        btnPrint.setBackground(new java.awt.Color(255, 255, 255));
        btnPrint.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(0, 153, 153));
        btnPrint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPrint.setText("Print");
        btnPrint.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnPrint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPrintMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPrintMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPrintMouseExited(evt);
            }
        });
        jPanel3.add(btnPrint, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 575, 90, 30));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 153));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel15.setText("Referee :");
        jPanel3.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 50, 70, 20));

        comboRef.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        comboRef.setForeground(new java.awt.Color(0, 153, 153));
        comboRef.setBorder(null);
        comboRef.setOpaque(false);
        comboRef.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboRefPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboRef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRefActionPerformed(evt);
            }
        });
        jPanel3.add(comboRef, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 70, 220, 30));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblAllInvoiceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblAllInvoiceMouseReleased

    }//GEN-LAST:event_tblAllInvoiceMouseReleased

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        DefaultTableModel dtm = (DefaultTableModel) tblAllInvoice.getModel();
        dtm.setRowCount(0);
        comboCustomer.setSelectedIndex(0);
        comboRef.setSelectedIndex(0);
        loadAllInv();
    }//GEN-LAST:event_lbRefeshMouseClicked

    private void txtBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPaidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPaidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPaidActionPerformed

    private void btnPrintMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPrintMouseClicked
        try {

            int response = JOptionPane.showConfirmDialog(this, "Do you want to print?", "Print", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {

                InputStream in = new FileInputStream(new File("D:\\DanajayaMotors\\outstanding.jrxml"));
                JasperDesign jd = JRXmlLoader.load(in);
                String sql = null;

                if (comboCustomer.getSelectedIndex() != 0) {

                    Integer customerId = customer.get(comboCustomer.getSelectedItem());

                    sql = "SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE ip.total != ip.paid AND c.status != 'temporory' AND i.idcustomer = '" + customerId + "'  ORDER BY c.customerName";

                } else if (comboRef.getSelectedIndex() != 0) {

                    sql = "SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE ip.total != ip.paid AND c.status != 'temporory' AND i.referre = '" + comboRef.getSelectedItem().toString() + "'  ORDER BY c.customerName";

                } else {

                    sql = "SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE ip.total != ip.paid AND c.status != 'temporory'  ORDER BY c.customerName";

                }

                JRDesignQuery newQuery = new JRDesignQuery();
                newQuery.setText(sql);
                jd.setQuery(newQuery);
                JasperReport jr = JasperCompileManager.compileReport(jd);
                HashMap para = new HashMap();
                JasperPrint j = JasperFillManager.fillReport(jr, para, DB.getCon());
                //JasperViewer.viewReport(j, false);

                //String path = dir.toString() + "/" + "Outstanding" + ".pdf";
                //JasperExportManager.exportReportToPdfFile(j, path);
                JasperPrintManager.printReport(j, false);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnPrintMouseClicked

    private void btnPrintMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPrintMouseEntered
        btnPrint.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnPrintMouseEntered

    private void btnPrintMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPrintMouseExited
        btnPrint.setBackground(Color.white);
    }//GEN-LAST:event_btnPrintMouseExited

    private void comboCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCustomerActionPerformed

    }//GEN-LAST:event_comboCustomerActionPerformed

    private void comboCustomerPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboCustomerPopupMenuWillBecomeInvisible
        try {

            if (comboCustomer.getSelectedIndex() != 0) {

                comboRef.setSelectedIndex(0);

                DefaultTableModel dtm = (DefaultTableModel) tblAllInvoice.getModel();
                dtm.setRowCount(0);

                Integer customerId = customer.get(comboCustomer.getSelectedItem());

                ResultSet rs = DB.search("SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE i.idcustomer = '" + customerId + "' ORDER BY c.customerName ASC");

                while (rs.next()) {

                    if (rs.getDouble("invoiceTotal") != rs.getDouble("paid")) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idinvoice"),
                            rs.getInt("idcustomer"),
                            rs.getString("customerName"),
                            "INV-" + rs.getString("invoiceNo"),
                            rs.getString("invoiceDate"),
                            Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                            Decimal_Formats.Price(rs.getDouble("paid")),
                            Decimal_Formats.Price(rs.getDouble("invoiceTotal") - rs.getDouble("paid")),
                            rs.getString("referre")
                        });
                    }

                }

                calculate();

            } else {
                comboRef.setSelectedIndex(0);
                loadAllInv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboCustomerPopupMenuWillBecomeInvisible

    private void comboRefPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboRefPopupMenuWillBecomeInvisible
        try {

            if (comboRef.getSelectedIndex() != 0) {

                comboCustomer.setSelectedIndex(0);

                DefaultTableModel dtm = (DefaultTableModel) tblAllInvoice.getModel();
                dtm.setRowCount(0);

                ResultSet rs = DB.search("SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE i.referre = '" + comboRef.getSelectedItem().toString() + "' ORDER BY c.customerName ASC");

                while (rs.next()) {

                    if (rs.getDouble("invoiceTotal") != rs.getDouble("paid")) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idinvoice"),
                            rs.getInt("idcustomer"),
                            rs.getString("customerName"),
                            "INV-" + rs.getString("invoiceNo"),
                            rs.getString("invoiceDate"),
                            Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                            Decimal_Formats.Price(rs.getDouble("paid")),
                            Decimal_Formats.Price(rs.getDouble("invoiceTotal") - rs.getDouble("paid")),
                            rs.getString("referre")
                        });
                    }

                }

                calculate();

            } else {
                comboCustomer.setSelectedIndex(0);
                loadAllInv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboRefPopupMenuWillBecomeInvisible

    private void comboRefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRefActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboRefActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OutstandingList().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnPrint;
    public static javax.swing.JComboBox<String> comboCustomer;
    public static javax.swing.JComboBox<String> comboRef;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbRefesh;
    public static javax.swing.JTable tblAllInvoice;
    public static javax.swing.JTextField txtBalance;
    public static javax.swing.JTextField txtPaid;
    public static javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        comboCustomer.setOpaque(true);
        comboRef.setOpaque(true);
        btnPrint.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        comboCustomer.setBackground(Color.white);
        comboRef.setBackground(Color.white);
    }

    private void DesignTable() {
        tblAllInvoice.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblAllInvoice.getTableHeader().setOpaque(false);
        tblAllInvoice.getTableHeader().setBackground(Color.white);
        tblAllInvoice.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblAllInvoice.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblAllInvoice.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblAllInvoice.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        tblAllInvoice.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
        tblAllInvoice.getColumnModel().getColumn(7).setCellRenderer(rightRenderer);
        tblAllInvoice.getColumnModel().getColumn(8).setCellRenderer(rightRenderer);
    }

    public static void loadAllInv() {
        try {
            ResultSet rs = DB.search("SELECT c.*, i.*, ip.* FROM customer c INNER JOIN invoice i ON c.idcustomer = i.idcustomer INNER JOIN invoicepayment ip ON ip.idinvoice = i.idinvoice WHERE c.status != 'temporory' AND i.isActive = '1' ORDER BY c.customerName ASC");

            DefaultTableModel dtm = (DefaultTableModel) tblAllInvoice.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                if (rs.getDouble("invoiceTotal") != rs.getDouble("paid")) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idinvoice"),
                        rs.getInt("idcustomer"),
                        rs.getString("customerName"),
                        "INV-" + rs.getString("invoiceNo"),
                        rs.getString("invoiceDate"),
                        Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                        Decimal_Formats.Price(rs.getDouble("paid")),
                        Decimal_Formats.Price(rs.getDouble("invoiceTotal") - rs.getDouble("paid")),
                        rs.getString("referre")
                    });
                }

            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadReF() {
        try {
            comboRef.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM referee WHERE isActive = '1'");

            comboRef.addItem("");

            while (rs.next()) {
                comboRef.addItem(rs.getString("fullName"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculate() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblAllInvoice.getModel();
            Double total = 0.0;
            Double paid = 0.0;
            Double balance = 0.0;

            for (int i = 0; i < dtm.getRowCount(); i++) {
                total += Double.parseDouble(dtm.getValueAt(i, 5).toString());
                paid += Double.parseDouble(dtm.getValueAt(i, 6).toString());
            }

            balance = total - paid;

            txtTotal.setText("" + Decimal_Formats.Price(total));
            txtPaid.setText("" + Decimal_Formats.Price(paid));
            txtBalance.setText("" + Decimal_Formats.Price(balance));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomer() {
        try {
            comboCustomer.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM customer WHERE status != 'temporory' AND customerName != 'SALES CUSTOMER' AND isActive = '1'");

            comboCustomer.addItem("");

            while (rs.next()) {
                comboCustomer.addItem(rs.getString("customerName"));
                customer.put(rs.getString("customerName"), rs.getInt("idcustomer"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
