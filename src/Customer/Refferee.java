package Customer;

import static Customer.AddInvoice.loadReferee;
import static Customer.EditInvoiceAll.loadReFAll;
import static Customer.EditInvoiceCus.loadReFCus;
import static Customer.EditInvoiceSales.loadReFSales;
import Items.*;
import ExClasses.DB;
import ExClasses.NotificationPopup;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Refferee extends javax.swing.JFrame {
    
    public Refferee() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        tableASC();
        getReferee();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        refereePopUp = new javax.swing.JPopupMenu();
        popupEdit = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRef = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();

        refereePopUp.setBackground(new java.awt.Color(102, 102, 102));
        refereePopUp.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        refereePopUp.setOpaque(false);
        refereePopUp.setPreferredSize(new java.awt.Dimension(109, 25));

        popupEdit.setBackground(new java.awt.Color(102, 102, 102));
        popupEdit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        popupEdit.setForeground(new java.awt.Color(255, 255, 255));
        popupEdit.setText("Edit Referee");
        popupEdit.setBorder(null);
        popupEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupEditActionPerformed(evt);
            }
        });
        refereePopUp.add(popupEdit);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Referees");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblRef.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblRef.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "REFEREE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblRef.setFocusable(false);
        tblRef.setOpaque(false);
        tblRef.setRowHeight(35);
        tblRef.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblRef.setShowVerticalLines(false);
        tblRef.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblRefMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblRefMouseReleased(evt);
            }
        });
        tblRef.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblRefKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblRefKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblRef);
        if (tblRef.getColumnModel().getColumnCount() > 0) {
            tblRef.getColumnModel().getColumn(0).setMinWidth(0);
            tblRef.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblRef.getColumnModel().getColumn(0).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 290, 430));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 260, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, 260, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204,0,0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblRefMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblRefMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            if (evt.isPopupTrigger() && tblRef.getSelectedRowCount() != 0) {
                refereePopUp.show(evt.getComponent(), evt.getX(), evt.getY());
            }

        }

    }//GEN-LAST:event_tblRefMouseReleased

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                if (!txtSearch.getText().isEmpty()) {

                    ResultSet rs = DB.search("SELECT * FROM referee WHERE fullName = '" + txtSearch.getText().toUpperCase() + "' AND isActive = 1 ");

                    if (!rs.next()) {

                        String insertRef = "INSERT INTO referee (fullName) VALUES('" + txtSearch.getText().toUpperCase() + "')";
                        DB.idu(insertRef);
                        getReferee();
                        txtSearch.setText("");
                        loadReferee();
                        loadReFAll();
                        loadReFCus();
                        loadReFSales();
                        
                    } else {
                        NotificationPopup.already();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_txtSearchKeyPressed

    private void popupEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupEditActionPerformed
        new RefEdit().setVisible(true);
    }//GEN-LAST:event_popupEditActionPerformed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            ResultSet rs = DB.search("SELECT * FROM referee WHERE fullName LIKE '%' '" + txtSearch.getText().toUpperCase() + "' '%'");

            DefaultTableModel dtm = (DefaultTableModel) tblRef.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idreferee"),
                    rs.getString("fullName")});
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_txtSearchKeyReleased

    private void tblRefMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblRefMousePressed
//        JTable table = (JTable) evt.getSource();
//        Point point = evt.getPoint();
//        int row = table.rowAtPoint(point);
//        if (evt.getClickCount() == 2 && table.getSelectedRow() != -1) {
//            setComboBrand();
//            this.dispose();
//        }
    }//GEN-LAST:event_tblRefMousePressed

    private void tblRefKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblRefKeyReleased

    }//GEN-LAST:event_tblRefKeyReleased

    private void tblRefKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblRefKeyPressed

    }//GEN-LAST:event_tblRefKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Refferee().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    private javax.swing.JMenuItem popupEdit;
    private javax.swing.JPopupMenu refereePopUp;
    public static javax.swing.JTable tblRef;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        refereePopUp.setOpaque(true);
        tblRef.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        tblRef.setBackground(new Color(255, 255, 255));
    }

    private void DesignTable() {
        tblRef.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblRef.getTableHeader().setOpaque(false);
        tblRef.getTableHeader().setBackground(Color.white);
        tblRef.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblRef.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
    }

    private void tableASC() {

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tblRef.getModel());
        tblRef.setRowSorter(sorter);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();

    }

    public static void getReferee() {
        try {
            ResultSet rs = DB.search("SELECT * FROM referee WHERE isActive = '1'");

            DefaultTableModel dtm = (DefaultTableModel) tblRef.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idreferee"),
                    rs.getString("fullName")});
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
