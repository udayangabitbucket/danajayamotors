package Customer;

import static Customer.ViewAllInvSalse.tblInvSales;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import static Customer.ViewInvItemsale.tblInvItemSale;
import javax.swing.ImageIcon;

public class ViewInvItemsale extends javax.swing.JFrame {

    public ViewInvItemsale() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadItems();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtItems = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblInvItemSale = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Invoice items");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 153, 153));
        jLabel11.setText("Items :");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 620, -1, -1));

        txtItems.setEditable(false);
        txtItems.setBackground(new java.awt.Color(255, 255, 255));
        txtItems.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItems.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItems.setText("0");
        txtItems.setBorder(null);
        jPanel3.add(txtItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 620, 50, 20));
        jPanel3.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 640, 50, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel3.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 620, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel3.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 620, 170, 20));
        jPanel3.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 640, 170, 10));

        jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane3.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblInvItemSale.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblInvItemSale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM ID", "PART NO", "DESCRIPTION", "UNIT PRICE", "QTY", "DISCOUNT", "ITEM PRICE", "AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblInvItemSale.setFocusable(false);
        tblInvItemSale.setRowHeight(30);
        tblInvItemSale.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblInvItemSale.setShowVerticalLines(false);
        tblInvItemSale.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblInvItemSaleMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tblInvItemSale);
        if (tblInvItemSale.getColumnModel().getColumnCount() > 0) {
            tblInvItemSale.getColumnModel().getColumn(0).setMinWidth(0);
            tblInvItemSale.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblInvItemSale.getColumnModel().getColumn(0).setMaxWidth(0);
            tblInvItemSale.getColumnModel().getColumn(1).setPreferredWidth(150);
            tblInvItemSale.getColumnModel().getColumn(2).setPreferredWidth(250);
            tblInvItemSale.getColumnModel().getColumn(4).setPreferredWidth(75);
            tblInvItemSale.getColumnModel().getColumn(5).setPreferredWidth(75);
            tblInvItemSale.getColumnModel().getColumn(6).setMinWidth(0);
            tblInvItemSale.getColumnModel().getColumn(6).setPreferredWidth(0);
            tblInvItemSale.getColumnModel().getColumn(6).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 740, 470));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 70, 300, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 90, 300, 10));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void tblInvItemSaleMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblInvItemSaleMouseReleased

    }//GEN-LAST:event_tblInvItemSaleMouseReleased

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {

            DefaultTableModel dtm_1 = (DefaultTableModel) tblInvSales.getModel();
            int selectedRow = tblInvSales.getSelectedRow();

            DefaultTableModel dtm_2 = (DefaultTableModel) tblInvItemSale.getModel();
            dtm_2.setRowCount(0);
            
            ResultSet rs = DB.search("SELECT inv.*,ii.*,i.*,b.*,c.* FROM invoice inv INNER JOIN invoiceitems ii ON inv.idinvoice = ii.idinvoice INNER JOIN items i ON i.iditems = ii.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE ii.idinvoice = '" + dtm_1.getValueAt(selectedRow, 0) + "' AND idcustomer = '" + dtm_1.getValueAt(selectedRow, 1) + "' AND  (itemsPartNo LIKE '%' '" + txtSearch.getText() + "' '%' OR brandName LIKE '%' '" + txtSearch.getText() + "' '%' OR categoryName LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsName LIKE '%' '" + txtSearch.getText() + "' '%') ");

            while (rs.next()) {

                Double itemPrice = (rs.getDouble("sellingPrice") * 1.0) / 100 * (100 - rs.getDouble("discount"));

                dtm_2.addRow(new Object[]{
                    rs.getString("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase(),
                    Decimal_Formats.Price((rs.getDouble("sellingPrice"))),
                    rs.getDouble("qty"),
                    rs.getDouble("discount") + " %",
                    Decimal_Formats.Price(itemPrice),
                    Decimal_Formats.Price((rs.getDouble("totalAmount")))
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewInvItemsale().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblInvItemSale;
    public static javax.swing.JTextField txtItems;
    private javax.swing.JTextField txtSearch;
    public static javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
    }

    private void DesignTable() {
        tblInvItemSale.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblInvItemSale.getTableHeader().setOpaque(false);
        tblInvItemSale.getTableHeader().setBackground(Color.white);
        tblInvItemSale.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer_1 = new DefaultTableCellRenderer();
        rightRenderer_1.setHorizontalAlignment(JLabel.CENTER);
        tblInvItemSale.getColumnModel().getColumn(3).setCellRenderer(rightRenderer_1);
        tblInvItemSale.getColumnModel().getColumn(4).setCellRenderer(rightRenderer_1);
        tblInvItemSale.getColumnModel().getColumn(5).setCellRenderer(rightRenderer_1);
        tblInvItemSale.getColumnModel().getColumn(7).setCellRenderer(rightRenderer_1);
    }

    private void loadItems() {
        try {

            DefaultTableModel dtm_1 = (DefaultTableModel) tblInvSales.getModel();
            int selectedRow = tblInvSales.getSelectedRow();

            DefaultTableModel dtm_2 = (DefaultTableModel) tblInvItemSale.getModel();

            ResultSet rs = DB.search("SELECT inv.*,ii.*,i.*,b.*,c.* FROM invoice inv INNER JOIN invoiceitems ii ON inv.idinvoice = ii.idinvoice INNER JOIN items i ON i.iditems = ii.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE ii.idinvoice = '" + dtm_1.getValueAt(selectedRow, 0) + "' AND idcustomer = '" + dtm_1.getValueAt(selectedRow, 1) + "' ");

            while (rs.next()) {

                Double itemPrice = (rs.getDouble("sellingPrice") * 1.0) / 100 * (100 - rs.getDouble("discount"));

                dtm_2.addRow(new Object[]{
                    rs.getString("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase(),
                    Decimal_Formats.Price((rs.getDouble("sellingPrice"))),
                    rs.getDouble("qty"),
                    rs.getDouble("discount") + " %",
                    Decimal_Formats.Price(itemPrice),
                    Decimal_Formats.Price((rs.getDouble("totalAmount")))
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void calculate() {
        try {
            DefaultTableModel dtm_1 = (DefaultTableModel) tblInvItemSale.getModel();
            Double total_1 = 0.0;

            for (int i = 0; i < dtm_1.getRowCount(); i++) {
                total_1 += Double.parseDouble(dtm_1.getValueAt(i, 7).toString());
            }

            txtTotal.setText("" + Decimal_Formats.Price(total_1));
            txtItems.setText(String.valueOf(dtm_1.getRowCount()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
