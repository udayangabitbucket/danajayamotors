/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExClasses;

import ExClasses.dateForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class DBBackup {

    public static void BACKUPDB() {

        String format = new SimpleDateFormat("yyyy-MM-DD").format(new Date());

        File dir = new File("C:/DBBackup/" + dateForm.currentdateFolder());

        if (!dir.exists()) {

            try {

                boolean mkdir = dir.mkdirs();

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

        String path = dir.toString() + "/" + dateForm.currentdateTime() + ".sql";
        String backup = "C:/Program Files/MySQL/MySQL Server 5.5/bin/mysqldump.exe -uroot -pNsyncnvidia -B danajayamotors -r" + path;
        try {
            Process exec = Runtime.getRuntime().exec(backup);
            int waitFor = exec.waitFor();
            if (waitFor == 0) {
                int TIME_VISIBLE = 3000;
                JOptionPane pane = new JOptionPane("Backup Completed..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
                JDialog dialog = pane.createDialog(null, "");
                dialog.setModal(false);
                dialog.setVisible(true);

                new Timer(TIME_VISIBLE, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dialog.setVisible(false);

                    }

                }).start();
            } else {

                JOptionPane.showMessageDialog(null, "Backup Error");

            }
        } catch (IOException ex) {

        } catch (InterruptedException ex) {

            ex.printStackTrace();
        }

    }

}
