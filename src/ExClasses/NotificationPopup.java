package ExClasses;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class NotificationPopup {

    public static void save() {
        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Successfull save..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void update() {
        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Successfull update..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();
    }

    public static void delete() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Successfull delete..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();
    }

    public static void fillFeilds() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Fill requre fields..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void already() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Already added..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void deleteadmin() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("You can't delete admin..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void deletecurrentuser() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("You can't delete login user..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void incorrectPayment() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Incorrect paid amount..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void noFindItem() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Can not find item..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

    public static void noFindQty() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Can't find available qty..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }
    
      public static void errorQty() {

        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Invalid qty..!", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

}
