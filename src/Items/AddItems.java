package Items;

import ExClasses.DB;
import ExClasses.NotificationPopup;
import static Items.Items.loadDetails;
import static Supplier.AddGrn.loadSearchTable;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class AddItems extends javax.swing.JFrame {

    boolean TXT_VALIDATE = false;
    boolean ITEM_VALIDATE = false;
    
    public static Map<String, Integer> cat = new HashMap();
    public static Map<String, Integer> bra = new HashMap();

    public AddItems() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        loadComboBrand();
        loadComboCat();
        //setComboCenter();
        txtPartNo.setFont(new Font("Times New Roman", Font.PLAIN, 15));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboItemBrand = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtPartNo = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        btnSave = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboItemCat = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        txtPartName = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        txtDescrip = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        btnAddBrand = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnAddCat = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" New item");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 26));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Brand : *");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 60, -1));

        comboItemBrand.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboItemBrand.setForeground(new java.awt.Color(0, 153, 153));
        comboItemBrand.setBorder(null);
        comboItemBrand.setNextFocusableComponent(comboItemCat);
        comboItemBrand.setOpaque(false);
        comboItemBrand.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboItemBrandPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboItemBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboItemBrandActionPerformed(evt);
            }
        });
        jPanel3.add(comboItemBrand, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 140, 200, 30));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Part No : *");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 150, 70, -1));

        txtPartNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPartNo.setBorder(null);
        txtPartNo.setNextFocusableComponent(txtPartName);
        txtPartNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartNoActionPerformed(evt);
            }
        });
        txtPartNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartNoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPartNoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPartNoKeyTyped(evt);
            }
        });
        jPanel3.add(txtPartNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 150, 230, 20));
        jPanel3.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 170, 230, 20));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("SAVE");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 360, 120, 40));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Category : *");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, 70, -1));

        comboItemCat.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboItemCat.setForeground(new java.awt.Color(0, 153, 153));
        comboItemCat.setBorder(null);
        comboItemCat.setNextFocusableComponent(txtPartNo);
        comboItemCat.setOpaque(false);
        comboItemCat.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboItemCatPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboItemCat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboItemCatActionPerformed(evt);
            }
        });
        jPanel3.add(comboItemCat, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 210, 200, 30));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Description :");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 80, -1));

        txtPartName.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPartName.setBorder(null);
        txtPartName.setNextFocusableComponent(txtDescrip);
        txtPartName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartNameActionPerformed(evt);
            }
        });
        txtPartName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartNameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPartNameKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPartNameKeyTyped(evt);
            }
        });
        jPanel3.add(txtPartName, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, 230, 20));
        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 240, 230, 20));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 153, 153));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Part Name :");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 220, 80, -1));

        txtDescrip.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtDescrip.setBorder(null);
        txtDescrip.setNextFocusableComponent(comboItemBrand);
        txtDescrip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripActionPerformed(evt);
            }
        });
        txtDescrip.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDescripKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescripKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDescripKeyTyped(evt);
            }
        });
        jPanel3.add(txtDescrip, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 290, 570, 20));
        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, 570, 20));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddBrand.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddBrand.setForeground(new java.awt.Color(255, 255, 255));
        btnAddBrand.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddBrand.setText("+");
        btnAddBrand.setToolTipText("Add item");
        btnAddBrand.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddBrand.setPreferredSize(new java.awt.Dimension(14, 50));
        btnAddBrand.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddBrandMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddBrandMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddBrandMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddBrand, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAddBrand, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 140, 30, 30));

        jPanel4.setBackground(new java.awt.Color(0, 153, 51));

        btnAddCat.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddCat.setForeground(new java.awt.Color(255, 255, 255));
        btnAddCat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddCat.setText("+");
        btnAddCat.setToolTipText("Add item");
        btnAddCat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddCat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddCatMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddCatMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddCatMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCat, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddCat, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 210, 30, 30));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void comboItemBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboItemBrandActionPerformed

    }//GEN-LAST:event_comboItemBrandActionPerformed

    private void txtPartNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNoActionPerformed

    private void txtPartNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyPressed

    }//GEN-LAST:event_txtPartNoKeyPressed

    private void txtPartNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveItem();
        }
    }//GEN-LAST:event_txtPartNoKeyReleased

    private void txtPartNoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyTyped
        char keyChar = evt.getKeyChar();
        if (Character.isLowerCase(keyChar)) {
            evt.setKeyChar(Character.toUpperCase(keyChar));
        }
    }//GEN-LAST:event_txtPartNoKeyTyped

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        saveItem();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void comboItemCatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboItemCatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboItemCatActionPerformed

    private void txtPartNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameActionPerformed

    private void txtPartNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameKeyPressed

    private void txtPartNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveItem();
        }
    }//GEN-LAST:event_txtPartNameKeyReleased

    private void txtPartNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameKeyTyped

    private void txtDescripActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripActionPerformed

    private void txtDescripKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripKeyPressed

    private void txtDescripKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveItem();
        }
    }//GEN-LAST:event_txtDescripKeyReleased

    private void txtDescripKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripKeyTyped

    private void comboItemBrandPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboItemBrandPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboItemBrandPopupMenuWillBecomeInvisible

    private void comboItemCatPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboItemCatPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboItemCatPopupMenuWillBecomeInvisible

    private void btnAddBrandMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddBrandMouseClicked
        //if (UserPrivilage.equals("admin")) {
            new Brand().setVisible(true);
            //}
    }//GEN-LAST:event_btnAddBrandMouseClicked

    private void btnAddBrandMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddBrandMouseEntered
        btnAddBrand.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddBrandMouseEntered

    private void btnAddBrandMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddBrandMouseExited
        btnAddBrand.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddBrandMouseExited

    private void btnAddCatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCatMouseClicked
        //if (UserPrivilage.equals("admin")) {
            new Category().setVisible(true);
            //}
    }//GEN-LAST:event_btnAddCatMouseClicked

    private void btnAddCatMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCatMouseEntered
        btnAddCat.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddCatMouseEntered

    private void btnAddCatMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddCatMouseExited
        btnAddCat.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddCatMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddItems().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddBrand;
    private javax.swing.JLabel btnAddCat;
    private javax.swing.JLabel btnSave;
    public static javax.swing.JComboBox<String> comboItemBrand;
    public static javax.swing.JComboBox<String> comboItemCat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lbClose;
    private javax.swing.JTextField txtDescrip;
    private javax.swing.JTextField txtPartName;
    private javax.swing.JTextField txtPartNo;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        comboItemBrand.setOpaque(true);
        comboItemCat.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        comboItemBrand.setBackground(Color.white);
        comboItemCat.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    public static void loadComboBrand() {
        try {
            comboItemBrand.removeAllItems();
            
            ResultSet rs = DB.search("SELECT * FROM brands WHERE isActive = '1'");

            comboItemBrand.addItem("");
            //comboItemBrand.addItem("ADD NEW...");

            while (rs.next()) {
                //comboItemBrand.addItem(rs.getString("idbrands") + " - " + rs.getString("brandName"));
                comboItemBrand.addItem(rs.getString("brandName"));
                bra.put(rs.getString("brandName"), rs.getInt("idbrands"));
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadComboCat() {
        try {

            comboItemCat.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM category WHERE isActive = '1'");

            comboItemCat.addItem("");
            //comboItemCat.addItem("ADD NEW...");

            while (rs.next()) {
                //comboItemCat.addItem(rs1.getString("idcategory") + " - " + rs1.getString("categoryName"));
                comboItemCat.addItem(rs.getString("categoryName"));
                cat.put(rs.getString("categoryName"), rs.getInt("idcategory"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setComboCenter() {

        ((JLabel) comboItemBrand.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        ((JLabel) comboItemCat.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

    }

    private void validateItem() {

        try {

            ResultSet rs = DB.search("SELECT * FROM items WHERE itemsPartNo = '" + txtPartNo.getText() + "' AND isActive = '1' ");

            if (rs.next()) {
                ITEM_VALIDATE = true;
            } else {
                ITEM_VALIDATE = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveItem() {
        try {

            validateTXT();
            if (TXT_VALIDATE == false) {
                validateItem();
                if (ITEM_VALIDATE == false) {

                    Integer brandId = null;
                    Integer catId = null;

                    //String getBrandId = (String) comboItemBrand.getSelectedItem();
                    //String[] setBrandId = getBrandId.split(" - ");
                    brandId = bra.get(comboItemBrand.getSelectedItem());

                    //String getCatId = (String) comboItemCat.getSelectedItem();
                    //String[] setCatId = getCatId.split(" - ");
                    catId = cat.get(comboItemCat.getSelectedItem());

                    String insertItem = "INSERT INTO items(itemsPartNo, itemsName, description, idbrands, idcategory)"
                            + "VALUES ('" + txtPartNo.getText().toUpperCase() + "', '" + txtPartName.getText().toUpperCase() + "', '" + txtDescrip.getText().toLowerCase() + "', '" + brandId + "', '" + catId + "')";

                    DB.idu(insertItem);

                    Integer itemId = null;

                    ResultSet rs = DB.search("SELECT * FROM items ORDER BY iditems DESC ");

                    if (rs.next()) {
                        itemId = rs.getInt("iditems");
                    }

                    String insertStock = "INSERT INTO stock (qty, lowstock, iditems)"
                            + "VALUES ('" + 0 + "', '" + 0 + "', '" + itemId + "' )";

                    DB.idu(insertStock);
                    
                    String insertPrice = "INSERT INTO itemprice (iditems)"
                            + "VALUES ('" + itemId + "' )";

                    DB.idu(insertPrice);

                    loadDetails();
                    loadSearchTable();
                    ClearTXT();
                    NotificationPopup.save();

                } else {
                    NotificationPopup.already();
                }

            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateTXT() {

        if (comboItemBrand.getSelectedIndex() == 0 || comboItemCat.getSelectedIndex() == 0) {
            TXT_VALIDATE = true;
        } else if (txtPartNo.getText().isEmpty()) {
            TXT_VALIDATE = true;
        }

    }

    private void ClearTXT() {

        Component[] cp = jPanel3.getComponents();
        for (Component c : cp) {

            if (c instanceof JTextField) {

                ((JTextField) c).setText(null);

            }

        }

        txtPartNo.grabFocus();

    }

}
