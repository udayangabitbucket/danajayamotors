package Items;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import ExClasses.dateForm;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class AdminPriceList extends javax.swing.JFrame {

    public static Map<String, Integer> cat = new HashMap();
    public static Map<String, Integer> bra = new HashMap();

    public AdminPriceList() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadBrands();
        loadCategory();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboBrand = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        comboCategory = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPrice = new javax.swing.JTable();
        btnPrice = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        txtSearch = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Price List");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Brand : *");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 50, 70, -1));

        comboBrand.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboBrand.setForeground(new java.awt.Color(0, 153, 153));
        comboBrand.setBorder(null);
        comboBrand.setOpaque(false);
        comboBrand.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboBrandPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBrandActionPerformed(evt);
            }
        });
        jPanel3.add(comboBrand, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 70, 180, 25));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Category : *");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 50, 70, -1));

        comboCategory.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboCategory.setForeground(new java.awt.Color(0, 153, 153));
        comboCategory.setBorder(null);
        comboCategory.setOpaque(false);
        comboCategory.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboCategoryPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategoryActionPerformed(evt);
            }
        });
        jPanel3.add(comboCategory, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 70, 180, 25));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblPrice.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblPrice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM ID", "PART NO", "DESCRIPTION", "BILL PRICE", "SELES CUSTOMER", "PERMENENT CUSTOMER", "QTY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPrice.setColumnSelectionAllowed(true);
        tblPrice.setFocusable(false);
        tblPrice.setRequestFocusEnabled(false);
        tblPrice.setRowHeight(30);
        tblPrice.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblPrice.setShowVerticalLines(false);
        tblPrice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPriceMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblPriceMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblPrice);
        tblPrice.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tblPrice.getColumnModel().getColumnCount() > 0) {
            tblPrice.getColumnModel().getColumn(0).setMinWidth(0);
            tblPrice.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblPrice.getColumnModel().getColumn(0).setMaxWidth(0);
            tblPrice.getColumnModel().getColumn(2).setPreferredWidth(150);
            tblPrice.getColumnModel().getColumn(6).setPreferredWidth(40);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 700, 390));

        btnPrice.setBackground(new java.awt.Color(255, 255, 255));
        btnPrice.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnPrice.setForeground(new java.awt.Color(0, 153, 153));
        btnPrice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPrice.setText("Price Variation");
        btnPrice.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnPrice.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnPrice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPriceMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPriceMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPriceMouseExited(evt);
            }
        });
        jPanel3.add(btnPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 540, 110, 25));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 20, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 100, 260, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 260, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void comboBrandPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboBrandPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboBrandPopupMenuWillBecomeInvisible

    private void comboBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBrandActionPerformed
        try {

            Integer brandId = null;
            Integer catId = null;

            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
            dtm.setRowCount(0);

            brandId = bra.get(comboBrand.getSelectedItem());
            catId = cat.get(comboCategory.getSelectedItem());

            if (comboBrand.getSelectedIndex() != 0) {
                if (comboCategory.getSelectedIndex() != 0) {

                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' AND i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                }
            } else {
                if (comboCategory.getSelectedIndex() != 0) {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    loadDetails();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboBrandActionPerformed

    private void comboCategoryPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboCategoryPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboCategoryPopupMenuWillBecomeInvisible

    private void comboCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategoryActionPerformed
        try {

            Integer brandId = null;
            Integer catId = null;

            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
            dtm.setRowCount(0);

            brandId = bra.get(comboBrand.getSelectedItem());
            catId = cat.get(comboCategory.getSelectedItem());

            if (comboCategory.getSelectedIndex() != 0) {
                if (comboBrand.getSelectedIndex() != 0) {

                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' AND i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idcategory = '" + catId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                }
            } else {
                if (comboBrand.getSelectedIndex() != 0) {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    loadDetails();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboCategoryActionPerformed

    private void tblPriceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPriceMouseReleased

    }//GEN-LAST:event_tblPriceMouseReleased

    private void tblPriceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPriceMouseClicked
//        try {
//            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
//            DefaultTableModel dtmSale = (DefaultTableModel) tblSelling.getModel();
//            int selectedRow = tblPrice.getSelectedRow();
//            dtmSale.setRowCount(0);
//
//            for (int i = 0; i < dtm.getRowCount(); i++) {
//
//                Boolean checked = (Boolean) dtm.getValueAt(i, 4);
//                if (checked != null && checked) {
//                    ResultSet rs = DB.search("SELECT * FROM itemprice WHERE iditemPrice = '" + dtm.getValueAt(i, 0) + "' ");
//
//                    if (rs.next()) {
//                        dtmSale.addRow(new Object[]{
//                            rs.getString("iditemPrice"),
//                            dtm.getValueAt(i, 1),
//                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
//                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
//                            Decimal_Formats.Price(rs.getDouble("billingPrice"))
//                        });
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            //e.printStackTrace();
//        }
    }//GEN-LAST:event_tblPriceMouseClicked

    private void btnPriceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceMouseClicked
        new PriceVariation().setVisible(true);
    }//GEN-LAST:event_btnPriceMouseClicked

    private void btnPriceMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceMouseEntered
        btnPrice.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnPriceMouseEntered

    private void btnPriceMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceMouseExited
        btnPrice.setBackground(Color.white);
    }//GEN-LAST:event_btnPriceMouseExited

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE  (brandName LIKE '%' '" + txtSearch.getText() + "' '%' OR categoryName LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsPartNo LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsName LIKE '%' '" + txtSearch.getText() + "' '%') ORDER BY brandName ASC");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                    Decimal_Formats.Price(rs.getDouble("billingPrice")),
                    Decimal_Formats.Price(rs.getDouble("salesPrice")),
                    Decimal_Formats.Price(rs.getDouble("otherPrice")),
                    rs.getInt("qty")
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminPriceList().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnPrice;
    public static javax.swing.JComboBox<String> comboBrand;
    public static javax.swing.JComboBox<String> comboCategory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblPrice;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnPrice.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        comboBrand.setBackground(Color.white);
        comboCategory.setBackground(Color.white);
    }

    private void DesignTable() {
        tblPrice.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 12));
        tblPrice.getTableHeader().setOpaque(false);
        tblPrice.getTableHeader().setBackground(Color.white);
        tblPrice.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer_1 = new DefaultTableCellRenderer();
        rightRenderer_1.setHorizontalAlignment(JLabel.CENTER);
        tblPrice.getColumnModel().getColumn(3).setCellRenderer(rightRenderer_1);
        tblPrice.getColumnModel().getColumn(4).setCellRenderer(rightRenderer_1);
        tblPrice.getColumnModel().getColumn(5).setCellRenderer(rightRenderer_1);
        tblPrice.getColumnModel().getColumn(6).setCellRenderer(rightRenderer_1);

    }

    private void loadBrands() {
        try {
            ResultSet rs = DB.search("SELECT * FROM brands");
            comboBrand.removeAllItems();
            comboBrand.addItem("");
            while (rs.next()) {
                comboBrand.addItem(rs.getString("brandName"));
                bra.put(rs.getString("brandName"), rs.getInt("idbrands"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCategory() {
        try {
            ResultSet rs = DB.search("SELECT * FROM category");
            comboCategory.removeAllItems();
            comboCategory.addItem("");
            while (rs.next()) {
                comboCategory.addItem(rs.getString("categoryName"));
                cat.put(rs.getString("categoryName"), rs.getInt("idcategory"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDetails() {

        try {

            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems ORDER BY brandName ASC");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                    Decimal_Formats.Price(rs.getDouble("billingPrice")),
                    Decimal_Formats.Price(rs.getDouble("salesPrice")),
                    Decimal_Formats.Price(rs.getDouble("otherPrice")),
                    rs.getInt("qty")
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void clearTXT() {

        try {

            Integer brandId = null;
            Integer catId = null;

            DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
            dtm.setRowCount(0);

            brandId = bra.get(comboBrand.getSelectedItem());
            catId = cat.get(comboCategory.getSelectedItem());

            if (comboBrand.getSelectedIndex() != 0) {
                if (comboCategory.getSelectedIndex() != 0) {

                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' AND i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idbrands = '" + brandId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                }
            } else {
                if (comboCategory.getSelectedIndex() != 0) {
                    ResultSet rs = DB.search("SELECT i.*, ip.*, b.*, c.*, s.* FROM items i INNER JOIN itemprice ip ON i.iditems = ip.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory INNER JOIN stock s ON i.iditems = s.iditems WHERE i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo"),
                            rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase(),
                            Decimal_Formats.Price(rs.getDouble("billingPrice")),
                            Decimal_Formats.Price(rs.getDouble("salesPrice")),
                            Decimal_Formats.Price(rs.getDouble("otherPrice")),
                            rs.getInt("qty")
                        });
                    }
                } else {
                    loadDetails();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
