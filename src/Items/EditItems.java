package Items;

import ExClasses.DB;
import ExClasses.NotificationPopup;
import static Items.Items.loadDetails;
import static Items.Items.tblItems;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class EditItems extends javax.swing.JFrame {

    boolean TXT_VALIDATE = false;
    boolean ITEM_VALIDATE = false;

    public static Map<String, Integer> cat = new HashMap();
    public static Map<String, Integer> bra = new HashMap();

    public EditItems() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        loadComboBrand();
        loadComboCat();
        getDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboItemBrand = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtPartNo = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        btnSave = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboItemCat = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        txtPartName = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        txtDescrip = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Change item");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Brand :");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 40, -1));

        comboItemBrand.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        comboItemBrand.setForeground(new java.awt.Color(0, 153, 153));
        comboItemBrand.setBorder(null);
        comboItemBrand.setNextFocusableComponent(comboItemCat);
        comboItemBrand.setOpaque(false);
        comboItemBrand.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                comboItemBrandPopupMenuWillBecomeVisible(evt);
            }
        });
        comboItemBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboItemBrandActionPerformed(evt);
            }
        });
        jPanel3.add(comboItemBrand, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 220, 30));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Part No :");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 150, 60, -1));

        txtPartNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPartNo.setBorder(null);
        txtPartNo.setNextFocusableComponent(txtPartName);
        txtPartNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartNoActionPerformed(evt);
            }
        });
        txtPartNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartNoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPartNoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPartNoKeyTyped(evt);
            }
        });
        jPanel3.add(txtPartNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 150, 230, 20));
        jPanel3.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 170, 230, 20));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("UPDATE");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 360, 120, 40));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Category :");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 70, -1));

        comboItemCat.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        comboItemCat.setForeground(new java.awt.Color(0, 153, 153));
        comboItemCat.setBorder(null);
        comboItemCat.setNextFocusableComponent(txtPartNo);
        comboItemCat.setOpaque(false);
        comboItemCat.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboItemCatPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                comboItemCatPopupMenuWillBecomeVisible(evt);
            }
        });
        comboItemCat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboItemCatActionPerformed(evt);
            }
        });
        jPanel3.add(comboItemCat, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 210, 220, 30));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Description :");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 80, -1));

        txtPartName.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPartName.setBorder(null);
        txtPartName.setNextFocusableComponent(txtDescrip);
        txtPartName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPartNameActionPerformed(evt);
            }
        });
        txtPartName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPartNameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPartNameKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPartNameKeyTyped(evt);
            }
        });
        jPanel3.add(txtPartName, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 220, 230, 20));
        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 240, 230, 20));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 153, 153));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Part Name :");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, 80, -1));

        txtDescrip.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtDescrip.setBorder(null);
        txtDescrip.setNextFocusableComponent(comboItemBrand);
        txtDescrip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripActionPerformed(evt);
            }
        });
        txtDescrip.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDescripKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescripKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDescripKeyTyped(evt);
            }
        });
        jPanel3.add(txtDescrip, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 290, 570, 20));
        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 310, 570, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void comboItemBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboItemBrandActionPerformed

    }//GEN-LAST:event_comboItemBrandActionPerformed

    private void txtPartNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNoActionPerformed

    private void txtPartNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyPressed

    }//GEN-LAST:event_txtPartNoKeyPressed

    private void txtPartNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateItem();
        }
    }//GEN-LAST:event_txtPartNoKeyReleased

    private void txtPartNoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNoKeyTyped
        char keyChar = evt.getKeyChar();
        if (Character.isLowerCase(keyChar)) {
            evt.setKeyChar(Character.toUpperCase(keyChar));
        }
    }//GEN-LAST:event_txtPartNoKeyTyped

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        updateItem();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void comboItemCatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboItemCatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboItemCatActionPerformed

    private void txtPartNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPartNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameActionPerformed

    private void txtPartNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameKeyPressed

    private void txtPartNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateItem();
        }
    }//GEN-LAST:event_txtPartNameKeyReleased

    private void txtPartNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPartNameKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPartNameKeyTyped

    private void txtDescripActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripActionPerformed

    private void txtDescripKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripKeyPressed

    private void txtDescripKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            updateItem();
        }
    }//GEN-LAST:event_txtDescripKeyReleased

    private void txtDescripKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripKeyTyped

    private void comboItemCatPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboItemCatPopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboItemCatPopupMenuWillBecomeInvisible

    private void comboItemBrandPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboItemBrandPopupMenuWillBecomeVisible

    }//GEN-LAST:event_comboItemBrandPopupMenuWillBecomeVisible

    private void comboItemCatPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboItemCatPopupMenuWillBecomeVisible

    }//GEN-LAST:event_comboItemCatPopupMenuWillBecomeVisible

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditItems().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    public static javax.swing.JComboBox<String> comboItemBrand;
    public static javax.swing.JComboBox<String> comboItemCat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTextField txtDescrip;
    public static javax.swing.JTextField txtPartName;
    public static javax.swing.JTextField txtPartNo;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    public static void loadComboBrand() {
        try {

            comboItemBrand.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM brands WHERE isActive = '1'");

            comboItemBrand.addItem("");

            while (rs.next()) {
                //comboItemBrand.addItem(rs.getString("idbrands") + " - " + rs.getString("brandName"));
                comboItemBrand.addItem(rs.getString("brandName"));
                bra.put(rs.getString("brandName"), rs.getInt("idbrands"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void loadComboCat() {
        try {

            comboItemCat.removeAllItems();

            ResultSet rs = DB.search("SELECT * FROM category WHERE isActive = '1'");

            comboItemCat.addItem("");

            while (rs.next()) {
                //comboItemCat.addItem(rs.getString("idcategory") + " - " + rs.getString("categoryName"));
                comboItemCat.addItem(rs.getString("categoryName"));
                cat.put(rs.getString("categoryName"), rs.getInt("idcategory"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getDetails() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblItems.getModel();
            int selectedRow = tblItems.getSelectedRow();

            ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE i.iditems = '" + dtm.getValueAt(selectedRow, 0) + "' ");

            if (rs.next()) {

                comboItemBrand.setSelectedItem(rs.getString("brandName"));
                comboItemCat.setSelectedItem(rs.getString("categoryName"));
                txtPartNo.setText(rs.getString("itemsPartNo"));
                txtPartName.setText(rs.getString("itemsName"));
                txtDescrip.setText(rs.getString("description"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateItem() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) Items.tblItems.getModel();
            int selectedRow = Items.tblItems.getSelectedRow();

            validateTXT();
            if (TXT_VALIDATE == false) {
                validateItem();
                if (ITEM_VALIDATE == false) {

                    Integer brandId = null;
                    Integer catId = null;

                    //String getBrandId = (String) comboItemBrand.getSelectedItem();
                    //String[] setBrandId = getBrandId.split(" - ");
                    brandId = bra.get(comboItemBrand.getSelectedItem());

                    //String getCatId = (String) comboItemCat.getSelectedItem();
                    //String[] setCatId = getCatId.split(" - ");
                    catId = cat.get(comboItemCat.getSelectedItem());

                    String updateItems = "UPDATE items SET itemsPartNo = '" + txtPartNo.getText().toUpperCase() + "', itemsName = '" + txtPartName.getText().toUpperCase() + "', description = '" + txtDescrip.getText().toLowerCase() + "', idbrands = '" + brandId + "', idcategory = '" + catId + "' WHERE iditems = '" + dtm.getValueAt(selectedRow, 0) + "' ";
                    DB.idu(updateItems);
                    loadDetails();
                    this.dispose();
                    NotificationPopup.update();

                } else {
                    NotificationPopup.already();
                }

            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateItem() {

        try {

            DefaultTableModel dtm = (DefaultTableModel) Items.tblItems.getModel();
            int selectedRow = Items.tblItems.getSelectedRow();

            ResultSet rs = DB.search("SELECT * FROM items WHERE itemsPartNo = '" + txtPartNo.getText().toUpperCase() + "' AND NOT iditems = '" + dtm.getValueAt(selectedRow, 0) + "' AND isActive = '1' ");

            if (rs.next()) {
                ITEM_VALIDATE = true;
            } else {
                ITEM_VALIDATE = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void validateTXT() {

        if (comboItemBrand.getSelectedIndex() == 0 || comboItemCat.getSelectedIndex() == 0) {
            TXT_VALIDATE = true;
        } else if (txtPartNo.getText().isEmpty()) {
            TXT_VALIDATE = true;
        }

    }

}
