package Items;

import Customer.*;
import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import static Items.Items.tblItems;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class ItemHistory extends javax.swing.JFrame {

    public ItemHistory() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        tableASC();
        getDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblItemHistory = new javax.swing.JTable();
        txtSearch = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        dateChooserFrom = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        dateChooserTo = new com.toedter.calendar.JDateChooser();
        dateSearch = new javax.swing.JLabel();
        lbRefesh = new javax.swing.JLabel();
        lbItem = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Item History");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblItemHistory.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblItemHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DOCUMENT NO", "STATUS", "DATE", "QTY", "UNIT PRICE", "DISCOUNT", "TOTAL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblItemHistory.setFocusable(false);
        tblItemHistory.setRowHeight(30);
        tblItemHistory.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblItemHistory.setShowVerticalLines(false);
        tblItemHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblItemHistoryMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tblItemHistory);
        if (tblItemHistory.getColumnModel().getColumnCount() > 0) {
            tblItemHistory.getColumnModel().getColumn(3).setPreferredWidth(75);
            tblItemHistory.getColumnModel().getColumn(5).setPreferredWidth(75);
        }

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, 720, 540));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 250, 20));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 20, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 250, 10));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("From :");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 80, 40, -1));

        dateChooserFrom.setDateFormatString("yyyy-MM-dd");
        dateChooserFrom.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserFrom, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 80, 110, -1));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("To :");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 80, 30, -1));

        dateChooserTo.setDateFormatString("yyyy-MM-dd");
        dateChooserTo.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserTo, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 80, 110, -1));

        dateSearch.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        dateSearch.setForeground(new java.awt.Color(0, 153, 153));
        dateSearch.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        dateSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dateSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateSearchMouseClicked(evt);
            }
        });
        jPanel3.add(dateSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 80, 40, 20));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel3.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 80, 30, 20));

        lbItem.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbItem.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbItem.setText("SET ITEM HERE");
        jPanel3.add(lbItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 550, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 730));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblItemHistoryMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItemHistoryMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tblItemHistoryMouseReleased

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {

            dateChooserFrom.setDate(null);
            dateChooserTo.setDate(null);

            DefaultTableModel dtm_1 = (DefaultTableModel) tblItems.getModel();
            int selectedRow_1 = tblItems.getSelectedRow();

            lbItem.setText(dtm_1.getValueAt(selectedRow_1, 1).toString() + " " + dtm_1.getValueAt(selectedRow_1, 2).toString() + " " + dtm_1.getValueAt(selectedRow_1, 4).toString() + " (" + dtm_1.getValueAt(selectedRow_1, 3).toString() + ")");

            DefaultTableModel dtm_2 = (DefaultTableModel) tblItemHistory.getModel();
            dtm_2.setRowCount(0);

            ResultSet rs_1 = DB.search("SELECT i.*, ii.* FROM invoice i INNER JOIN invoiceitems ii ON i.idinvoice = ii.idinvoice WHERE ii.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND (invoiceNo LIKE '%' '" + txtSearch.getText() + "' '%' OR status LIKE '%' '" + txtSearch.getText() + "' '%') ");

            while (rs_1.next()) {

                dtm_2.addRow(new Object[]{
                    "INV-" + rs_1.getString("invoiceNo"),
                    rs_1.getString("status"),
                    rs_1.getDate("invoiceDate"),
                    "- " + rs_1.getDouble("qty"),
                    Decimal_Formats.Price((rs_1.getDouble("sellingPrice"))),
                    Decimal_Formats.Price((rs_1.getDouble("discount"))) + "%",
                    Decimal_Formats.Price((rs_1.getDouble("totalAmount")))
                });
            }

            ResultSet rs_2 = DB.search("SELECT i.*, ri.* FROM invoice i INNER JOIN returninvoice ri ON i.idinvoice = ri.idinvoice WHERE ri.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND (invoiceNo LIKE '%' '" + txtSearch.getText() + "' '%' OR returnstatus LIKE '%' '" + txtSearch.getText() + "' '%') ");

            while (rs_2.next()) {

                dtm_2.addRow(new Object[]{
                    "INV-" + rs_2.getString("invoiceNo"),
                    rs_2.getString("returnstatus"),
                    rs_2.getDate("returnDate"),
                    "+ " + rs_2.getDouble("qty"),
                    Decimal_Formats.Price((rs_2.getDouble("returnPrice"))),
                    ".00%",
                    Decimal_Formats.Price((rs_2.getDouble("returnTotal")))
                });
            }

            ResultSet rs_3 = DB.search("SELECT g.*, gi.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn WHERE gi.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND (billNo LIKE '%' '" + txtSearch.getText() + "' '%' OR status LIKE '%' '" + txtSearch.getText() + "' '%') ");

            while (rs_3.next()) {

                dtm_2.addRow(new Object[]{
                    "GR-" + rs_3.getString("billNo"),
                    rs_3.getString("status"),
                    rs_3.getDate("date"),
                    "+ " + rs_3.getDouble("qty"),
                    Decimal_Formats.Price((rs_3.getDouble("unitPrice"))),
                    Decimal_Formats.Price((rs_3.getDouble("discount"))) + "%",
                    Decimal_Formats.Price((rs_3.getDouble("totalAmount")))
                });
            }

            ResultSet rs_4 = DB.search("SELECT g.*, rg.* FROM grn g INNER JOIN returngrn rg ON g.idGrn = rg.idGrn WHERE rg.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND (billNo LIKE '%' '" + txtSearch.getText() + "' '%' OR returnstatus LIKE '%' '" + txtSearch.getText() + "' '%') ");

            while (rs_4.next()) {

                dtm_2.addRow(new Object[]{
                    "GR-" + rs_4.getString("billNo"),
                    rs_4.getString("returnstatus"),
                    rs_4.getDate("returnDate"),
                    "- " + rs_4.getDouble("qty"),
                    Decimal_Formats.Price((rs_4.getDouble("returnPrice"))),
                    ".00%",
                    Decimal_Formats.Price((rs_4.getDouble("returnTotal")))
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void dateSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateSearchMouseClicked
        datesearch();
    }//GEN-LAST:event_dateSearchMouseClicked

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        txtSearch.setText(null);
        dateChooserFrom.setDate(null);
        dateChooserTo.setDate(null);
        DefaultTableModel dtm = (DefaultTableModel) tblItemHistory.getModel();
        dtm.setRowCount(0);
        getDetails();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tblItemHistory.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_lbRefeshMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ItemHistory().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser dateChooserFrom;
    private com.toedter.calendar.JDateChooser dateChooserTo;
    private javax.swing.JLabel dateSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbItem;
    private javax.swing.JLabel lbRefesh;
    public static javax.swing.JTable tblItemHistory;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
    }

    private void DesignTable() {

        tblItemHistory.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 12));
        tblItemHistory.getTableHeader().setOpaque(false);
        tblItemHistory.getTableHeader().setBackground(Color.white);
        tblItemHistory.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblItemHistory.getColumnModel().getColumn(0).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        tblItemHistory.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
    }

    private void tableASC() {

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tblItemHistory.getModel());
        tblItemHistory.setRowSorter(sorter);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();

    }

    private void getDetails() {
        try {

            DefaultTableModel dtm_1 = (DefaultTableModel) tblItems.getModel();
            int selectedRow_1 = tblItems.getSelectedRow();

            lbItem.setText(dtm_1.getValueAt(selectedRow_1, 1).toString() + " " + dtm_1.getValueAt(selectedRow_1, 2).toString() + " " + dtm_1.getValueAt(selectedRow_1, 4).toString() + " (" + dtm_1.getValueAt(selectedRow_1, 3).toString() + ")");

            DefaultTableModel dtm_2 = (DefaultTableModel) tblItemHistory.getModel();

            ResultSet rs_1 = DB.search("SELECT i.*, ii.* FROM invoice i INNER JOIN invoiceitems ii ON i.idinvoice = ii.idinvoice WHERE ii.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' ");

            while (rs_1.next()) {

                dtm_2.addRow(new Object[]{
                    "INV-" + rs_1.getString("invoiceNo"),
                    rs_1.getString("status"),
                    rs_1.getDate("invoiceDate"),
                    "- " + rs_1.getDouble("qty"),
                    Decimal_Formats.Price((rs_1.getDouble("sellingPrice"))),
                    Decimal_Formats.Price((rs_1.getDouble("discount"))) + "%",
                    Decimal_Formats.Price((rs_1.getDouble("totalAmount")))
                });
            }

            ResultSet rs_2 = DB.search("SELECT i.*, ri.* FROM invoice i INNER JOIN returninvoice ri ON i.idinvoice = ri.idinvoice WHERE ri.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' ");

            while (rs_2.next()) {

                dtm_2.addRow(new Object[]{
                    "INV-" + rs_2.getString("invoiceNo"),
                    rs_2.getString("returnstatus"),
                    rs_2.getDate("returnDate"),
                    "+ " + rs_2.getDouble("qty"),
                    Decimal_Formats.Price((rs_2.getDouble("returnPrice"))),
                    ".00%",
                    Decimal_Formats.Price((rs_2.getDouble("returnTotal")))
                });
            }

            ResultSet rs_3 = DB.search("SELECT g.*, gi.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn WHERE gi.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' ");

            while (rs_3.next()) {

                dtm_2.addRow(new Object[]{
                    "GR-" + rs_3.getString("billNo"),
                    rs_3.getString("status"),
                    rs_3.getDate("date"),
                    "+ " + rs_3.getDouble("qty"),
                    Decimal_Formats.Price((rs_3.getDouble("unitPrice"))),
                    Decimal_Formats.Price((rs_3.getDouble("discount"))) + "%",
                    Decimal_Formats.Price((rs_3.getDouble("totalAmount")))
                });
            }

            ResultSet rs_4 = DB.search("SELECT g.*, rg.* FROM grn g INNER JOIN returngrn rg ON g.idGrn = rg.idGrn WHERE rg.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' ");

            while (rs_4.next()) {

                dtm_2.addRow(new Object[]{
                    "GR-" + rs_4.getString("billNo"),
                    rs_4.getString("returnstatus"),
                    rs_4.getDate("returnDate"),
                    "- " + rs_4.getDouble("qty"),
                    Decimal_Formats.Price((rs_4.getDouble("returnPrice"))),
                    ".00%",
                    Decimal_Formats.Price((rs_4.getDouble("returnTotal")))
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void datesearch() {
        try {
            txtSearch.setText(null);

            if (dateChooserFrom.getDate() != null || dateChooserTo.getDate() != null) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DefaultTableModel dtm_1 = (DefaultTableModel) tblItems.getModel();
                int selectedRow_1 = tblItems.getSelectedRow();

                DefaultTableModel dtm_2 = (DefaultTableModel) tblItemHistory.getModel();
                dtm_2.setRowCount(0);

                ResultSet rs_1 = null;
                ResultSet rs_2 = null;
                ResultSet rs_3 = null;
                ResultSet rs_4 = null;

                if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() != null) {

                    rs_1 = DB.search("SELECT i.*, ii.* FROM invoice i INNER JOIN invoiceitems ii ON i.idinvoice = ii.idinvoice WHERE ii.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND i.invoiceDate BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_2 = DB.search("SELECT i.*, ri.* FROM invoice i INNER JOIN returninvoice ri ON i.idinvoice = ri.idinvoice WHERE ri.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND ri.returnDate BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_3 = DB.search("SELECT g.*, gi.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn WHERE gi.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND g.date BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_4 = DB.search("SELECT g.*, rg.* FROM grn g INNER JOIN returngrn rg ON g.idGrn = rg.idGrn WHERE rg.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND rg.returnDate BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' ");

                } else if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() == null) {

                    rs_1 = DB.search("SELECT i.*, ii.* FROM invoice i INNER JOIN invoiceitems ii ON i.idinvoice = ii.idinvoice WHERE ii.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND i.invoiceDate = '" + df.format(dateChooserFrom.getDate()) + "' ");

                    rs_2 = DB.search("SELECT i.*, ri.* FROM invoice i INNER JOIN returninvoice ri ON i.idinvoice = ri.idinvoice WHERE ri.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND ri.returnDate = '" + df.format(dateChooserFrom.getDate()) + "' ");

                    rs_3 = DB.search("SELECT g.*, gi.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn WHERE gi.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND g.date = '" + df.format(dateChooserFrom.getDate()) + "' ");

                    rs_4 = DB.search("SELECT g.*, rg.* FROM grn g INNER JOIN returngrn rg ON g.idGrn = rg.idGrn WHERE rg.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND rg.returnDate = '" + df.format(dateChooserFrom.getDate()) + "' ");

                } else if (dateChooserTo.getDate() != null && dateChooserFrom.getDate() == null) {

                    rs_1 = DB.search("SELECT i.*, ii.* FROM invoice i INNER JOIN invoiceitems ii ON i.idinvoice = ii.idinvoice WHERE ii.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND i.invoiceDate = '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_2 = DB.search("SELECT i.*, ri.* FROM invoice i INNER JOIN returninvoice ri ON i.idinvoice = ri.idinvoice WHERE ri.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND ri.returnDate = '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_3 = DB.search("SELECT g.*, gi.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn WHERE gi.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND g.date = '" + df.format(dateChooserTo.getDate()) + "' ");

                    rs_4 = DB.search("SELECT g.*, rg.* FROM grn g INNER JOIN returngrn rg ON g.idGrn = rg.idGrn WHERE rg.iditems = '" + dtm_1.getValueAt(selectedRow_1, 0) + "' AND rg.returnDate = '" + df.format(dateChooserTo.getDate()) + "' ");

                }

                while (rs_1.next()) {

                    dtm_2.addRow(new Object[]{
                        "INV-" + rs_1.getString("invoiceNo"),
                        rs_1.getString("status"),
                        rs_1.getDate("invoiceDate"),
                        "- " + rs_1.getDouble("qty"),
                        Decimal_Formats.Price((rs_1.getDouble("sellingPrice"))),
                        Decimal_Formats.Price((rs_1.getDouble("discount"))) + "%",
                        Decimal_Formats.Price((rs_1.getDouble("totalAmount")))
                    });
                }

                while (rs_2.next()) {

                    dtm_2.addRow(new Object[]{
                        "INV-" + rs_2.getString("invoiceNo"),
                        rs_2.getString("returnstatus"),
                        rs_2.getDate("returnDate"),
                        "+ " + rs_2.getDouble("qty"),
                        Decimal_Formats.Price((rs_2.getDouble("returnPrice"))),
                        ".00%",
                        Decimal_Formats.Price((rs_2.getDouble("returnTotal")))
                    });
                }

                while (rs_3.next()) {

                    dtm_2.addRow(new Object[]{
                        "GR-" + rs_3.getString("billNo"),
                        rs_3.getString("status"),
                        rs_3.getDate("date"),
                        "+ " + rs_3.getDouble("qty"),
                        Decimal_Formats.Price((rs_3.getDouble("unitPrice"))),
                        Decimal_Formats.Price((rs_3.getDouble("discount"))) + "%",
                        Decimal_Formats.Price((rs_3.getDouble("totalAmount")))
                    });
                }

                while (rs_4.next()) {

                    dtm_2.addRow(new Object[]{
                        "GR-" + rs_4.getString("billNo"),
                        rs_4.getString("returnstatus"),
                        rs_4.getDate("returnDate"),
                        "- " + rs_4.getDouble("qty"),
                        Decimal_Formats.Price((rs_4.getDouble("returnPrice"))),
                        ".00%",
                        Decimal_Formats.Price((rs_4.getDouble("returnTotal")))
                    });
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
