package Items;

import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Items extends javax.swing.JFrame {

    public Items() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        tableASC();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        itemPopUp = new javax.swing.JPopupMenu();
        itemHistory = new javax.swing.JMenuItem();
        popupEdit = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAddItems = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblItems = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        txtItems = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        btnPriceList = new javax.swing.JLabel();

        itemPopUp.setBackground(new java.awt.Color(102, 102, 102));
        itemPopUp.setForeground(new java.awt.Color(255, 255, 255));
        itemPopUp.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        itemPopUp.setOpaque(false);

        itemHistory.setBackground(new java.awt.Color(102, 102, 102));
        itemHistory.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        itemHistory.setForeground(new java.awt.Color(255, 255, 255));
        itemHistory.setText("Item History");
        itemHistory.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        itemHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemHistoryActionPerformed(evt);
            }
        });
        itemPopUp.add(itemHistory);

        popupEdit.setBackground(new java.awt.Color(102, 102, 102));
        popupEdit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        popupEdit.setForeground(new java.awt.Color(255, 255, 255));
        popupEdit.setText("Edit");
        popupEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        popupEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupEditActionPerformed(evt);
            }
        });
        itemPopUp.add(popupEdit);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Items");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 26));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddItems.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddItems.setForeground(new java.awt.Color(255, 255, 255));
        btnAddItems.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddItems.setText("+");
        btnAddItems.setToolTipText("Add item");
        btnAddItems.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddItemsMouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddItemsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddItemsMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddItems, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddItems, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 30, 30));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblItems.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "BRAND", "CATEGORY", "PART NO", "NAME", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblItems.setFocusable(false);
        tblItems.setOpaque(false);
        tblItems.setRowHeight(35);
        tblItems.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblItems.setShowVerticalLines(false);
        tblItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblItemsMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblItemsMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblItems);
        if (tblItems.getColumnModel().getColumnCount() > 0) {
            tblItems.getColumnModel().getColumn(0).setMinWidth(0);
            tblItems.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblItems.getColumnModel().getColumn(0).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 700, 380));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 90, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 90, 260, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 110, 260, 20));

        txtItems.setEditable(false);
        txtItems.setBackground(new java.awt.Color(255, 255, 255));
        txtItems.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItems.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItems.setText("0");
        txtItems.setBorder(null);
        jPanel3.add(txtItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 540, 60, 20));
        jPanel3.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 560, 60, 10));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Items :");
        jPanel3.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 540, -1, -1));

        btnPriceList.setBackground(new java.awt.Color(255, 255, 255));
        btnPriceList.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnPriceList.setForeground(new java.awt.Color(0, 153, 153));
        btnPriceList.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPriceList.setText("Price List");
        btnPriceList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnPriceList.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnPriceList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPriceListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPriceListMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPriceListMouseExited(evt);
            }
        });
        jPanel3.add(btnPriceList, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 540, 110, 25));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblItemsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItemsMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblItems.getSelectedRowCount() != 0) {
                itemPopUp.show(evt.getComponent(), evt.getX(), evt.getY());
            }

            // }
        }

    }//GEN-LAST:event_tblItemsMouseReleased

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void popupEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupEditActionPerformed
        new EditItems().setVisible(true);
    }//GEN-LAST:event_popupEditActionPerformed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblItems.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE  (brandName LIKE '%' '" + txtSearch.getText() + "' '%' OR categoryName LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsPartNo LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsName LIKE '%' '" + txtSearch.getText() + "' '%') ORDER BY brandName ASC");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("brandName"),
                    rs.getString("categoryName"),
                    rs.getString("itemsPartNo"),
                    rs.getString("itemsName"),
                    rs.getString("description")
                });
            }

            txtItems.setText(String.valueOf(dtm.getRowCount()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void tblItemsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblItemsMousePressed
//        JTable table = (JTable) evt.getSource();
//        Point point = evt.getPoint();
//        int row = table.rowAtPoint(point);
//        if (evt.getClickCount() == 2 && table.getSelectedRow() != -1) {
//            System.out.println("double click");
//        }
    }//GEN-LAST:event_tblItemsMousePressed

    private void btnAddItemsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddItemsMouseExited
        btnAddItems.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddItemsMouseExited

    private void btnAddItemsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddItemsMouseEntered
        btnAddItems.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddItemsMouseEntered

    private void btnAddItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddItemsMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new AddItems().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddItemsMouseClicked

    private void itemHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemHistoryActionPerformed
        new ItemHistory().setVisible(true);
    }//GEN-LAST:event_itemHistoryActionPerformed

    private void btnPriceListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceListMouseClicked
        if (UserPrivilage.equals("admin")) {
            new AdminPriceList().setVisible(true);
        }else{
            new UserPriceList().setVisible(true);
        }
    }//GEN-LAST:event_btnPriceListMouseClicked

    private void btnPriceListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceListMouseEntered
        btnPriceList.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnPriceListMouseEntered

    private void btnPriceListMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPriceListMouseExited
        btnPriceList.setBackground(Color.white);
    }//GEN-LAST:event_btnPriceListMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Items().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddItems;
    private javax.swing.JLabel btnPriceList;
    private javax.swing.JMenuItem itemHistory;
    private javax.swing.JPopupMenu itemPopUp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JLabel lbClose;
    private javax.swing.JMenuItem popupEdit;
    public static javax.swing.JTable tblItems;
    public static javax.swing.JTextField txtItems;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        itemPopUp.setOpaque(true);
        tblItems.setOpaque(true);
        btnAddItems.setOpaque(true);
        btnPriceList.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnPriceList.setBackground(Color.white);
        tblItems.setBackground(new Color(255, 255, 255));
        btnAddItems.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblItems.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblItems.getTableHeader().setOpaque(false);
        tblItems.getTableHeader().setBackground(Color.white);
        tblItems.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblItems.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        
        JTableHeader headerResumen = tblItems.getTableHeader();
        headerResumen.setResizingAllowed(true);

    }

    private void tableASC() {

        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tblItems.getModel());
        tblItems.setRowSorter(sorter);
        ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();

    }

    public static void loadDetails() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblItems.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT i.*,b.*,c.* FROM items i INNER JOIN brands b ON i.idbrands = b.idbrands INNER JOIN category c ON i.idcategory = c.idcategory WHERE i.isActive = 1 ORDER BY brandName ASC");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("iditems"),
                    rs.getString("brandName"),
                    rs.getString("categoryName"),
                    rs.getString("itemsPartNo"),
                    rs.getString("itemsName"),
                    rs.getString("description")
                });
            }

            txtItems.setText(String.valueOf(dtm.getRowCount()));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
