package MainMenu;

import Cashbook.Cashbook;
import Customer.Customer;
import ExClasses.DB;
import ExClasses.DBBackup;
import ExClasses.NotificationPopup;
import Items.Items;
import static MainMenu.Login.UserName;
import static MainMenu.Login.UserPrivilage;
import Stock.Stock;
import Supplier.Supplier;
import User.User;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Dashbord extends javax.swing.JFrame {

    public Dashbord() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        lbUsername.setText(UserName);

//        if (UserPrivilage.equals("admin")) {
//            lbCashBook.setVisible(true);
//            lbBackup.setVisible(true);
//            lbUser.setVisible(true);
//        } else {
//            lbCashBook.setVisible(false);
//            lbBackup.setVisible(false);
//            lbUser.setVisible(false);
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lbUsername = new javax.swing.JLabel();
        lbUser = new javax.swing.JLabel();
        lbLogout = new javax.swing.JLabel();
        lbItems = new javax.swing.JLabel();
        lbSupplier = new javax.swing.JLabel();
        lbUser5 = new javax.swing.JLabel();
        lbCustomer = new javax.swing.JLabel();
        lbCashBook = new javax.swing.JLabel();
        lbBackup = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel3MouseClicked(evt);
            }
        });
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));
        jPanel1.setPreferredSize(new java.awt.Dimension(180, 780));
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbUsername.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        lbUsername.setForeground(new java.awt.Color(255, 255, 255));
        lbUsername.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/User_32x32.png"))); // NOI18N
        lbUsername.setText("user");
        jPanel1.add(lbUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 110, 27));

        lbUser.setBackground(new java.awt.Color(90, 0, 156));
        lbUser.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbUser.setForeground(new java.awt.Color(255, 255, 255));
        lbUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbUser.setText("User");
        lbUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbUserMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbUserMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbUserMouseExited(evt);
            }
        });
        jPanel1.add(lbUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 450, 150, 43));

        lbLogout.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Log Out_32x32.png"))); // NOI18N
        lbLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbLogoutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbLogoutMouseEntered(evt);
            }
        });
        jPanel1.add(lbLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 670, 70, 40));

        lbItems.setBackground(new java.awt.Color(90, 0, 156));
        lbItems.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbItems.setForeground(new java.awt.Color(255, 255, 255));
        lbItems.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbItems.setText("Items");
        lbItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbItemsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbItemsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbItemsMouseExited(evt);
            }
        });
        jPanel1.add(lbItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 150, 43));

        lbSupplier.setBackground(new java.awt.Color(90, 0, 156));
        lbSupplier.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbSupplier.setForeground(new java.awt.Color(255, 255, 255));
        lbSupplier.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbSupplier.setText("Supplier");
        lbSupplier.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbSupplierMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbSupplierMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbSupplierMouseExited(evt);
            }
        });
        jPanel1.add(lbSupplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 250, 150, 43));

        lbUser5.setBackground(new java.awt.Color(90, 0, 156));
        lbUser5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbUser5.setForeground(new java.awt.Color(255, 255, 255));
        lbUser5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbUser5.setText("Stock");
        lbUser5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbUser5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbUser5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbUser5MouseExited(evt);
            }
        });
        jPanel1.add(lbUser5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 150, 43));

        lbCustomer.setBackground(new java.awt.Color(90, 0, 156));
        lbCustomer.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbCustomer.setForeground(new java.awt.Color(255, 255, 255));
        lbCustomer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCustomer.setText("Customer");
        lbCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCustomerMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCustomerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCustomerMouseExited(evt);
            }
        });
        jPanel1.add(lbCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 150, 43));

        lbCashBook.setBackground(new java.awt.Color(90, 0, 156));
        lbCashBook.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbCashBook.setForeground(new java.awt.Color(255, 255, 255));
        lbCashBook.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCashBook.setText("Cashbook");
        lbCashBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCashBookMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCashBookMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCashBookMouseExited(evt);
            }
        });
        jPanel1.add(lbCashBook, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 150, 43));

        lbBackup.setBackground(new java.awt.Color(90, 0, 156));
        lbBackup.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lbBackup.setForeground(new java.awt.Color(255, 255, 255));
        lbBackup.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbBackup.setText("Backup");
        lbBackup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbBackupMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbBackupMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbBackupMouseExited(evt);
            }
        });
        jPanel1.add(lbBackup, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 150, 43));

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 150, 760));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel2MouseClicked(evt);
            }
        });
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(0, 0, 0));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setBackground(new java.awt.Color(0, 0, 0));
        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Contact Support - 0776953022");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 250, 30));

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 720, 250, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/dashboard.jpg"))); // NOI18N
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 970, 760));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 0, 970, 760));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1120, 760));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbUserMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUserMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbUser.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbUser.setForeground(color2);
    }//GEN-LAST:event_lbUserMouseEntered

    private void lbUserMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUserMouseExited
        Color color1 = new Color(90, 0, 156);
        lbUser.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbUser.setForeground(color2);
    }//GEN-LAST:event_lbUserMouseExited

    private void lbLogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbLogoutMouseClicked

        int response = JOptionPane.showConfirmDialog(this, "Do you want to exit?", "System Exit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }

        if (response == JOptionPane.YES_OPTION) {

            System.exit(0);

        }

    }//GEN-LAST:event_lbLogoutMouseClicked

    private void lbUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUserMouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
        new User().setVisible(true);
    }//GEN-LAST:event_lbUserMouseClicked

    private void lbLogoutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbLogoutMouseEntered

    }//GEN-LAST:event_lbLogoutMouseEntered

    private void lbItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbItemsMouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
        new Items().setVisible(true);
    }//GEN-LAST:event_lbItemsMouseClicked

    private void lbItemsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbItemsMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbItems.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbItems.setForeground(color2);
    }//GEN-LAST:event_lbItemsMouseEntered

    private void lbItemsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbItemsMouseExited
        Color color1 = new Color(90, 0, 156);
        lbItems.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbItems.setForeground(color2);
    }//GEN-LAST:event_lbItemsMouseExited

    private void lbSupplierMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbSupplierMouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
        new Supplier().setVisible(true);
    }//GEN-LAST:event_lbSupplierMouseClicked

    private void lbSupplierMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbSupplierMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbSupplier.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbSupplier.setForeground(color2);
    }//GEN-LAST:event_lbSupplierMouseEntered

    private void lbSupplierMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbSupplierMouseExited
        Color color1 = new Color(90, 0, 156);
        lbSupplier.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbSupplier.setForeground(color2);
    }//GEN-LAST:event_lbSupplierMouseExited

    private void lbUser5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUser5MouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
        new Stock().setVisible(true);
    }//GEN-LAST:event_lbUser5MouseClicked

    private void lbUser5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUser5MouseEntered
        Color color1 = new Color(204, 204, 204);
        lbUser5.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbUser5.setForeground(color2);
    }//GEN-LAST:event_lbUser5MouseEntered

    private void lbUser5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUser5MouseExited
        Color color1 = new Color(90, 0, 156);
        lbUser5.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbUser5.setForeground(color2);
    }//GEN-LAST:event_lbUser5MouseExited

    private void lbCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCustomerMouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
        new Customer().setVisible(true);
    }//GEN-LAST:event_lbCustomerMouseClicked

    private void lbCustomerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCustomerMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbCustomer.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbCustomer.setForeground(color2);
    }//GEN-LAST:event_lbCustomerMouseEntered

    private void lbCustomerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCustomerMouseExited
        Color color1 = new Color(90, 0, 156);
        lbCustomer.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbCustomer.setForeground(color2);
    }//GEN-LAST:event_lbCustomerMouseExited

    private void lbBackupMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbBackupMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbBackup.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbBackup.setForeground(color2);
    }//GEN-LAST:event_lbBackupMouseEntered

    private void lbBackupMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbBackupMouseExited
        Color color1 = new Color(90, 0, 156);
        lbBackup.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbBackup.setForeground(color2);
    }//GEN-LAST:event_lbBackupMouseExited

    private void lbBackupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbBackupMouseClicked
        DBBackup.BACKUPDB();
    }//GEN-LAST:event_lbBackupMouseClicked

    private void lbCashBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCashBookMouseClicked
        if (UserPrivilage.equals("admin")) {
            java.awt.Window win[] = java.awt.Window.getWindows();
            for (int i = 2; i < win.length; i++) {
                win[i].dispose();
                win[i] = null;
            }
            new Cashbook().setVisible(true);
        }
    }//GEN-LAST:event_lbCashBookMouseClicked

    private void lbCashBookMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCashBookMouseEntered
        Color color1 = new Color(204, 204, 204);
        lbCashBook.setBackground(color1);
        Color color2 = new Color(0, 0, 0);
        lbCashBook.setForeground(color2);
    }//GEN-LAST:event_lbCashBookMouseEntered

    private void lbCashBookMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCashBookMouseExited
        Color color1 = new Color(90, 0, 156);
        lbCashBook.setBackground(color1);
        Color color2 = new Color(255, 255, 255);
        lbCashBook.setForeground(color2);
    }//GEN-LAST:event_lbCashBookMouseExited

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jPanel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
    }//GEN-LAST:event_jPanel3MouseClicked

    private void jPanel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
    }//GEN-LAST:event_jPanel2MouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        java.awt.Window win[] = java.awt.Window.getWindows();
        for (int i = 2; i < win.length; i++) {
            win[i].dispose();
            win[i] = null;
        }
    }//GEN-LAST:event_jLabel1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Dashbord().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lbBackup;
    private javax.swing.JLabel lbCashBook;
    private javax.swing.JLabel lbCustomer;
    private javax.swing.JLabel lbItems;
    private javax.swing.JLabel lbLogout;
    private javax.swing.JLabel lbSupplier;
    private javax.swing.JLabel lbUser;
    private javax.swing.JLabel lbUser5;
    private javax.swing.JLabel lbUsername;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbUser.setOpaque(true);
        lbItems.setOpaque(true);
        lbSupplier.setOpaque(true);
        lbCustomer.setOpaque(true);
        lbUser5.setOpaque(true);
        lbBackup.setOpaque(true);
        lbCashBook.setOpaque(true);
    }
}
