package MainMenu;

import ExClasses.DB;
import User.AddAdmin;
import java.sql.ResultSet;

public class first {

    public static void main(String[] args) {
        try {
            ResultSet rs = DB.search("SELECT * FROM users");

            if (!rs.next()) {

                new AddAdmin().setVisible(true);

            } else {
                new Login().setVisible(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
