package OldInvoice;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import static OldInvoice.OldInvoice.loadDetails;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class AddNew extends javax.swing.JFrame {

    boolean TXT_VALIDATE;
    boolean INV_VALIDATE;

    public AddNew() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        dateChooser.setDate(new Date());
        txtCustomer.grabFocus();
        lbChequeNo.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtCustomer = new javax.swing.JTextField();
        btnSave = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txtInvoiceNo = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        comboType = new javax.swing.JComboBox<>();
        lbChequeNo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Add New");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 80, 130, 20));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date :*");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 80, 40, -1));

        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        txtTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTotalKeyTyped(evt);
            }
        });
        jPanel3.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 230, 270, 20));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("InvoiceTotal :*");
        jPanel3.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 230, 90, -1));

        txtCustomer.setBackground(new java.awt.Color(255, 255, 255));
        txtCustomer.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtCustomer.setBorder(null);
        jPanel3.add(txtCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, 270, 20));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("SAVE");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 410, 140, 30));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setText("Customer :*");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, -1, -1));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 270, 20));
        jPanel3.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 250, 270, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Invoice no :*");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, -1, -1));

        txtInvoiceNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtInvoiceNo.setBorder(null);
        jPanel3.add(txtInvoiceNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 180, 270, 20));
        jPanel3.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 270, 20));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Type :*");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 340, 50, -1));

        comboType.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboType.setForeground(new java.awt.Color(0, 153, 153));
        comboType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "Cheque" }));
        comboType.setBorder(null);
        comboType.setOpaque(false);
        comboType.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboTypePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTypeActionPerformed(evt);
            }
        });
        jPanel3.add(comboType, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 340, 270, 25));

        lbChequeNo.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        lbChequeNo.setForeground(new java.awt.Color(0, 153, 153));
        lbChequeNo.setText("000");
        jPanel3.add(lbChequeNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 370, 270, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Paid : ");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, -1, -1));

        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setBorder(null);
        txtPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaidKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidKeyTyped(evt);
            }
        });
        jPanel2.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 270, 20));
        jPanel2.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 270, 10));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 450, 50));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 620, 540));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        try {

            if (dateChooser.getDate() != null) {
                validateTXT();
                if (TXT_VALIDATE == false) {
                    validateInvoice();
                    if (INV_VALIDATE == false) {
                        savePaid();
                        ClearTXT();
                        NotificationPopup.save();
                    } else {
                        alreadyInv();
                    }

                } else {
                    NotificationPopup.fillFeilds();
                }
            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void txtPaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {

                if (dateChooser.getDate() != null) {
                    validateTXT();
                    if (TXT_VALIDATE == false) {
                        validateInvoice();
                        if (INV_VALIDATE == false) {
                            savePaid();
                            ClearTXT();
                            NotificationPopup.save();
                        } else {
                            alreadyInv();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }
                } else {
                    NotificationPopup.fillFeilds();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_txtPaidKeyReleased

    private void txtPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtPaidKeyTyped

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void comboTypePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboTypePopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboTypePopupMenuWillBecomeInvisible

    private void comboTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTypeActionPerformed
        try {
            if (comboType.getSelectedIndex() == 1) {
                String chequeNo = JOptionPane.showInputDialog("Enter Cheque No");

                if (chequeNo.equals("")) {
                    comboType.setSelectedIndex(0);
                } else {
                    lbChequeNo.setText(chequeNo);
                    lbChequeNo.setVisible(true);
                }

            } else {
                lbChequeNo.setText("");
                lbChequeNo.setVisible(false);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_comboTypeActionPerformed

    private void txtTotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtTotalKeyTyped

    private void txtTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {

                if (dateChooser.getDate() != null) {
                    validateTXT();
                    if (TXT_VALIDATE == false) {
                        validateInvoice();
                        if (INV_VALIDATE == false) {
                            savePaid();
                            ClearTXT();
                            NotificationPopup.save();
                        } else {
                            alreadyInv();
                        }

                    } else {
                        NotificationPopup.fillFeilds();
                    }
                } else {
                    NotificationPopup.fillFeilds();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_txtTotalKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddNew().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    public static javax.swing.JComboBox<String> comboType;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbChequeNo;
    private javax.swing.JLabel lbClose;
    private javax.swing.JTextField txtCustomer;
    private javax.swing.JTextField txtInvoiceNo;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    private void savePaid() {
        try {

            Double paid = null;

            if (txtPaid.getText().isEmpty()) {
                paid = 0.0;
            } else {
                paid = Double.parseDouble(txtPaid.getText());
            }

            if (!(Double.parseDouble(txtTotal.getText()) < paid)) {

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

                String payType = null;

                if (comboType.getSelectedIndex() == 1) {
                    payType = comboType.getSelectedItem().toString() + " (" + lbChequeNo.getText().toLowerCase() + ")";
                } else {
                    payType = comboType.getSelectedItem().toString();
                }

                if (txtPaid.getText().isEmpty() || txtPaid.getText().equals("0")) {
                    String insertInvoice = "INSERT INTO oldinvoice(customer, invoiceNo, invoiceDate, invoiceTotal, invoicePaid, paymentType)"
                            + "VALUES ('" + txtCustomer.getText() + "', '" + txtInvoiceNo.getText() + "', '" + date + "', '" + Double.parseDouble(txtTotal.getText()) + "', '" + paid + "', '')";

                    DB.idu(insertInvoice);
                } else {
                    String insertInvoice = "INSERT INTO oldinvoice(customer, invoiceNo, invoiceDate, invoiceTotal, invoicePaid, paymentType)"
                            + "VALUES ('" + txtCustomer.getText() + "', '" + txtInvoiceNo.getText() + "', '" + date + "', '" + Double.parseDouble(txtTotal.getText()) + "', '" + paid + "', '" + payType + "')";

                    DB.idu(insertInvoice);
                }

                ResultSet rs = DB.search("SELECT * FROM oldinvoice ORDER BY oldInvId DESC ");

                if (rs.next()) {

                    if (txtPaid.getText().isEmpty() || txtPaid.getText().equals("0")) {
                        String insertPayment = "INSERT INTO oldinvpayment(paymentDate, paymentTotal, paidTotal, paymentType, oldInvId)"
                                + "VALUES ('" + date + "', '" + Double.parseDouble(txtTotal.getText()) + "', '" + paid + "', '', '" + rs.getInt("oldInvId") + "')";

                        DB.idu(insertPayment);
                    } else {
                        String insertPayment = "INSERT INTO oldinvpayment(paymentDate, paymentTotal, paidTotal, paymentType, oldInvId)"
                                + "VALUES ('" + date + "', '" + Double.parseDouble(txtTotal.getText()) + "', '" + paid + "', '" + payType + "', '" + rs.getInt("oldInvId") + "')";

                        DB.idu(insertPayment);
                    }

                }
                
                loadDetails();

            } else {
                NotificationPopup.incorrectPayment();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void validateInvoice() {

        try {

            ResultSet rs = DB.search("SELECT * FROM oldinvoice WHERE invoiceNo = '" + txtInvoiceNo.getText() + "' ");

            if (!rs.next()) {
                INV_VALIDATE = false;
            } else {
                INV_VALIDATE = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void validateTXT() {

        Component[] cp = jPanel3.getComponents();
        for (Component c : cp) {

            if (c instanceof JTextField) {

                if ((((JTextField) c).getText().isEmpty())) {

                    TXT_VALIDATE = true;

                    break;

                } else {

                    TXT_VALIDATE = false;

                }

            }

        }
    }

    private void ClearTXT() {

        Component[] cp = jPanel3.getComponents();
        for (Component c : cp) {

            if (c instanceof JTextField) {

                ((JTextField) c).setText(null);

            }

        }
        txtPaid.setText("");
        comboType.setSelectedIndex(0);
        lbChequeNo.setVisible(false);
        txtCustomer.grabFocus();

    }

    private void alreadyInv() {
        int TIME_VISIBLE = 2000;
        JOptionPane pane = new JOptionPane("Invoice number already added..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        new Timer(TIME_VISIBLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);

            }

        }).start();

    }

}
