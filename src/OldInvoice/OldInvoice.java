package OldInvoice;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class OldInvoice extends javax.swing.JFrame {

    public OldInvoice() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        outstanding = new javax.swing.JMenuItem();
        deletePopup = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOldInv = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtBalance = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        btnAddnew = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        dateChooserFrom = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        dateChooserTo = new com.toedter.calendar.JDateChooser();
        dateSearch = new javax.swing.JLabel();
        lbRefesh = new javax.swing.JLabel();

        jPopupMenu1.setBackground(new java.awt.Color(102, 102, 102));
        jPopupMenu1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPopupMenu1.setPreferredSize(new java.awt.Dimension(110, 50));

        outstanding.setBackground(new java.awt.Color(102, 102, 102));
        outstanding.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        outstanding.setForeground(new java.awt.Color(255, 255, 255));
        outstanding.setText("Pay Oustanding");
        outstanding.setBorder(null);
        outstanding.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outstandingActionPerformed(evt);
            }
        });
        jPopupMenu1.add(outstanding);

        deletePopup.setBackground(new java.awt.Color(102, 102, 102));
        deletePopup.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        deletePopup.setForeground(new java.awt.Color(255, 255, 255));
        deletePopup.setText("Delete");
        deletePopup.setBorder(null);
        deletePopup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePopupActionPerformed(evt);
            }
        });
        jPopupMenu1.add(deletePopup);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Invoice");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblOldInv.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblOldInv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "INV ID", "CUSTOMER", "INVOICE NO", "DATE", "TOTAL", "PAID", "TYPE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOldInv.setFocusable(false);
        tblOldInv.setRowHeight(30);
        tblOldInv.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblOldInv.setShowVerticalLines(false);
        tblOldInv.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOldInvMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblOldInvMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblOldInv);
        if (tblOldInv.getColumnModel().getColumnCount() > 0) {
            tblOldInv.getColumnModel().getColumn(0).setMinWidth(0);
            tblOldInv.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblOldInv.getColumnModel().getColumn(0).setMaxWidth(0);
            tblOldInv.getColumnModel().getColumn(1).setPreferredWidth(150);
            tblOldInv.getColumnModel().getColumn(6).setPreferredWidth(150);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 720, 390));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Total :");
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel4.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 130, 20));
        jPanel4.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 40, 130, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Paid :");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, -1, -1));

        txtPaid.setEditable(false);
        txtPaid.setBackground(new java.awt.Color(255, 255, 255));
        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setText("0.00");
        txtPaid.setBorder(null);
        txtPaid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPaidActionPerformed(evt);
            }
        });
        jPanel4.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 130, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 130, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Balance :");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 20, -1, -1));

        txtBalance.setEditable(false);
        txtBalance.setBackground(new java.awt.Color(255, 255, 255));
        txtBalance.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtBalance.setText("0.00");
        txtBalance.setBorder(null);
        txtBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceActionPerformed(evt);
            }
        });
        jPanel4.add(txtBalance, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 20, 130, 20));
        jPanel4.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 40, 130, 10));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 550, 630, 60));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddnew.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddnew.setForeground(new java.awt.Color(255, 255, 255));
        btnAddnew.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddnew.setText("+");
        btnAddnew.setToolTipText("Add user");
        btnAddnew.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddnew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddnewMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddnewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddnewMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddnew, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddnew, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 30, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, -1, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 210, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 210, 10));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("From :");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 40, -1));

        dateChooserFrom.setDateFormatString("yyyy-MM-dd");
        dateChooserFrom.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserFrom, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 110, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("To :");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 90, 30, -1));

        dateChooserTo.setDateFormatString("yyyy-MM-dd");
        dateChooserTo.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserTo, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 90, 110, 20));

        dateSearch.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        dateSearch.setForeground(new java.awt.Color(0, 153, 153));
        dateSearch.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        dateSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dateSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateSearchMouseClicked(evt);
            }
        });
        jPanel3.add(dateSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 90, 40, 20));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel3.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 90, 30, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblOldInvMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOldInvMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblOldInv.getSelectedRowCount() != 0) {
                jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}

        }
    }//GEN-LAST:event_tblOldInvMouseReleased

    private void outstandingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outstandingActionPerformed
        new OutstandViaCus().setVisible(true);
    }//GEN-LAST:event_outstandingActionPerformed

    private void deletePopupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePopupActionPerformed
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
            int selectedRow = tblOldInv.getSelectedRow();

            int response = JOptionPane.showConfirmDialog(this, "Do you want delete " + dtm.getValueAt(selectedRow, 1).toString().toUpperCase() + " invoice?", "Delete user", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {

                String deleteInvoice = "UPDATE oldinvoice SET isActive = 0 WHERE oldInvId = '" + dtm.getValueAt(selectedRow, 0) + "' ";
                DB.idu(deleteInvoice);

                ResultSet rs = DB.search("SELECT * FROM oldinvpayment WHERE oldInvId = '" + dtm.getValueAt(selectedRow, 0) + "' ");

                while (rs.next()) {
                    String deleteInvPay = "UPDATE oldinvpayment SET isActive = 0 WHERE oldInvId = '" + dtm.getValueAt(selectedRow, 0) + "' ";
                    DB.idu(deleteInvPay);
                }

                loadDetails();

                NotificationPopup.delete();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_deletePopupActionPerformed

    private void tblOldInvMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOldInvMouseClicked
        txtSearch.setText(null);
    }//GEN-LAST:event_tblOldInvMouseClicked

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPaidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPaidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPaidActionPerformed

    private void txtBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceActionPerformed

    private void btnAddnewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddnewMouseClicked
        //if (UserPrivilage.equals("admin")) {

        new AddNew().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddnewMouseClicked

    private void btnAddnewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddnewMouseEntered
        btnAddnew.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddnewMouseEntered

    private void btnAddnewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddnewMouseExited
        btnAddnew.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddnewMouseExited

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            dateChooserFrom.setDate(null);
            dateChooserTo.setDate(null);

            ResultSet rs = DB.search("SELECT * FROM oldinvoice WHERE (customer LIKE '%' '" + txtSearch.getText().toUpperCase() + "' '%' OR invoiceNo LIKE '%' '" + txtSearch.getText().toUpperCase() + "' '%') AND isActive = 1 ORDER BY invoiceDate DESC ");

            DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                dtm.addRow(new Object[]{
                    rs.getInt("oldInvId"),
                    rs.getString("customer").toUpperCase(),
                    "INV-" + rs.getString("invoiceNo"),
                    rs.getString("invoiceDate"),
                    Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                    Decimal_Formats.Price(rs.getDouble("invoicePaid")),
                    rs.getString("paymentType")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void dateSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateSearchMouseClicked
        dateSearch();
    }//GEN-LAST:event_dateSearchMouseClicked

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        txtSearch.setText(null);
        dateChooserFrom.setDate(null);
        dateChooserTo.setDate(null);
        DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
        dtm.setRowCount(0);
        loadDetails();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(dtm);
        tblOldInv.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(txtSearch.getText().toUpperCase()));
    }//GEN-LAST:event_lbRefeshMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OldInvoice().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddnew;
    private com.toedter.calendar.JDateChooser dateChooserFrom;
    private com.toedter.calendar.JDateChooser dateChooserTo;
    private javax.swing.JLabel dateSearch;
    private javax.swing.JMenuItem deletePopup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbRefesh;
    private javax.swing.JMenuItem outstanding;
    public static javax.swing.JTable tblOldInv;
    public static javax.swing.JTextField txtBalance;
    public static javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtSearch;
    public static javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnAddnew.setOpaque(true);

    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnAddnew.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblOldInv.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblOldInv.getTableHeader().setOpaque(false);
        tblOldInv.getTableHeader().setBackground(Color.white);
        tblOldInv.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblOldInv.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblOldInv.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblOldInv.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblOldInv.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        tblOldInv.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
    }

    public static void loadDetails() {
        try {
            ResultSet rs = DB.search("SELECT * FROM oldinvoice WHERE isActive = 1 ORDER BY invoiceDate DESC ");

            DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                dtm.addRow(new Object[]{
                    rs.getInt("oldInvId"),
                    rs.getString("customer").toUpperCase(),
                    "INV-" + rs.getString("invoiceNo"),
                    rs.getString("invoiceDate"),
                    Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                    Decimal_Formats.Price(rs.getDouble("invoicePaid")),
                    rs.getString("paymentType")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void dateSearch() {
        try {

            txtSearch.setText(null);

            if (dateChooserFrom.getDate() != null || dateChooserTo.getDate() != null) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
                dtm.setRowCount(0);

                ResultSet rs = null;

                if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() != null) {

                    rs = DB.search("SELECT * FROM oldinvoice WHERE invoiceDate BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' AND isActive = 1 ORDER BY invoiceDate DESC ");

                } else if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() == null) {

                    rs = DB.search("SELECT * FROM oldinvoice WHERE invoiceDate = '" + df.format(dateChooserFrom.getDate()) + "' AND isActive = 1 ORDER BY invoiceDate DESC ");

                } else if (dateChooserTo.getDate() != null && dateChooserFrom.getDate() == null) {

                    rs = DB.search("SELECT * FROM oldinvoice WHERE invoiceDate = '" + df.format(dateChooserTo.getDate()) + "' AND isActive = 1 ORDER BY invoiceDate DESC ");

                }

                while (rs.next()) {

                    dtm.addRow(new Object[]{
                        rs.getInt("oldInvId"),
                        rs.getString("customer").toUpperCase(),
                        "INV-" + rs.getString("invoiceNo"),
                        rs.getString("invoiceDate"),
                        Decimal_Formats.Price(rs.getDouble("invoiceTotal")),
                        Decimal_Formats.Price(rs.getDouble("invoicePaid")),
                        rs.getString("paymentType")
                    });
                }

                calculate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculate() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblOldInv.getModel();
            Double total = 0.0;
            Double paid = 0.0;
            Double balance = 0.0;

            for (int i = 0; i < dtm.getRowCount(); i++) {
                total += Double.parseDouble(dtm.getValueAt(i, 4).toString());
                paid += Double.parseDouble(dtm.getValueAt(i, 5).toString());
            }

            balance = total - paid;

            txtTotal.setText("" + Decimal_Formats.Price(total));
            txtPaid.setText("" + Decimal_Formats.Price(paid));
            txtBalance.setText("" + Decimal_Formats.Price(balance));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
