package OldInvoice;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import static OldInvoice.OldInvoice.loadDetails;
import static OldInvoice.OldInvoice.tblOldInv;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class OutstandViaCus extends javax.swing.JFrame {

    boolean TXT_VALIDATE_1 = false;
    boolean TXT_VALIDATE_2 = false;

    Integer invId;

    public OutstandViaCus() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        getDetails();
        lbChequeNo.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOutstand = new javax.swing.JTable();
        lBCustomer = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtBillNo = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        txtTotal = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        txtArrears = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        btnSave = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        comboType = new javax.swing.JComboBox<>();
        lbChequeNo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Pay outstanding");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblOutstand.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblOutstand.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "INVOICE ID", "PAYMENT ID", "INV. TOTAL", "PAID AMOUNT", "LAST PAID", "TYPE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOutstand.setFocusable(false);
        tblOutstand.setRequestFocusEnabled(false);
        tblOutstand.setRowHeight(30);
        tblOutstand.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblOutstand.setShowVerticalLines(false);
        tblOutstand.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOutstandMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblOutstandMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblOutstand);
        if (tblOutstand.getColumnModel().getColumnCount() > 0) {
            tblOutstand.getColumnModel().getColumn(0).setMinWidth(0);
            tblOutstand.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblOutstand.getColumnModel().getColumn(0).setMaxWidth(0);
            tblOutstand.getColumnModel().getColumn(1).setMinWidth(0);
            tblOutstand.getColumnModel().getColumn(1).setPreferredWidth(0);
            tblOutstand.getColumnModel().getColumn(1).setMaxWidth(0);
            tblOutstand.getColumnModel().getColumn(5).setPreferredWidth(150);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 430, 370));

        lBCustomer.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lBCustomer.setText("SET CUSTOMER HERE");
        jPanel3.add(lBCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 400, 20));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 100, 130, 20));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date :*");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 100, 40, -1));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setText("Invoice no :");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 40, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Arrears :");
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        txtBillNo.setEditable(false);
        txtBillNo.setBackground(new java.awt.Color(255, 255, 255));
        txtBillNo.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtBillNo.setBorder(null);
        jPanel4.add(txtBillNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, 160, 20));
        jPanel4.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, 160, 20));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel4.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 160, 20));
        jPanel4.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 160, 20));

        txtArrears.setEditable(false);
        txtArrears.setBackground(new java.awt.Color(255, 255, 255));
        txtArrears.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtArrears.setText("0.00");
        txtArrears.setBorder(null);
        txtArrears.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtArrearsKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtArrearsKeyTyped(evt);
            }
        });
        jPanel4.add(txtArrears, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 160, 20));
        jPanel4.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 150, 160, 10));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("PAY");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel4.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 310, 160, 30));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Pay : *");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, -1, -1));

        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setBorder(null);
        txtPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaidKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidKeyTyped(evt);
            }
        });
        jPanel4.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, 160, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 160, 10));

        comboType.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboType.setForeground(new java.awt.Color(0, 153, 153));
        comboType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "Cheque" }));
        comboType.setBorder(null);
        comboType.setOpaque(false);
        comboType.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboTypePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTypeActionPerformed(evt);
            }
        });
        jPanel4.add(comboType, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 240, 160, 25));

        lbChequeNo.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        lbChequeNo.setForeground(new java.awt.Color(0, 153, 153));
        lbChequeNo.setText("000");
        jPanel4.add(lbChequeNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, 160, -1));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Type :*");
        jPanel4.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 50, -1));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 150, 300, 370));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblOutstandMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandMouseReleased

    }//GEN-LAST:event_tblOutstandMouseReleased

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        savePaid();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void tblOutstandMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblOutstandMouseClicked

    private void txtArrearsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyTyped
        char c = evt.getKeyChar();
        if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtArrearsKeyTyped

    private void txtArrearsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyReleased

    }//GEN-LAST:event_txtArrearsKeyReleased

    private void txtPaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            savePaid();
        }
    }//GEN-LAST:event_txtPaidKeyReleased

    private void txtPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtPaidKeyTyped

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void comboTypePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboTypePopupMenuWillBecomeInvisible

    }//GEN-LAST:event_comboTypePopupMenuWillBecomeInvisible

    private void comboTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTypeActionPerformed
        try {
            if (comboType.getSelectedIndex() == 1) {
                String chequeNo = JOptionPane.showInputDialog("Enter Cheque No");

                if (chequeNo.equals("")) {
                    comboType.setSelectedIndex(0);
                } else {
                    lbChequeNo.setText(chequeNo);
                    lbChequeNo.setVisible(true);
                }

            } else {
                lbChequeNo.setText("");
                lbChequeNo.setVisible(false);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_comboTypeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OutstandViaCus().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    public static javax.swing.JComboBox<String> comboType;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lBCustomer;
    private javax.swing.JLabel lbChequeNo;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblOutstand;
    private javax.swing.JTextField txtArrears;
    private javax.swing.JTextField txtBillNo;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    private void DesignTable() {
        tblOutstand.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 11));
        tblOutstand.getTableHeader().setOpaque(false);
        tblOutstand.getTableHeader().setBackground(Color.white);
        tblOutstand.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblOutstand.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblOutstand.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblOutstand.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblOutstand.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
    }

    private void getDetails() {
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            DefaultTableModel dtm_1 = (DefaultTableModel) tblOldInv.getModel();
            int selectedRow = tblOldInv.getSelectedRow();

            DefaultTableModel dtm_2 = (DefaultTableModel) tblOutstand.getModel();
            dtm_2.setRowCount(0);

            ResultSet rs = DB.search("SELECT * FROM oldinvpayment WHERE oldInvId = '" + dtm_1.getValueAt(selectedRow, 0) + "' ORDER BY paymentDate DESC ");

            while (rs.next()) {
                dtm_2.addRow(new Object[]{
                    rs.getInt("oldInvId"),
                    rs.getInt("idoldinvpayment"),
                    Decimal_Formats.Price(rs.getDouble("paymentTotal")),
                    Decimal_Formats.Price(rs.getDouble("paidTotal")),
                    rs.getString("paymentDate"),
                    rs.getString("paymentType")
                });

                dateChooser.setDate(new Date());
                invId = Integer.parseInt(dtm_1.getValueAt(selectedRow, 0).toString());
                lBCustomer.setText(dtm_1.getValueAt(0, 1).toString());
                txtBillNo.setText(dtm_1.getValueAt(selectedRow, 2).toString());
                txtTotal.setText(Decimal_Formats.Price(Double.parseDouble(dtm_1.getValueAt(selectedRow, 4).toString())));

                Double totalPaid = 0.0;

                for (int i = 0; i < dtm_2.getRowCount(); i++) {
                    totalPaid += Double.parseDouble(dtm_2.getValueAt(i, 3).toString());
                }

                txtArrears.setText(Decimal_Formats.Price(Double.parseDouble(txtTotal.getText()) - totalPaid));
                txtPaid.grabFocus();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void savePaid() {
        try {

            if (!(txtPaid.getText().isEmpty() || dateChooser.getDate() == null)) {
                if (!(Double.parseDouble(txtArrears.getText()) < Double.parseDouble(txtPaid.getText()))) {

                    if (Double.parseDouble(txtPaid.getText()) != 0) {
                        DefaultTableModel dtm = (DefaultTableModel) tblOutstand.getModel();

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

                        String payType = null;

                        if (comboType.getSelectedIndex() == 1) {
                            payType = comboType.getSelectedItem().toString() + " (" + lbChequeNo.getText().toLowerCase() + ")";
                        } else {
                            payType = comboType.getSelectedItem().toString();
                        }

                        String insertOutstand = "INSERT INTO oldinvpayment(paymentTotal, paidTotal, paymentDate, paymentType, oldInvId)"
                                + "VALUES ('" + Double.parseDouble(txtTotal.getText()) + "', '" + Double.parseDouble(txtPaid.getText()) + "', '" + date + "', '" + payType + "', '" + invId + "')";

                        DB.idu(insertOutstand);
                        
                        Double paid = null;

                        ResultSet rs = DB.search("SELECT * FROM oldinvoice WHERE oldInvId = '" + invId + "' ");

                        if (rs.next()) {
                            paid = rs.getDouble("invoicePaid");
                        }

                        Double newPaid = paid + Double.parseDouble(txtPaid.getText());

                        String updatePayment = "UPDATE oldinvoice SET invoicePaid = '" + newPaid + "', paymentType = '"+ payType +"' WHERE oldInvId = '" + invId + "' ";
                        DB.idu(updatePayment);

                        dtm.setRowCount(0);

                        ResultSet rs1 = DB.search("SELECT * FROM oldinvpayment WHERE oldInvId = '" + invId + "' ORDER BY paymentDate DESC");

                        while (rs1.next()) {
                            dtm.addRow(new Object[]{
                                rs1.getInt("oldInvId"),
                                rs1.getInt("idoldinvpayment"),
                                Decimal_Formats.Price(rs1.getDouble("paymentTotal")),
                                Decimal_Formats.Price(rs1.getDouble("paidTotal")),
                                rs1.getString("paymentDate"),
                                rs1.getString("paymentType")
                            });
                        }

                        Double totalPaid = 0.0;

                        for (int i = 0; i < dtm.getRowCount(); i++) {
                            totalPaid += Double.parseDouble(dtm.getValueAt(i, 2).toString());
                        }

                        txtArrears.setText(Decimal_Formats.Price(Double.parseDouble(txtTotal.getText()) - totalPaid));

                        txtPaid.setText("");
                        txtPaid.grabFocus();

                        getDetails();
                        loadDetails();
                        NotificationPopup.save();
                    }

                } else {
                    NotificationPopup.incorrectPayment();
                }
            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
