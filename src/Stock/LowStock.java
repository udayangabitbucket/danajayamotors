package Stock;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class LowStock extends javax.swing.JFrame {

    public LowStock() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        comboLowQty.setOpaque(true);
        comboLowQty.setBackground(Color.white);
        lowStock();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        comboLowQty = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLowStock = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        txtItems = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Low stock");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, 26));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 153, 153));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Low qty :");
        jPanel3.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        comboLowQty.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboLowQty.setForeground(new java.awt.Color(0, 153, 153));
        comboLowQty.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "10", "25", "50", "100", "500", "1000" }));
        comboLowQty.setBorder(null);
        comboLowQty.setOpaque(false);
        comboLowQty.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboLowQtyPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboLowQty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboLowQtyActionPerformed(evt);
            }
        });
        jPanel3.add(comboLowQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 70, 25));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblLowStock.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblLowStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PART NO", "DESCRIPTION", "QTY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblLowStock.setFocusable(false);
        tblLowStock.setRowHeight(35);
        tblLowStock.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblLowStock.setShowVerticalLines(false);
        tblLowStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblLowStockMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblLowStock);
        if (tblLowStock.getColumnModel().getColumnCount() > 0) {
            tblLowStock.getColumnModel().getColumn(0).setPreferredWidth(150);
            tblLowStock.getColumnModel().getColumn(1).setPreferredWidth(350);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 670, 420));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 153, 153));
        jLabel11.setText("Items :");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 540, -1, -1));

        txtItems.setEditable(false);
        txtItems.setBackground(new java.awt.Color(255, 255, 255));
        txtItems.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItems.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItems.setText("0");
        txtItems.setBorder(null);
        jPanel3.add(txtItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 540, 60, 20));
        jPanel3.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 560, 60, 10));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void comboLowQtyPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboLowQtyPopupMenuWillBecomeInvisible
        lowStock();
    }//GEN-LAST:event_comboLowQtyPopupMenuWillBecomeInvisible

    private void comboLowQtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboLowQtyActionPerformed

    }//GEN-LAST:event_comboLowQtyActionPerformed

    private void tblLowStockMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblLowStockMouseReleased

    }//GEN-LAST:event_tblLowStockMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LowStock().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JComboBox<String> comboLowQty;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblLowStock;
    public static javax.swing.JTextField txtItems;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
    }

    private void DesignTable() {
        tblLowStock.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblLowStock.getTableHeader().setOpaque(false);
        tblLowStock.getTableHeader().setBackground(Color.white);
        tblLowStock.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblLowStock.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
    }

    private void lowStock() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblLowStock.getModel();
            Integer qty = Integer.parseInt(comboLowQty.getSelectedItem().toString());

            ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.isActive = 1 ORDER BY s.qty ASC ");

            dtm.setRowCount(0);

            while (rs.next()) {

                Integer stockQty = rs.getInt("qty");

                if (stockQty <= qty) {
                    dtm.addRow(new Object[]{
                        rs.getString("itemsPartNo").toUpperCase(),
                        rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                        rs.getString("qty")});
                }

            }

            calculation();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void calculation() {
        DefaultTableModel dtm = (DefaultTableModel) tblLowStock.getModel();

        txtItems.setText(String.valueOf(dtm.getRowCount()));

    }

}
