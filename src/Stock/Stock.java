package Stock;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Stock extends javax.swing.JFrame {

    public static Map<String, Integer> cat = new HashMap();
    public static Map<String, Integer> bra = new HashMap();

    public Stock() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadBrands();
        loadCategory();
        loadStock();
        txtSearch.grabFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        stockPopUp = new javax.swing.JPopupMenu();
        itemHistory = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        btnLowStock = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboBrand = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        comboCategory = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        txtQty = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        txtItems = new javax.swing.JTextField();

        stockPopUp.setBackground(new java.awt.Color(102, 102, 102));
        stockPopUp.setForeground(new java.awt.Color(255, 255, 255));
        stockPopUp.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        stockPopUp.setOpaque(false);
        stockPopUp.setPreferredSize(new java.awt.Dimension(123, 25));

        itemHistory.setBackground(new java.awt.Color(102, 102, 102));
        itemHistory.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        itemHistory.setForeground(new java.awt.Color(255, 255, 255));
        itemHistory.setText("Item History");
        itemHistory.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        itemHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemHistoryActionPerformed(evt);
            }
        });
        stockPopUp.add(itemHistory);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Stock");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblStock.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM ID", "PART NO", "DESCRIPTION", "QTY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblStock.setFocusable(false);
        tblStock.setRowHeight(35);
        tblStock.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblStock.setShowVerticalLines(false);
        tblStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblStockMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblStockMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblStock);
        if (tblStock.getColumnModel().getColumnCount() > 0) {
            tblStock.getColumnModel().getColumn(0).setMinWidth(0);
            tblStock.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblStock.getColumnModel().getColumn(0).setMaxWidth(0);
            tblStock.getColumnModel().getColumn(1).setMinWidth(0);
            tblStock.getColumnModel().getColumn(1).setPreferredWidth(0);
            tblStock.getColumnModel().getColumn(1).setMaxWidth(0);
            tblStock.getColumnModel().getColumn(2).setPreferredWidth(150);
            tblStock.getColumnModel().getColumn(3).setPreferredWidth(350);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 101, 700, 420));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 60, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 60, 200, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 80, 200, 20));

        btnLowStock.setBackground(new java.awt.Color(255, 255, 255));
        btnLowStock.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnLowStock.setForeground(new java.awt.Color(0, 153, 153));
        btnLowStock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnLowStock.setText("Low Stock");
        btnLowStock.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnLowStock.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnLowStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLowStockMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLowStockMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLowStockMouseExited(evt);
            }
        });
        jPanel3.add(btnLowStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 540, 110, 25));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Brand : *");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 70, -1));

        comboBrand.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboBrand.setForeground(new java.awt.Color(0, 153, 153));
        comboBrand.setBorder(null);
        comboBrand.setOpaque(false);
        comboBrand.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboBrandPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboBrand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBrandActionPerformed(evt);
            }
        });
        jPanel3.add(comboBrand, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 200, 25));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 153));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Category : *");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 40, 70, -1));

        comboCategory.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        comboCategory.setForeground(new java.awt.Color(0, 153, 153));
        comboCategory.setBorder(null);
        comboCategory.setOpaque(false);
        comboCategory.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                comboCategoryPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        comboCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategoryActionPerformed(evt);
            }
        });
        jPanel3.add(comboCategory, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 60, 200, 25));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Qty :");
        jPanel3.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 540, -1, -1));

        txtQty.setEditable(false);
        txtQty.setBackground(new java.awt.Color(255, 255, 255));
        txtQty.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtQty.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtQty.setText("0");
        txtQty.setBorder(null);
        jPanel3.add(txtQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 540, 60, 20));
        jPanel3.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 560, 60, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Items :");
        jPanel3.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 540, -1, -1));
        jPanel3.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 560, 60, 10));

        txtItems.setEditable(false);
        txtItems.setBackground(new java.awt.Color(255, 255, 255));
        txtItems.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItems.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItems.setText("0");
        txtItems.setBorder(null);
        jPanel3.add(txtItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 540, 60, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblStockMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblStockMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblStock.getSelectedRowCount() != 0) {
                stockPopUp.show(evt.getComponent(), evt.getX(), evt.getY());
            }

            // }
        }
    }//GEN-LAST:event_tblStockMouseReleased

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            comboBrand.setSelectedIndex(0);
            comboCategory.setSelectedIndex(0);

            ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE (brandName LIKE '%' '" + txtSearch.getText() + "' '%' OR categoryName LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsPartNo LIKE '%' '" + txtSearch.getText() + "' '%' OR itemsName LIKE '%' '" + txtSearch.getText() + "' '%') ORDER BY brandName ASC ");

            DefaultTableModel dtm = (DefaultTableModel) tblStock.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idstock"),
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo").toUpperCase(),
                    rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                    rs.getString("qty").toLowerCase()
                });
            }

            calculation();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void itemHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemHistoryActionPerformed
        new StockItemHistory().setVisible(true);
    }//GEN-LAST:event_itemHistoryActionPerformed

    private void tblStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblStockMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblStockMouseClicked

    private void btnLowStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLowStockMouseClicked
        new LowStock().setVisible(true);
    }//GEN-LAST:event_btnLowStockMouseClicked

    private void btnLowStockMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLowStockMouseEntered
        btnLowStock.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnLowStockMouseEntered

    private void btnLowStockMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLowStockMouseExited
        btnLowStock.setBackground(Color.white);
    }//GEN-LAST:event_btnLowStockMouseExited

    private void comboBrandPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboBrandPopupMenuWillBecomeInvisible
        try {
            txtSearch.setText("");
            Integer brandId = null;
            Integer catId = null;

            DefaultTableModel dtm = (DefaultTableModel) tblStock.getModel();
            dtm.setRowCount(0);

            brandId = bra.get(comboBrand.getSelectedItem());
            catId = cat.get(comboCategory.getSelectedItem());

            if (comboBrand.getSelectedIndex() != 0) {
                if (comboCategory.getSelectedIndex() != 0) {

                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idbrands = '" + brandId + "' AND i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                } else {
                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idbrands = '" + brandId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                }
            } else {
                if (comboCategory.getSelectedIndex() != 0) {
                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                } else {
                    loadStock();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboBrandPopupMenuWillBecomeInvisible

    private void comboBrandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBrandActionPerformed

    }//GEN-LAST:event_comboBrandActionPerformed

    private void comboCategoryPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_comboCategoryPopupMenuWillBecomeInvisible
        try {
            txtSearch.setText("");
            Integer brandId = null;
            Integer catId = null;

            DefaultTableModel dtm = (DefaultTableModel) tblStock.getModel();
            dtm.setRowCount(0);

            brandId = bra.get(comboBrand.getSelectedItem());
            catId = cat.get(comboCategory.getSelectedItem());

            if (comboCategory.getSelectedIndex() != 0) {
                if (comboBrand.getSelectedIndex() != 0) {

                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idbrands = '" + brandId + "' AND i.idcategory = '" + catId + "'  ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                } else {
                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idcategory = '" + catId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                }
            } else {
                if (comboBrand.getSelectedIndex() != 0) {
                    ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.idbrands = '" + brandId + "' ORDER BY brandName ASC");

                    while (rs.next()) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idstock"),
                            rs.getInt("iditems"),
                            rs.getString("itemsPartNo").toUpperCase(),
                            rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                            rs.getString("qty").toLowerCase()
                        });
                    }

                    calculation();

                } else {
                    loadStock();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_comboCategoryPopupMenuWillBecomeInvisible

    private void comboCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategoryActionPerformed

    }//GEN-LAST:event_comboCategoryActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Stock().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnLowStock;
    public static javax.swing.JComboBox<String> comboBrand;
    public static javax.swing.JComboBox<String> comboCategory;
    private javax.swing.JMenuItem itemHistory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lbClose;
    private javax.swing.JPopupMenu stockPopUp;
    public static javax.swing.JTable tblStock;
    public static javax.swing.JTextField txtItems;
    public static javax.swing.JTextField txtQty;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnLowStock.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnLowStock.setBackground(Color.white);
        comboBrand.setBackground(Color.white);
        comboCategory.setBackground(Color.white);
    }

    private void DesignTable() {
        tblStock.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblStock.getTableHeader().setOpaque(false);
        tblStock.getTableHeader().setBackground(Color.white);
        tblStock.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblStock.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
    }

    public static void loadStock() {
        try {
            ResultSet rs = DB.search("SELECT s.*, i.*, b.*, c.* FROM stock s INNER JOIN items i ON s.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE i.isActive = 1 ORDER BY brandName ASC ");

            DefaultTableModel dtm = (DefaultTableModel) tblStock.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idstock"),
                    rs.getInt("iditems"),
                    rs.getString("itemsPartNo").toUpperCase(),
                    rs.getString("brandName").toUpperCase() + "  " + rs.getString("categoryName").toUpperCase() + " " + rs.getString("itemsName").toUpperCase(),
                    rs.getString("qty").toLowerCase()});
            }

            calculation();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadBrands() {
        try {
            ResultSet rs = DB.search("SELECT * FROM brands");
            comboBrand.removeAllItems();
            comboBrand.addItem("");
            while (rs.next()) {
                comboBrand.addItem(rs.getString("brandName"));
                bra.put(rs.getString("brandName"), rs.getInt("idbrands"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCategory() {
        try {
            ResultSet rs = DB.search("SELECT * FROM category");
            comboCategory.removeAllItems();
            comboCategory.addItem("");
            while (rs.next()) {
                comboCategory.addItem(rs.getString("categoryName"));
                cat.put(rs.getString("categoryName"), rs.getInt("idcategory"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculation() {
        DefaultTableModel dtm = (DefaultTableModel) tblStock.getModel();

        if (dtm.getRowCount() != 0) {
            txtItems.setText(String.valueOf(dtm.getRowCount()));

            Double qty = 0.0;

            for (int i = 0; i < dtm.getRowCount(); i++) {
                qty += Double.parseDouble(dtm.getValueAt(i, 4).toString());
            }

            txtQty.setText("" + qty);

        } else {
            txtItems.setText("0");
            txtQty.setText("0");
        }

    }

}
