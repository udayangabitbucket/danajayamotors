package Supplier;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import Items.AddItems;
import static MainMenu.Login.UserPrivilage;
import static Supplier.ViewGrnSupp.loadGrn;
import static Supplier.ViewGrnSupp.tblGrnSupp;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class OutstandViaSupp extends javax.swing.JFrame {

    boolean TXT_VALIDATE_1 = false;
    boolean TXT_VALIDATE_2 = false;

    String billNo;
    Integer grnId;

    public OutstandViaSupp() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOutstand = new javax.swing.JTable();
        lbGrn = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnSave = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        txtPiadTotal = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        txtArrears = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        lbChequeNo = new javax.swing.JLabel();
        lbSupplier = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Payments");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblOutstand.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblOutstand.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GRN ID", "DATE", "ADDITIONS", "DEDUCTIONS", "PAYMENT TYPE", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOutstand.setFocusable(false);
        tblOutstand.setRequestFocusEnabled(false);
        tblOutstand.setRowHeight(30);
        tblOutstand.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblOutstand.setShowVerticalLines(false);
        tblOutstand.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOutstandMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblOutstandMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblOutstand);
        if (tblOutstand.getColumnModel().getColumnCount() > 0) {
            tblOutstand.getColumnModel().getColumn(0).setMinWidth(0);
            tblOutstand.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblOutstand.getColumnModel().getColumn(0).setMaxWidth(0);
            tblOutstand.getColumnModel().getColumn(1).setPreferredWidth(50);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, 740, 230));

        lbGrn.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbGrn.setText("SET GRN NO HERE");
        jPanel3.add(lbGrn, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, 250, 20));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pay Outstanding", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 153, 153));
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSave.setText("PAY");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel4.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 190, 220, 30));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Pay : *");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 80, -1, -1));

        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setBorder(null);
        txtPaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPaidKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPaidKeyTyped(evt);
            }
        });
        jPanel4.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 80, 200, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 100, 200, 10));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date :*");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, -1, -1));

        dateChooser.setDateFormatString("yyyy-MM-dd");
        dateChooser.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel4.add(dateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 40, 150, 20));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, -1, -1));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Paid :");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Arrears :");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 50, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 30, 190, 20));
        jPanel2.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 190, 20));

        txtPiadTotal.setEditable(false);
        txtPiadTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtPiadTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPiadTotal.setText("0.00");
        txtPiadTotal.setBorder(null);
        jPanel2.add(txtPiadTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 190, 20));
        jPanel2.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, 190, 20));

        txtArrears.setEditable(false);
        txtArrears.setBackground(new java.awt.Color(255, 255, 255));
        txtArrears.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtArrears.setText("0.00");
        txtArrears.setBorder(null);
        txtArrears.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtArrearsKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtArrearsKeyTyped(evt);
            }
        });
        jPanel2.add(txtArrears, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, 190, 20));
        jPanel2.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 190, 10));

        jPanel4.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 320, 200));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 153, 153));
        jLabel17.setText("Payment Type : *");
        jPanel4.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 130, -1, -1));

        jCheckBox2.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jCheckBox2);
        jCheckBox2.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jCheckBox2.setForeground(new java.awt.Color(0, 153, 153));
        jCheckBox2.setSelected(true);
        jCheckBox2.setText("Cash");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });
        jPanel4.add(jCheckBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 130, -1, 20));

        jCheckBox1.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jCheckBox1);
        jCheckBox1.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(0, 153, 153));
        jCheckBox1.setText("Cheque");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel4.add(jCheckBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 130, -1, 20));

        lbChequeNo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbChequeNo.setText("set cheque no here");
        jPanel4.add(lbChequeNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 160, 230, -1));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 740, 270));

        lbSupplier.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lbSupplier.setText("SET SUPPLIER HERE");
        jPanel3.add(lbSupplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 250, 20));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblOutstandMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandMouseReleased

    }//GEN-LAST:event_tblOutstandMouseReleased

    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        savePaid();
    }//GEN-LAST:event_btnSaveMouseClicked

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.white);
    }//GEN-LAST:event_btnSaveMouseExited

    private void tblOutstandMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOutstandMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblOutstandMouseClicked

    private void txtArrearsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyTyped
        char c = evt.getKeyChar();
        if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }//GEN-LAST:event_txtArrearsKeyTyped

    private void txtArrearsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtArrearsKeyReleased

    }//GEN-LAST:event_txtArrearsKeyReleased

    private void txtPaidKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            savePaid();
        }
    }//GEN-LAST:event_txtPaidKeyReleased

    private void txtPaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaidKeyTyped
        char c = evt.getKeyChar();
         if(Character.isLetter(c)&&!evt.isAltDown()){
            evt.consume();
        }
    }//GEN-LAST:event_txtPaidKeyTyped

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OutstandViaSupp().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbChequeNo;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbGrn;
    private javax.swing.JLabel lbSupplier;
    public static javax.swing.JTable tblOutstand;
    private javax.swing.JTextField txtArrears;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtPiadTotal;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnSave.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnSave.setBackground(Color.white);
    }

    private void DesignTable() {
        tblOutstand.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 12));
        tblOutstand.getTableHeader().setOpaque(false);
        tblOutstand.getTableHeader().setBackground(Color.white);
        tblOutstand.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblOutstand.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblOutstand.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
    }

    private void loadDetails() {
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            DefaultTableModel dtm_1 = (DefaultTableModel) tblGrnSupp.getModel();
            int selectedRow = tblGrnSupp.getSelectedRow();

            DefaultTableModel dtm_2 = (DefaultTableModel) tblOutstand.getModel();
            dtm_2.setRowCount(0);

            dateChooser.setDate(new Date());
            grnId = Integer.parseInt(dtm_1.getValueAt(selectedRow, 0).toString());
            billNo = dtm_1.getValueAt(selectedRow, 3).toString();
            lbGrn.setText(dtm_1.getValueAt(0, 2).toString());
            txtTotal.setText(Decimal_Formats.Price(Double.parseDouble(dtm_1.getValueAt(selectedRow, 5).toString())));
            txtPiadTotal.setText(Decimal_Formats.Price(Double.parseDouble(dtm_1.getValueAt(selectedRow, 6).toString())));

            ResultSet rs = DB.search("SELECT * FROM grnoustanding WHERE idGrn = '" + grnId + "' ORDER BY outstandDate DESC ");

            while (rs.next()) {
                dtm_2.addRow(new Object[]{
                    rs.getInt("idGrn"),
                    rs.getString("outstandDate"),
                    Decimal_Formats.Price(rs.getDouble("addition")),
                    Decimal_Formats.Price(rs.getDouble("deduction")),
                    rs.getString("status")
                });
            }

            txtArrears.setText(Decimal_Formats.Price(Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtPiadTotal.getText())));

            txtPaid.grabFocus();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void savePaid() {
        try {

            if (!txtPaid.getText().isEmpty()) {
                if (!(Double.parseDouble(txtArrears.getText()) < Double.parseDouble(txtPaid.getText()))) {

                    if (Double.parseDouble(txtPaid.getText()) != 0) {
                        DefaultTableModel dtm = (DefaultTableModel) tblOutstand.getModel();

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(dateChooser.getDate())));

                        String insertOutstand = "INSERT INTO grnoustanding(outstandDate, deduction, status, idGrn)"
                                + "VALUES ('" + date + "', '" + Double.parseDouble(txtPaid.getText()) + "', 'outstanding payment', '" + grnId + "')";

                        DB.idu(insertOutstand);

                        Double paid = null;

                        ResultSet rs = DB.search("SELECT * FROM grnpayment WHERE idGrn = '" + grnId + "' ");

                        if (rs.next()) {
                            paid = rs.getDouble("paid");
                        }

                        Double newPaid = paid + Double.parseDouble(txtPaid.getText());

                        String updatePayment = "UPDATE grnpayment SET paid = '" + newPaid + "' WHERE idGrn = '" + grnId + "' ";
                        DB.idu(updatePayment);

                        String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + 0.0 + "', '" + Double.parseDouble(txtPaid.getText()) + "', '(" + billNo + ") ' 'supplier outstanding amount' ) ";
                        DB.idu(insertCashbook);

                        ResultSet rs_1 = DB.search("SELECT gp.*, go.* FROM grnpayment gp INNER JOIN grnoustanding go ON gp.idGrn = go.idGrn WHERE go.idGrn = '" + grnId + "' ORDER BY go.outstandDate DESC  ");
                        dtm.setRowCount(0);
                        while (rs_1.next()) {
                            dtm.addRow(new Object[]{
                                rs_1.getInt("idGrn"),
                                rs_1.getString("outstandDate"),
                                Decimal_Formats.Price(rs_1.getDouble("addition")),
                                Decimal_Formats.Price(rs_1.getDouble("deduction")),
                                rs_1.getString("status")
                            });

                            txtTotal.setText(Decimal_Formats.Price(rs_1.getDouble("total")));
                            txtPiadTotal.setText(Decimal_Formats.Price(rs_1.getDouble("paid")));

                            txtArrears.setText(Decimal_Formats.Price(rs_1.getDouble("total") - rs_1.getDouble("paid")));
                        }

                        txtPaid.setText("");
                        txtPaid.grabFocus();
                        loadGrn();
                        NotificationPopup.save();
                    }

                } else {
                    NotificationPopup.incorrectPayment();
                }
            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
