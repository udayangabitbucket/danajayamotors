package Supplier;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Supplier extends javax.swing.JFrame {

    public Supplier() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadSupplier();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        addGrn = new javax.swing.JMenuItem();
        grnHistory = new javax.swing.JMenuItem();
        editPopup = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAddSupplier = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSupplier = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        btnGrnHistory = new javax.swing.JLabel();

        jPopupMenu1.setBackground(new java.awt.Color(102, 102, 102));
        jPopupMenu1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPopupMenu1.setPreferredSize(new java.awt.Dimension(100, 75));

        addGrn.setBackground(new java.awt.Color(102, 102, 102));
        addGrn.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        addGrn.setForeground(new java.awt.Color(255, 255, 255));
        addGrn.setText("Add Grn");
        addGrn.setBorder(null);
        addGrn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addGrnActionPerformed(evt);
            }
        });
        jPopupMenu1.add(addGrn);

        grnHistory.setBackground(new java.awt.Color(102, 102, 102));
        grnHistory.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        grnHistory.setForeground(new java.awt.Color(255, 255, 255));
        grnHistory.setText("Grn history");
        grnHistory.setBorder(null);
        grnHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grnHistoryActionPerformed(evt);
            }
        });
        jPopupMenu1.add(grnHistory);

        editPopup.setBackground(new java.awt.Color(102, 102, 102));
        editPopup.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        editPopup.setForeground(new java.awt.Color(255, 255, 255));
        editPopup.setText("Edit");
        editPopup.setBorder(null);
        editPopup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editPopupActionPerformed(evt);
            }
        });
        jPopupMenu1.add(editPopup);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Supplier");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddSupplier.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddSupplier.setForeground(new java.awt.Color(255, 255, 255));
        btnAddSupplier.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddSupplier.setText("+");
        btnAddSupplier.setToolTipText("Add user");
        btnAddSupplier.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddSupplier.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddSupplierMouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddSupplierMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddSupplierMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 30, 30));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblSupplier.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblSupplier.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NAME", "CONTACT NO", "ADDRESS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSupplier.setFocusable(false);
        tblSupplier.setRowHeight(35);
        tblSupplier.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblSupplier.setShowVerticalLines(false);
        tblSupplier.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblSupplierMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblSupplier);
        if (tblSupplier.getColumnModel().getColumnCount() > 0) {
            tblSupplier.getColumnModel().getColumn(0).setMinWidth(0);
            tblSupplier.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblSupplier.getColumnModel().getColumn(0).setMaxWidth(0);
            tblSupplier.getColumnModel().getColumn(1).setPreferredWidth(250);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 700, 380));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 90, 20, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 90, 260, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 110, 260, 20));

        btnGrnHistory.setBackground(new java.awt.Color(255, 255, 255));
        btnGrnHistory.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnGrnHistory.setForeground(new java.awt.Color(0, 153, 153));
        btnGrnHistory.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnGrnHistory.setText("All GRN History");
        btnGrnHistory.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnGrnHistory.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnGrnHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGrnHistoryMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGrnHistoryMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGrnHistoryMouseExited(evt);
            }
        });
        jPanel3.add(btnGrnHistory, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 540, 130, 25));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void btnAddSupplierMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddSupplierMouseEntered
        btnAddSupplier.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddSupplierMouseEntered

    private void btnAddSupplierMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddSupplierMouseExited
        btnAddSupplier.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddSupplierMouseExited

    private void btnAddSupplierMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddSupplierMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new AddSupplier().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddSupplierMouseClicked

    private void tblSupplierMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSupplierMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblSupplier.getSelectedRowCount() != 0) {
                jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}

        }
    }//GEN-LAST:event_tblSupplierMouseReleased

    private void addGrnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addGrnActionPerformed
        new AddGrn().setVisible(true);
    }//GEN-LAST:event_addGrnActionPerformed

    private void grnHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grnHistoryActionPerformed
        new ViewGrnSupp().setVisible(true);
    }//GEN-LAST:event_grnHistoryActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            ResultSet rs = DB.search("SELECT * FROM supplier WHERE (supplierName LIKE '%' '" + txtSearch.getText() + "' '%' OR supplierContact_1 LIKE '%' '" + txtSearch.getText() + "' '%' OR supplierContact_2 LIKE '%' '" + txtSearch.getText() + "' '%')");

            DefaultTableModel dtm = (DefaultTableModel) tblSupplier.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idsupplier"),
                    rs.getString("supplierName"),
                    rs.getString("supplierContact_1") + "  " + rs.getString("supplierContact_2"),
                    rs.getString("supplierAddress")});
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void editPopupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editPopupActionPerformed
        new EditSupplier().setVisible(true);
    }//GEN-LAST:event_editPopupActionPerformed

    private void btnGrnHistoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGrnHistoryMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new ViewAllGrn().setVisible(true);
        //}
    }//GEN-LAST:event_btnGrnHistoryMouseClicked

    private void btnGrnHistoryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGrnHistoryMouseEntered
        btnGrnHistory.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnGrnHistoryMouseEntered

    private void btnGrnHistoryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGrnHistoryMouseExited
        btnGrnHistory.setBackground(Color.white);
    }//GEN-LAST:event_btnGrnHistoryMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Supplier().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addGrn;
    private javax.swing.JLabel btnAddSupplier;
    private javax.swing.JLabel btnGrnHistory;
    private javax.swing.JMenuItem editPopup;
    private javax.swing.JMenuItem grnHistory;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblSupplier;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnAddSupplier.setOpaque(true);
        btnGrnHistory.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnGrnHistory.setBackground(Color.white);
        btnAddSupplier.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblSupplier.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblSupplier.getTableHeader().setOpaque(false);
        tblSupplier.getTableHeader().setBackground(Color.white);
        tblSupplier.getTableHeader().setForeground(new Color(0, 153, 153));
    }

    public static void loadSupplier() {
        try {
            ResultSet rs = DB.search("SELECT * FROM supplier WHERE isActive = '1'");

            DefaultTableModel dtm = (DefaultTableModel) tblSupplier.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idsupplier"),
                    rs.getString("supplierName"),
                    rs.getString("supplierContact_1") + "  " + rs.getString("supplierContact_2"),
                    rs.getString("supplierAddress")});
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
