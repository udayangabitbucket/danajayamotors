package Supplier;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import static MainMenu.Login.UserPrivilage;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ViewAllGrn extends javax.swing.JFrame {
    
    public ViewAllGrn() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadAllGrn();
        txtSearch.grabFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        viewGrnItems = new javax.swing.JMenuItem();
        paidOutstanding = new javax.swing.JMenuItem();
        grnReturnAll = new javax.swing.JMenuItem();
        edit = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAllGrn = new javax.swing.JTable();
        dateSearch = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        dateChooserFrom = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        dateChooserTo = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        lbRefesh = new javax.swing.JLabel();
        checkOutstand = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtBalance = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();

        jPopupMenu1.setBackground(new java.awt.Color(102, 102, 102));
        jPopupMenu1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPopupMenu1.setPreferredSize(new java.awt.Dimension(120, 100));

        viewGrnItems.setBackground(new java.awt.Color(102, 102, 102));
        viewGrnItems.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        viewGrnItems.setForeground(new java.awt.Color(255, 255, 255));
        viewGrnItems.setText("View items");
        viewGrnItems.setBorder(null);
        viewGrnItems.setPreferredSize(new java.awt.Dimension(111, 8));
        viewGrnItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewGrnItemsActionPerformed(evt);
            }
        });
        jPopupMenu1.add(viewGrnItems);

        paidOutstanding.setBackground(new java.awt.Color(102, 102, 102));
        paidOutstanding.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        paidOutstanding.setForeground(new java.awt.Color(255, 255, 255));
        paidOutstanding.setText("Payment");
        paidOutstanding.setBorder(null);
        paidOutstanding.setPreferredSize(new java.awt.Dimension(111, 8));
        paidOutstanding.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paidOutstandingActionPerformed(evt);
            }
        });
        jPopupMenu1.add(paidOutstanding);

        grnReturnAll.setBackground(new java.awt.Color(102, 102, 102));
        grnReturnAll.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        grnReturnAll.setForeground(new java.awt.Color(255, 255, 255));
        grnReturnAll.setText("Return");
        grnReturnAll.setBorder(null);
        grnReturnAll.setPreferredSize(new java.awt.Dimension(111, 8));
        grnReturnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grnReturnAllActionPerformed(evt);
            }
        });
        jPopupMenu1.add(grnReturnAll);

        edit.setBackground(new java.awt.Color(102, 102, 102));
        edit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        edit.setForeground(new java.awt.Color(255, 255, 255));
        edit.setText("Edit");
        edit.setBorder(null);
        edit.setPreferredSize(new java.awt.Dimension(111, 8));
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });
        jPopupMenu1.add(edit);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Grn history - All");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 140, 26));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblAllGrn.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblAllGrn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GRN ID", "SUPPLIER", "BILL NO", "DATE", "TOTAL", "PAID", "TYPE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblAllGrn.setFocusable(false);
        tblAllGrn.setRowHeight(35);
        tblAllGrn.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblAllGrn.setShowVerticalLines(false);
        tblAllGrn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblAllGrnMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblAllGrn);
        if (tblAllGrn.getColumnModel().getColumnCount() > 0) {
            tblAllGrn.getColumnModel().getColumn(0).setMinWidth(0);
            tblAllGrn.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblAllGrn.getColumnModel().getColumn(0).setMaxWidth(0);
            tblAllGrn.getColumnModel().getColumn(1).setPreferredWidth(150);
            tblAllGrn.getColumnModel().getColumn(6).setPreferredWidth(100);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 700, 410));

        dateSearch.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        dateSearch.setForeground(new java.awt.Color(0, 153, 153));
        dateSearch.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        dateSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dateSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateSearchMouseClicked(evt);
            }
        });
        jPanel3.add(dateSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 80, 40, 20));

        txtSearch.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtSearch.setBorder(null);
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        jPanel3.add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 230, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 230, 10));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("From :");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 80, 40, -1));

        dateChooserFrom.setDateFormatString("yyyy-MM-dd");
        dateChooserFrom.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserFrom, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 80, 110, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("To :");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 80, 30, -1));

        dateChooserTo.setDateFormatString("yyyy-MM-dd");
        dateChooserTo.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jPanel3.add(dateChooserTo, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 80, 110, 20));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 153, 153));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Search_16x16.png"))); // NOI18N
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 20, 20));

        lbRefesh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRefesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Refresh_24x24.png"))); // NOI18N
        lbRefesh.setToolTipText("Refesh table");
        lbRefesh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbRefesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbRefeshMouseClicked(evt);
            }
        });
        jPanel3.add(lbRefesh, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 80, 30, 20));

        checkOutstand.setBackground(new java.awt.Color(255, 255, 255));
        checkOutstand.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        checkOutstand.setForeground(new java.awt.Color(0, 153, 153));
        checkOutstand.setText("Outstanding Only");
        checkOutstand.setOpaque(false);
        checkOutstand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkOutstandActionPerformed(evt);
            }
        });
        jPanel3.add(checkOutstand, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 560, 130, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 153, 153));
        jLabel13.setText("Total :");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 110, 20));
        jPanel2.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, 110, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Paid :");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, -1, -1));

        txtPaid.setEditable(false);
        txtPaid.setBackground(new java.awt.Color(255, 255, 255));
        txtPaid.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPaid.setText("0.00");
        txtPaid.setBorder(null);
        txtPaid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPaidActionPerformed(evt);
            }
        });
        jPanel2.add(txtPaid, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, 110, 20));
        jPanel2.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 40, 110, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Balance :");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, -1, -1));

        txtBalance.setEditable(false);
        txtBalance.setBackground(new java.awt.Color(255, 255, 255));
        txtBalance.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtBalance.setText("0.00");
        txtBalance.setBorder(null);
        txtBalance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceActionPerformed(evt);
            }
        });
        jPanel2.add(txtBalance, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 20, 110, 20));
        jPanel2.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, 110, 10));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 550, 570, -1));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblAllGrnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblAllGrnMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblAllGrn.getSelectedRowCount() != 0) {
                jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}

        }
    }//GEN-LAST:event_tblAllGrnMouseReleased

    private void viewGrnItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewGrnItemsActionPerformed
        new GrnItemAll().setVisible(true);
    }//GEN-LAST:event_viewGrnItemsActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            dateChooserFrom.setDate(null);
            dateChooserTo.setDate(null);

            ResultSet rs = DB.search("SELECT s.*, g.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnpayment gp ON gp.idGrn = g.idGrn WHERE supplierName LIKE '%' '" + txtSearch.getText() + "' '%' OR billNo LIKE '%' '" + txtSearch.getText() + "' '%' ORDER BY s.supplierName ASC");

            DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idGrn"),
                    rs.getString("supplierName"),
                    "GR-" + rs.getString("billNo"),
                    rs.getString("date"),
                    Decimal_Formats.Price(rs.getDouble("grnTotal")),
                    Decimal_Formats.Price(rs.getDouble("paid")),
                    rs.getString("paymentType")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_txtSearchKeyReleased

    private void dateSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateSearchMouseClicked
        dateSearch();
    }//GEN-LAST:event_dateSearchMouseClicked

    private void lbRefeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbRefeshMouseClicked
        txtSearch.setText(null);
        dateChooserFrom.setDate(null);
        dateChooserTo.setDate(null);
        checkOutstand.setSelected(false);
        DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();
        dtm.setRowCount(0);
        loadAllGrn();
    }//GEN-LAST:event_lbRefeshMouseClicked

    private void paidOutstandingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paidOutstandingActionPerformed
        new OutstandViaAll().setVisible(true);
    }//GEN-LAST:event_paidOutstandingActionPerformed

    private void grnReturnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grnReturnAllActionPerformed
        new ViewReturnAll().setVisible(true);
    }//GEN-LAST:event_grnReturnAllActionPerformed

    private void checkOutstandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkOutstandActionPerformed
        if (checkOutstand.isSelected()) {
            DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();

            if (dtm.getRowCount() != 0) {
                for (int i = 0; i < dtm.getRowCount(); i++) {
                    while (!(Double.parseDouble(dtm.getValueAt(i, 4).toString()) > Double.parseDouble(dtm.getValueAt(i, 5).toString()))) {
                        dtm.removeRow(i);
                    }
                }
                calculate();
            }
        } else {
            loadAllGrn();
        }
    }//GEN-LAST:event_checkOutstandActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPaidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPaidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPaidActionPerformed

    private void txtBalanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceActionPerformed

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
       new EditGrnAll().setVisible(true);
    }//GEN-LAST:event_editActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewAllGrn().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox checkOutstand;
    private com.toedter.calendar.JDateChooser dateChooserFrom;
    private com.toedter.calendar.JDateChooser dateChooserTo;
    private javax.swing.JLabel dateSearch;
    private javax.swing.JMenuItem edit;
    private javax.swing.JMenuItem grnReturnAll;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbRefesh;
    private javax.swing.JMenuItem paidOutstanding;
    public static javax.swing.JTable tblAllGrn;
    public static javax.swing.JTextField txtBalance;
    public static javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtSearch;
    public static javax.swing.JTextField txtTotal;
    private javax.swing.JMenuItem viewGrnItems;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
    }

    private void DesignTable() {
        tblAllGrn.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblAllGrn.getTableHeader().setOpaque(false);
        tblAllGrn.getTableHeader().setBackground(Color.white);
        tblAllGrn.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblAllGrn.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblAllGrn.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tblAllGrn.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
        tblAllGrn.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
        tblAllGrn.getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
    }

    public static void loadAllGrn() {
        try {
            ResultSet rs = DB.search("SELECT s.*, g.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnpayment gp ON gp.idGrn = g.idGrn WHERE g.isActive = '1' ORDER BY s.supplierName ASC");

            DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("idGrn"),
                    rs.getString("supplierName"),
                    "GR-" + rs.getString("billNo"),
                    rs.getString("date"),
                    Decimal_Formats.Price(rs.getDouble("grnTotal")),
                    Decimal_Formats.Price(rs.getDouble("paid")),
                    rs.getString("paymentType")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void dateSearch() {
        try {
            txtSearch.setText(null);

            if (dateChooserFrom.getDate() != null || dateChooserTo.getDate() != null) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();
                dtm.setRowCount(0);

                ResultSet rs = null;

                if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() != null) {

                    rs = DB.search("SELECT s.*, g.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnpayment gp ON gp.idGrn = g.idGrn WHERE g.date BETWEEN '" + df.format(dateChooserFrom.getDate()) + "' AND '" + df.format(dateChooserTo.getDate()) + "' AND g.isActive = 1 ORDER BY s.supplierName ASC  ");

                } else if (dateChooserFrom.getDate() != null && dateChooserTo.getDate() == null) {

                    rs = DB.search("SELECT s.*, g.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnpayment gp ON gp.idGrn = g.idGrn WHERE g.date = '" + df.format(dateChooserFrom.getDate()) + "' AND g.isActive = 1 ORDER BY s.supplierName ASC ");

                } else if (dateChooserTo.getDate() != null && dateChooserFrom.getDate() == null) {

                    rs = DB.search("SELECT s.*, g.*, gp.* FROM supplier s INNER JOIN grn g ON s.idsupplier = g.idsupplier INNER JOIN grnpayment gp ON gp.idGrn = g.idGrn WHERE g.date = '" + df.format(dateChooserTo.getDate()) + "' AND g.isActive = 1 ORDER BY s.supplierName ASC ");

                }

                while (rs.next()) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idGrn"),
                        rs.getString("supplierName"),
                        "GR-" + rs.getString("billNo"),
                        rs.getString("date"),
                        Decimal_Formats.Price(rs.getDouble("grnTotal")),
                        Decimal_Formats.Price(rs.getDouble("paid")),
                        rs.getString("paymentType")
                    });
                }

                calculate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculate() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblAllGrn.getModel();
            Double total = 0.0;
            Double paid = 0.0;
            Double balance = 0.0;

            for (int i = 0; i < dtm.getRowCount(); i++) {
                total += Double.parseDouble(dtm.getValueAt(i, 4).toString());
                paid += Double.parseDouble(dtm.getValueAt(i, 5).toString());
            }

            balance = total - paid;

            txtTotal.setText("" + Decimal_Formats.Price(total));
            txtPaid.setText("" + Decimal_Formats.Price(paid));
            txtBalance.setText("" + Decimal_Formats.Price(balance));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
