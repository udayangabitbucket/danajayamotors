package Supplier;

import User.*;
import MainMenu.*;
import ExClasses.DB;
import ExClasses.Decimal_Formats;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import static Supplier.ViewGrnSupp.loadGrn;
import static Supplier.ViewGrnSupp.tblGrnSupp;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ViewReturnGrnSupp extends javax.swing.JFrame {

    String itemID = null;
    Double stockQty = null;
    public static String billNo;
    public static String grnID;
    public static String supID;

    public ViewReturnGrnSupp() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        billNo = tblGrnSupp.getModel().getValueAt(tblGrnSupp.getSelectedRow(), 3).toString();
        grnID = tblGrnSupp.getModel().getValueAt(tblGrnSupp.getSelectedRow(), 0).toString();
        supID = tblGrnSupp.getModel().getValueAt(tblGrnSupp.getSelectedRow(), 1).toString();
        loadItems();
        getSupReturn();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        allGrnItem = new javax.swing.JPopupMenu();
        addRetrun = new javax.swing.JMenuItem();
        allRemove = new javax.swing.JPopupMenu();
        removeReturn = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblGrnItemSupp = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        txtItems = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSupReturn = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        txtItemsReturn = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        txtTotalReturn = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();

        allGrnItem.setBackground(new java.awt.Color(102, 102, 102));
        allGrnItem.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        allGrnItem.setPreferredSize(new java.awt.Dimension(117, 25));

        addRetrun.setBackground(new java.awt.Color(102, 102, 102));
        addRetrun.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        addRetrun.setForeground(new java.awt.Color(255, 255, 255));
        addRetrun.setText("Add Retrun");
        addRetrun.setBorder(null);
        addRetrun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRetrunActionPerformed(evt);
            }
        });
        allGrnItem.add(addRetrun);

        allRemove.setBackground(new java.awt.Color(102, 102, 102));
        allRemove.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        allRemove.setPreferredSize(new java.awt.Dimension(117, 25));

        removeReturn.setBackground(new java.awt.Color(102, 102, 102));
        removeReturn.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        removeReturn.setForeground(new java.awt.Color(255, 255, 255));
        removeReturn.setText("Remove");
        removeReturn.setBorder(null);
        removeReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeReturnActionPerformed(evt);
            }
        });
        allRemove.add(removeReturn);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Grn items");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 26));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grn Items", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane3.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblGrnItemSupp.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblGrnItemSupp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GRN ID", "ITEM ID", "PART NO", "DESCRIPTION", "UNIT PRICE", "QTY", "DISCOUNT", "ITEM PRICE", "AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblGrnItemSupp.setFocusable(false);
        tblGrnItemSupp.setRowHeight(30);
        tblGrnItemSupp.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblGrnItemSupp.setShowVerticalLines(false);
        tblGrnItemSupp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblGrnItemSuppMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tblGrnItemSupp);
        if (tblGrnItemSupp.getColumnModel().getColumnCount() > 0) {
            tblGrnItemSupp.getColumnModel().getColumn(0).setMinWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(0).setMaxWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(1).setMinWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(1).setPreferredWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(1).setMaxWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(2).setPreferredWidth(130);
            tblGrnItemSupp.getColumnModel().getColumn(3).setPreferredWidth(180);
            tblGrnItemSupp.getColumnModel().getColumn(7).setMinWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(7).setPreferredWidth(0);
            tblGrnItemSupp.getColumnModel().getColumn(7).setMaxWidth(0);
        }

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 700, 220));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 153, 153));
        jLabel11.setText("Items :");
        jPanel2.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 280, -1, -1));

        txtItems.setEditable(false);
        txtItems.setBackground(new java.awt.Color(255, 255, 255));
        txtItems.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItems.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItems.setText("0");
        txtItems.setBorder(null);
        jPanel2.add(txtItems, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 280, 50, 20));
        jPanel2.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 300, 50, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 153));
        jLabel12.setText("Total :");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 280, -1, -1));

        txtTotal.setEditable(false);
        txtTotal.setBackground(new java.awt.Color(255, 255, 255));
        txtTotal.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotal.setText("0.00");
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 280, 180, 20));
        jPanel2.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 300, 180, 10));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 740, 330));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Return Items", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 14))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblSupReturn.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblSupReturn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "GRN ID", "ITEM ID", "DATE", "PART NO", "DESCRIPTION", "RETURN PRICE", "QTY", "AMOUNT", "REASON"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSupReturn.setFocusable(false);
        tblSupReturn.setRowHeight(30);
        tblSupReturn.setSelectionBackground(new java.awt.Color(204, 204, 204));
        tblSupReturn.setShowVerticalLines(false);
        tblSupReturn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblSupReturnMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tblSupReturn);
        if (tblSupReturn.getColumnModel().getColumnCount() > 0) {
            tblSupReturn.getColumnModel().getColumn(0).setMinWidth(0);
            tblSupReturn.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblSupReturn.getColumnModel().getColumn(0).setMaxWidth(0);
            tblSupReturn.getColumnModel().getColumn(1).setMinWidth(0);
            tblSupReturn.getColumnModel().getColumn(1).setPreferredWidth(0);
            tblSupReturn.getColumnModel().getColumn(1).setMaxWidth(0);
            tblSupReturn.getColumnModel().getColumn(4).setPreferredWidth(180);
        }

        jPanel4.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 700, 200));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 153));
        jLabel15.setText("Items :");
        jPanel4.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 250, -1, -1));

        txtItemsReturn.setEditable(false);
        txtItemsReturn.setBackground(new java.awt.Color(255, 255, 255));
        txtItemsReturn.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtItemsReturn.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtItemsReturn.setText("0");
        txtItemsReturn.setBorder(null);
        jPanel4.add(txtItemsReturn, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 250, 50, 20));
        jPanel4.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 270, 50, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 153, 153));
        jLabel14.setText("Total :");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 250, -1, -1));

        txtTotalReturn.setEditable(false);
        txtTotalReturn.setBackground(new java.awt.Color(255, 255, 255));
        txtTotalReturn.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtTotalReturn.setText("0.00");
        txtTotalReturn.setBorder(null);
        txtTotalReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalReturnActionPerformed(evt);
            }
        });
        jPanel4.add(txtTotalReturn, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 250, 180, 20));
        jPanel4.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 270, 180, 10));

        jPanel3.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 390, 740, 290));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void tblSupReturnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSupReturnMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblSupReturn.getSelectedRowCount() != 0) {
                allRemove.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}
        }
    }//GEN-LAST:event_tblSupReturnMouseReleased

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void tblGrnItemSuppMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGrnItemSuppMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            //if (UserPrivilage.equals("admin")) {
            if (evt.isPopupTrigger() && tblGrnItemSupp.getSelectedRowCount() != 0) {
                allGrnItem.show(evt.getComponent(), evt.getX(), evt.getY());
            }
            //}
        }
    }//GEN-LAST:event_tblGrnItemSuppMouseReleased

    private void txtTotalReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalReturnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalReturnActionPerformed

    private void addRetrunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRetrunActionPerformed
        new AddReturnSupp().setVisible(true);
    }//GEN-LAST:event_addRetrunActionPerformed

    private void removeReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeReturnActionPerformed
        try {
            int response = JOptionPane.showConfirmDialog(this, "Are you sure remove this item ?", "Remove items", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            DefaultTableModel dtm = (DefaultTableModel) tblSupReturn.getModel();
            int selectedRow = tblSupReturn.getSelectedRow();

            grnID = dtm.getValueAt(selectedRow, 0).toString();
            itemID = dtm.getValueAt(selectedRow, 1).toString();

            if (response == JOptionPane.YES_OPTION) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date date = java.sql.Date.valueOf(LocalDate.parse((CharSequence) df.format(new Date())));

                Double total = Double.parseDouble(dtm.getValueAt(selectedRow, 7).toString());

                if (total != 0) {
                    ResultSet grn = DB.search("SELECT * FROM grn WHERE idGrn = '" + grnID + "' ");

                    if (grn.next()) {

                        String grnTotal = "UPDATE grn SET grnTotal = '" + (grn.getDouble("grnTotal") + total) + "' WHERE idGrn = '" + grnID + "' ";
                        DB.idu(grnTotal);
                    }

                    ResultSet grnItem = DB.search("SELECT * FROM grnitems WHERE idGrn = '" + grnID + "' AND iditems = '" + itemID + "' ");

                    if (grnItem.next()) {

                        String grnItemTotal = "UPDATE grnitems SET totalAmount = '" + (grnItem.getDouble("totalAmount") + total) + "' WHERE idGrn = '" + grnID + "' AND iditems = '" + itemID + "' ";
                        DB.idu(grnItemTotal);
                    }

                    ResultSet grnPayment = DB.search("SELECT * FROM grnpayment WHERE idGrn = '" + grnID + "' ");

                    if (grnPayment.next()) {

                        String paymentTotal = "UPDATE grnpayment SET total = '" + (grnPayment.getDouble("total") + total) + "' WHERE idGrn = '" + grnID + "' ";
                        DB.idu(paymentTotal);

                    }

                    String insertOutstand = "INSERT INTO grnoustanding(outstandDate, addition, status, idGrn)"
                            + "VALUES ('" + date + "', '" + total + "', '(" + dtm.getValueAt(selectedRow, 3).toString() + ") ' 'cancel return', '" + grnID + "')";

                    DB.idu(insertOutstand);

                    String insertCashbook = "INSERT INTO cashbook (date, income, expence, description) VALUES ('" + date + "', '" + 0.0 + "', '" + total + "', '" + dtm.getValueAt(selectedRow, 3).toString() + "('  '" + billNo + ")' ' cancel return ' ) ";
                    DB.idu(insertCashbook);
                }

                String deleteReturn = "DELETE FROM returngrn WHERE idGrn = '" + grnID + "' AND iditems = '" + itemID + "' ";
                DB.idu(deleteReturn);

                ResultSet grnQty = DB.search("SELECT * FROM grnitems WHERE idGrn = '" + grnID + "' AND iditems = '" + itemID + "' ");

                if (grnQty.next()) {
                    String updateGrnItem = "UPDATE grnitems SET qty = '" + (grnQty.getDouble("qty") + Double.parseDouble(dtm.getValueAt(selectedRow, 6).toString())) + "' WHERE idGrn = '" + grnID + "' AND iditems = '" + itemID + "' ";
                    DB.idu(updateGrnItem);
                }

                ResultSet stockQty = DB.search("SELECT * FROM stock WHERE iditems = '" + itemID + "' ");

                if (stockQty.next()) {
                    String updateStock = "UPDATE stock SET qty = '" + (stockQty.getDouble("qty") + Double.parseDouble(dtm.getValueAt(selectedRow, 6).toString())) + "' WHERE iditems = '" + itemID + "' ";
                    DB.idu(updateStock);
                }

                loadItems();
                getSupReturn();
                loadGrn();

                NotificationPopup.delete();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_removeReturnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewReturnGrnSupp().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addRetrun;
    private javax.swing.JPopupMenu allGrnItem;
    private javax.swing.JPopupMenu allRemove;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbClose;
    private javax.swing.JMenuItem removeReturn;
    public static javax.swing.JTable tblGrnItemSupp;
    public static javax.swing.JTable tblSupReturn;
    public static javax.swing.JTextField txtItems;
    public static javax.swing.JTextField txtItemsReturn;
    public static javax.swing.JTextField txtTotal;
    public static javax.swing.JTextField txtTotalReturn;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
    }

    private void DesignTable() {
        tblGrnItemSupp.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblGrnItemSupp.getTableHeader().setOpaque(false);
        tblGrnItemSupp.getTableHeader().setBackground(Color.white);
        tblGrnItemSupp.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer_1 = new DefaultTableCellRenderer();
        rightRenderer_1.setHorizontalAlignment(JLabel.CENTER);
        tblGrnItemSupp.getColumnModel().getColumn(4).setCellRenderer(rightRenderer_1);
        tblGrnItemSupp.getColumnModel().getColumn(5).setCellRenderer(rightRenderer_1);
        tblGrnItemSupp.getColumnModel().getColumn(6).setCellRenderer(rightRenderer_1);
        tblGrnItemSupp.getColumnModel().getColumn(8).setCellRenderer(rightRenderer_1);

        tblSupReturn.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblSupReturn.getTableHeader().setOpaque(false);
        tblSupReturn.getTableHeader().setBackground(Color.white);
        tblSupReturn.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer_2 = new DefaultTableCellRenderer();
        rightRenderer_2.setHorizontalAlignment(JLabel.CENTER);
        tblSupReturn.getColumnModel().getColumn(5).setCellRenderer(rightRenderer_2);
        tblSupReturn.getColumnModel().getColumn(6).setCellRenderer(rightRenderer_2);
        tblSupReturn.getColumnModel().getColumn(7).setCellRenderer(rightRenderer_2);
        tblSupReturn.getColumnModel().getColumn(8).setCellRenderer(rightRenderer_2);
    }

    public static void loadItems() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblGrnItemSupp.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT g.*,gi.*,i.*,b.*,c.* FROM grn g INNER JOIN grnitems gi ON g.idGrn = gi.idGrn INNER JOIN items i ON i.iditems = gi.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE gi.idGrn = '" + grnID + "' AND idsupplier = '" + supID + "' ");

            while (rs.next()) {

                Double itemPrice = (rs.getDouble("unitPrice") * 1.0) / 100 * (100 - rs.getDouble("discount"));

                dtm.addRow(new Object[]{
                    rs.getString("idGrn"),
                    rs.getString("iditems"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase(),
                    Decimal_Formats.Price((rs.getDouble("unitPrice"))),
                    rs.getDouble("qty"),
                    rs.getDouble("discount") + " %",
                    Decimal_Formats.Price(itemPrice),
                    Decimal_Formats.Price((rs.getDouble("totalAmount")))
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getSupReturn() {
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblSupReturn.getModel();
            dtm.setRowCount(0);

            ResultSet rs = DB.search("SELECT rg.*,i.*,b.*,c.* FROM returngrn rg INNER JOIN items i ON rg.iditems = i.iditems INNER JOIN brands b ON b.idbrands = i.idbrands INNER JOIN category c ON c.idcategory = i.idcategory WHERE rg.idGrn = '" + grnID + "' ");

            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getString("idGrn"),
                    rs.getString("iditems"),
                    rs.getString("returnDate"),
                    rs.getString("itemsPartNo"),
                    rs.getString("brandName").toLowerCase() + " " + rs.getString("categoryName").toLowerCase() + " " + rs.getString("itemsName").toLowerCase(),
                    Decimal_Formats.Price((rs.getDouble("returnPrice"))),
                    rs.getDouble("qty"),
                    Decimal_Formats.Price((rs.getDouble("returnTotal"))),
                    rs.getString("reason")
                });
            }

            calculate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void calculate() {
        try {
            DefaultTableModel dtm_1 = (DefaultTableModel) tblGrnItemSupp.getModel();
            Double total_1 = 0.0;

            for (int i = 0; i < dtm_1.getRowCount(); i++) {
                total_1 += Double.parseDouble(dtm_1.getValueAt(i, 8).toString());
            }

            txtTotal.setText("" + Decimal_Formats.Price(total_1));
            txtItems.setText(String.valueOf(dtm_1.getRowCount()));

            DefaultTableModel dtm_2 = (DefaultTableModel) tblSupReturn.getModel();
            Double total_2 = 0.0;

            for (int i = 0; i < dtm_2.getRowCount(); i++) {
                total_2 += Double.parseDouble(dtm_2.getValueAt(i, 7).toString());
            }

            txtTotalReturn.setText("" + Decimal_Formats.Price(total_2));
            txtItemsReturn.setText(String.valueOf(dtm_2.getRowCount()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
