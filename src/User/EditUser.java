package User;

import MainMenu.*;
import ExClasses.DB;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserName;
import static User.User.loadUsers;
import static User.User.tblUser;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

public class EditUser extends javax.swing.JFrame {

    boolean TXT_VALIDATE = false;
    boolean USER_VALIDATE;
    String USERNAME;
    Integer USERID;

    public EditUser() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        getDetails();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        txtConPassword = new javax.swing.JPasswordField();
        jSeparator3 = new javax.swing.JSeparator();
        btnUpdate = new javax.swing.JLabel();
        checkBox2 = new javax.swing.JCheckBox();
        checkBox1 = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        lbShow = new javax.swing.JLabel();
        lbConfirm = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Change User");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, 26));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 153, 153));
        jLabel2.setText("Username : *");
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 180, -1, -1));

        txtUsername.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtUsername.setBorder(null);
        txtUsername.setNextFocusableComponent(txtPassword);
        txtUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsernameActionPerformed(evt);
            }
        });
        txtUsername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsernameKeyPressed(evt);
            }
        });
        jPanel3.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 180, 290, 20));
        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 200, 290, 20));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setText("Confirm : *");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 320, 70, -1));

        txtPassword.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtPassword.setBorder(null);
        txtPassword.setNextFocusableComponent(txtConPassword);
        txtPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPasswordKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPasswordKeyTyped(evt);
            }
        });
        jPanel3.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 250, 290, 20));
        jPanel3.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 270, 290, 20));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setText("Password : *");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, -1, -1));

        txtConPassword.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtConPassword.setBorder(null);
        txtConPassword.setNextFocusableComponent(txtUsername);
        txtConPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtConPasswordKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtConPasswordKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtConPasswordKeyTyped(evt);
            }
        });
        jPanel3.add(txtConPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 320, 290, 20));
        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 340, 290, 20));

        btnUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnUpdate.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(0, 153, 153));
        btnUpdate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnUpdate.setText("UPDATE");
        btnUpdate.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 153)));
        btnUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUpdateMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdateMouseExited(evt);
            }
        });
        jPanel3.add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 380, 120, 40));

        checkBox2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        checkBox2.setOpaque(false);
        checkBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBox2ActionPerformed(evt);
            }
        });
        jPanel3.add(checkBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 320, -1, -1));

        checkBox1.setBackground(new java.awt.Color(255, 255, 255));
        checkBox1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        checkBox1.setOpaque(false);
        checkBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBox1ActionPerformed(evt);
            }
        });
        jPanel3.add(checkBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 250, -1, -1));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Name : *");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 110, -1, -1));

        txtName.setFont(new java.awt.Font("Times New Roman", 0, 15)); // NOI18N
        txtName.setBorder(null);
        txtName.setNextFocusableComponent(txtPassword);
        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });
        txtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNameKeyPressed(evt);
            }
        });
        jPanel3.add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 290, 20));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 130, 290, 20));

        lbShow.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        lbShow.setForeground(new java.awt.Color(153, 153, 153));
        lbShow.setText("Show password");
        jPanel3.add(lbShow, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 250, -1, -1));

        lbConfirm.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        lbConfirm.setForeground(new java.awt.Color(153, 153, 153));
        lbConfirm.setText("Show password");
        jPanel3.add(lbConfirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 320, -1, -1));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void txtUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUsernameActionPerformed

    private void txtUsernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsernameKeyPressed

    }//GEN-LAST:event_txtUsernameKeyPressed

    private void txtPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswordKeyPressed

    }//GEN-LAST:event_txtPasswordKeyPressed

    private void txtPasswordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswordKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPasswordKeyTyped

    private void txtConPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConPasswordKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtConPasswordKeyPressed

    private void txtConPasswordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConPasswordKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtConPasswordKeyTyped

    private void btnUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseEntered
        btnUpdate.setBackground(new Color(204, 204, 204));
    }//GEN-LAST:event_btnUpdateMouseEntered

    private void btnUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseExited
        btnUpdate.setBackground(Color.white);
    }//GEN-LAST:event_btnUpdateMouseExited

    private void checkBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBox1ActionPerformed
        if (checkBox1.isSelected()) {
            txtPassword.setEchoChar((char) 0);
            lbShow.setText("Hide password");
        } else {
            txtPassword.setEchoChar('*');
            lbShow.setText("Show password");
        }
    }//GEN-LAST:event_checkBox1ActionPerformed

    private void checkBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBox2ActionPerformed
        if (checkBox2.isSelected()) {
            txtConPassword.setEchoChar((char) 0);
            lbConfirm.setText("Hide password");
        } else {
            txtConPassword.setEchoChar('*');
            lbConfirm.setText("Show password");
        }
    }//GEN-LAST:event_checkBox2ActionPerformed

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameKeyPressed

    private void btnUpdateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseClicked
        try {
            validateTXT();

            if (TXT_VALIDATE == false) {

                validateUser();

                if (USER_VALIDATE == true) {
                    if (txtPassword.getText().equals(txtConPassword.getText())) {

                        String updateUser = "UPDATE users SET userFullName = '" + txtName.getText().toLowerCase() + "', userName = '" + txtUsername.getText() + "', password = '" + txtPassword.getText() + "' WHERE idusers = '" + USERID + "' ";
                        DB.idu(updateUser);

                        NotificationPopup.update();
                        loadUsers();
                        this.dispose();
                    } else {
                        int TIME_VISIBLE = 2000;
                        JOptionPane pane = new JOptionPane("Password not match..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
                        JDialog dialog = pane.createDialog(null, "");
                        dialog.setModal(false);
                        dialog.setVisible(true);

                        new Timer(TIME_VISIBLE, new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                dialog.setVisible(false);

                            }

                        }).start();
                    }

                } else {
                    NotificationPopup.already();
                }

            } else {
                NotificationPopup.fillFeilds();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnUpdateMouseClicked

    private void txtConPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConPasswordKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                validateTXT();

                if (TXT_VALIDATE == false) {

                    validateUser();

                    if (USER_VALIDATE == true) {
                        if (txtPassword.getText().equals(txtConPassword.getText())) {

                            String updateUser = "UPDATE users SET userFullName = '" + txtName.getText().toLowerCase() + "', userName = '" + txtUsername.getText() + "', password = '" + txtPassword.getText() + "' WHERE idusers = '" + USERID + "' ";
                            DB.idu(updateUser);

                            NotificationPopup.update();
                            loadUsers();
                            this.dispose();
                        } else {
                            int TIME_VISIBLE = 2000;
                            JOptionPane pane = new JOptionPane("Password not match..!", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
                            JDialog dialog = pane.createDialog(null, "");
                            dialog.setModal(false);
                            dialog.setVisible(true);

                            new Timer(TIME_VISIBLE, new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    dialog.setVisible(false);

                                }

                            }).start();
                        }

                    } else {
                        NotificationPopup.already();
                    }

                } else {
                    NotificationPopup.fillFeilds();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_txtConPasswordKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditUser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnUpdate;
    private javax.swing.JCheckBox checkBox1;
    private javax.swing.JCheckBox checkBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lbClose;
    private javax.swing.JLabel lbConfirm;
    private javax.swing.JLabel lbShow;
    private javax.swing.JPasswordField txtConPassword;
    private javax.swing.JTextField txtName;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnUpdate.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnUpdate.setBackground(Color.white);
    }

    private void getDetails() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) tblUser.getModel();
            int selectedRow = tblUser.getSelectedRow();

            txtName.setText(dtm.getValueAt(selectedRow, 2).toString());
            txtUsername.setText(dtm.getValueAt(selectedRow, 3).toString());
            txtPassword.setText(dtm.getValueAt(selectedRow, 4).toString());
            txtConPassword.setText(dtm.getValueAt(selectedRow, 4).toString());

            USERNAME = dtm.getValueAt(selectedRow, 3).toString();
            USERID = (Integer) dtm.getValueAt(selectedRow, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateTXT() {

        java.awt.Component[] cp = jPanel3.getComponents();
        for (java.awt.Component c : cp) {

            if (c instanceof JTextField) {

                if ((((JTextField) c).getText().isEmpty())) {

                    TXT_VALIDATE = true;

                    break;

                } else {

                    TXT_VALIDATE = false;

                }

            }

        }
    }

    private void validateUser() {

        try {

            if (!USERNAME.equals(txtUsername.getText())) {

                ResultSet rs = DB.search("SELECT * FROM users WHERE userName = '" + txtUsername.getText() + "' AND NOT idusers = '" + USERID + "' AND isActive = 1 ");

                if (!rs.next()) {
                    USER_VALIDATE = true;
                } else {
                    USER_VALIDATE = false;
                }

            } else {
                USER_VALIDATE = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
