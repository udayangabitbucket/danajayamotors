package User;

import MainMenu.*;
import ExClasses.DB;
import ExClasses.NotificationPopup;
import static MainMenu.Login.UserPrivilage;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class User extends javax.swing.JFrame {

    public User() {
        initComponents();
        setOpaque();
        this.setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png")).getImage());
        changeBackgroud();
        DesignTable();
        loadUsers();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jpEdit = new javax.swing.JMenuItem();
        jpDelete = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        lbClose = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAddUser = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUser = new javax.swing.JTable();

        jPopupMenu1.setBackground(new java.awt.Color(102, 102, 102));
        jPopupMenu1.setForeground(new java.awt.Color(255, 255, 255));
        jPopupMenu1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPopupMenu1.setPreferredSize(new java.awt.Dimension(100, 50));

        jpEdit.setBackground(new java.awt.Color(102, 102, 102));
        jpEdit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jpEdit.setForeground(new java.awt.Color(255, 255, 255));
        jpEdit.setText("Edit");
        jpEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jpEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpEditActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jpEdit);

        jpDelete.setBackground(new java.awt.Color(102, 102, 102));
        jpDelete.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jpDelete.setForeground(new java.awt.Color(255, 255, 255));
        jpDelete.setText("Delete");
        jpDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jpDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpDeleteActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jpDelete);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel3.setPreferredSize(new java.awt.Dimension(630, 440));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbClose.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbClose.setText("X");
        lbClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lbClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbCloseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbCloseMouseExited(evt);
            }
        });
        jPanel3.add(lbClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 40, -1));

        jPanel1.setBackground(new java.awt.Color(90, 0, 156));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" User");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 26));

        jPanel2.setBackground(new java.awt.Color(0, 153, 51));

        btnAddUser.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnAddUser.setForeground(new java.awt.Color(255, 255, 255));
        btnAddUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAddUser.setText("+");
        btnAddUser.setToolTipText("Add user");
        btnAddUser.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAddUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddUserMouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddUserMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddUserMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddUser, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddUser, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 30, 30));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        tblUser.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        tblUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "REF.NO", "NAME", "USERNAME", "PASSWORD", "PRIVILAGE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblUser.setFocusable(false);
        tblUser.setRowHeight(35);
        tblUser.setSelectionBackground(new java.awt.Color(153, 153, 153));
        tblUser.setShowVerticalLines(false);
        tblUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblUserMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblUser);
        if (tblUser.getColumnModel().getColumnCount() > 0) {
            tblUser.getColumnModel().getColumn(0).setMinWidth(0);
            tblUser.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblUser.getColumnModel().getColumn(0).setMaxWidth(0);
            tblUser.getColumnModel().getColumn(2).setPreferredWidth(300);
            tblUser.getColumnModel().getColumn(4).setMinWidth(0);
            tblUser.getColumnModel().getColumn(4).setPreferredWidth(0);
            tblUser.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 700, 350));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 590));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseExited
        lbClose.setBackground(Color.white);
        lbClose.setForeground(Color.black);
    }//GEN-LAST:event_lbCloseMouseExited

    private void lbCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseEntered
        lbClose.setBackground(new Color(204, 0, 0));
        lbClose.setForeground(Color.white);
    }//GEN-LAST:event_lbCloseMouseEntered

    private void lbCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbCloseMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbCloseMouseClicked

    private void btnAddUserMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddUserMouseEntered
        btnAddUser.setBackground(new Color(0, 102, 0));
    }//GEN-LAST:event_btnAddUserMouseEntered

    private void btnAddUserMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddUserMouseExited
        btnAddUser.setBackground(new Color(0, 153, 51));
    }//GEN-LAST:event_btnAddUserMouseExited

    private void btnAddUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddUserMouseClicked
        //if (UserPrivilage.equals("admin")) {
        new AddUser().setVisible(true);
        //}
    }//GEN-LAST:event_btnAddUserMouseClicked

    private void tblUserMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblUserMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3) {
            if (UserPrivilage.equals("admin")) {
                if (evt.isPopupTrigger() && tblUser.getSelectedRowCount() != 0) {
                    jPopupMenu1.show(evt.getComponent(), evt.getX(), evt.getY());
                }
            }

        }
    }//GEN-LAST:event_tblUserMouseReleased

    private void jpEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpEditActionPerformed
        new EditUser().setVisible(true);
    }//GEN-LAST:event_jpEditActionPerformed

    private void jpDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpDeleteActionPerformed
        try {

            DefaultTableModel dtm = (DefaultTableModel) tblUser.getModel();
            int selectedRow = tblUser.getSelectedRow();

            if (dtm.getValueAt(selectedRow, 5).equals("admin")) {

                NotificationPopup.deleteadmin();

            } else {

                if (Login.UserName.equals(dtm.getValueAt(selectedRow, 3))) {

                    NotificationPopup.deletecurrentuser();

                } else {

                    int response = JOptionPane.showConfirmDialog(this, "Do you want delete " + dtm.getValueAt(selectedRow, 3).toString().toUpperCase() + " ?", "Delete user", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (response == JOptionPane.YES_OPTION) {

                        String deleteUser = "UPDATE users SET isActive = 0 WHERE idusers = '" + dtm.getValueAt(selectedRow, 0) + "' ";
                        DB.idu(deleteUser);
                        loadUsers();

                        NotificationPopup.delete();
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jpDeleteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {

        } catch (InstantiationException ex) {

        } catch (IllegalAccessException ex) {

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new User().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAddUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem jpDelete;
    private javax.swing.JMenuItem jpEdit;
    private javax.swing.JLabel lbClose;
    public static javax.swing.JTable tblUser;
    // End of variables declaration//GEN-END:variables

    private void setOpaque() {
        lbClose.setOpaque(true);
        btnAddUser.setOpaque(true);
    }

    private void changeBackgroud() {
        lbClose.setBackground(Color.white);
        btnAddUser.setBackground(new Color(0, 153, 51));
    }

    private void DesignTable() {
        tblUser.getTableHeader().setFont(new Font("Times New Roman", Font.PLAIN, 14));
        tblUser.getTableHeader().setOpaque(false);
        tblUser.getTableHeader().setBackground(Color.white);
        tblUser.getTableHeader().setForeground(new Color(0, 153, 153));

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblUser.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
        tblUser.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tblUser.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);

    }

    public static void loadUsers() {
        try {
            ResultSet rs = DB.search("SELECT * FROM users WHERE isActive = '1' ORDER BY idusers ASC");

            DefaultTableModel dtm = (DefaultTableModel) tblUser.getModel();
            dtm.setRowCount(0);

            while (rs.next()) {

                if (UserPrivilage.equals("admin")) {
                    dtm.addRow(new Object[]{
                        rs.getInt("idusers"),
                        rs.getString("userRef"),
                        rs.getString("userFullName"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getString("privilage")});
                } else {

                    if (!rs.getString("userName").equals("admin")) {
                        dtm.addRow(new Object[]{
                            rs.getInt("idusers"),
                            rs.getString("userRef"),
                            rs.getString("userFullName"),
                            rs.getString("userName"),
                            rs.getString("password"),
                            rs.getString("privilage")});
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
